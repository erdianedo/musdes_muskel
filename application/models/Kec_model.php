<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kec_model extends CI_Model
{
    public function getKotaByKec($kec){
        $this->db->like('kode',$kec);
        $this->db->order_by('nmdesa','ASC');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->row_array();
    }

    public function getKelByKec($kec){
        $this->db->like('kode',$kec);
        $this->db->order_by('nmdesa','DSC');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->result_array();
    }

    public function get_AllKota(){
        $this->db->order_by('nmkab','ASC');
        $this->db->group_by('nmkab');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->result_array();
    }

    public function get_AllKec($kota){
        $this->db->where('nmkab',$kota);
        $this->db->order_by('nmkec','ASC');
        $this->db->group_by('nmkec');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->result_array();
    }

    public function bulan_ini(){
        $bulan = date('m');
        switch($bulan){
            case '01':
                $bulan_r = "Januari";
            break;
            case '02':
                $bulan_r = "Februari";
            break;
            case '03':
                $bulan_r = "Maret";
            break;
            case '04':
                $bulan_r = "April";
            break;
            case '05':
                $bulan_r = "Mei";
            break;
            case '06':
                $bulan_r = "Juni";
            break;
            case '07':
                $bulan_r = "Juli";
            break;
            case '08':
                $bulan_r = "Agustus";
            break;
            case '09':
                $bulan_r = "September";
            break;
            case '10':
                $bulan_r = "Oktober";
            break;
            case '11':
                $bulan_r = "November";
            break;
            case '12':
                $bulan_r = "Desember";
            break;
            default:
                $bulan_r = "Tidak di ketahui";     
            break;
        }
        return $bulan_r." ".date('Y');
    }

    public function bulan_ini_saja(){
        $bulan = date('m');
        switch($bulan){
            case '01':
                $bulan_r = "Januari";
            break;
            case '02':
                $bulan_r = "Februari";
            break;
            case '03':
                $bulan_r = "Maret";
            break;
            case '04':
                $bulan_r = "April";
            break;
            case '05':
                $bulan_r = "Mei";
            break;
            case '06':
                $bulan_r = "Juni";
            break;
            case '07':
                $bulan_r = "Juli";
            break;
            case '08':
                $bulan_r = "Agustus";
            break;
            case '09':
                $bulan_r = "September";
            break;
            case '10':
                $bulan_r = "Oktober";
            break;
            case '11':
                $bulan_r = "November";
            break;
            case '12':
                $bulan_r = "Desember";
            break;
            case 'semua':
                $bulan_r = "Januari - Desember";
            break;
            default:
                $bulan_r = "Tidak di ketahui";     
            break;
        }
        return $bulan_r." ".date('Y');
    }

    public function bulan_ini_saja2($bulan){
        switch($bulan){
            case '01':
                $bulan_r = "Januari";
            break;
            case '02':
                $bulan_r = "Februari";
            break;
            case '03':
                $bulan_r = "Maret";
            break;
            case '04':
                $bulan_r = "April";
            break;
            case '05':
                $bulan_r = "Mei";
            break;
            case '06':
                $bulan_r = "Juni";
            break;
            case '07':
                $bulan_r = "Juli";
            break;
            case '08':
                $bulan_r = "Agustus";
            break;
            case '09':
                $bulan_r = "September";
            break;
            case '10':
                $bulan_r = "Oktober";
            break;
            case '11':
                $bulan_r = "November";
            break;
            case '12':
                $bulan_r = "Desember";
            break;
            case 'semua':
                $bulan_r = "Januari-Desember";
            break;
            default:
                $bulan_r = "Tidak di ketahui";     
            break;
        }
        return $bulan_r;
    }
}