<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ruta_model extends CI_Model
{
    public function get_ruta($kab,$kec,$bulan){
        $ar_ruta = array();
        $this->db->where('kota',$kab);
        $this->db->where('kecamatan',$kec);
        $this->db->like('tahun',date('Y-m'));
        $query = $this->db->get('data_ruta');
        if($query->row_array()==null){
            $this->db->where('nmkab',$kab);
            $this->db->where('nmkec',$kec);
            $this->db->order_by('nmdesa','DSC');
            $query = $this->db->get('kode_wil_kemendagri');
            $data['kel'] = $query->result_array();
            for($x=0;$x<count($data['kel']);$x++){
                $ar_ruta[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'perbaiki'=>"0",'keluar'=>"0",'baru'=>"0",'status'=>"baru");
            }
            return $ar_ruta;
        }
        else{
            $data['ruta'] = $query->row_array();
            $kel = explode(",",$data['ruta']['kelurahan']);
            $perbaiki = explode(",",$data['ruta']['perbaiki']);
            $keluar = explode(",",$data['ruta']['keluar']);
            $baru = explode(",",$data['ruta']['baru']);
            for($x=0;$x<count($kel);$x++){
                $ar_ruta[$x] = array('id'=>$data['ruta']['id'],'nmdesa'=>$kel[$x],'perbaiki'=>$perbaiki[$x],'keluar'=>$keluar[$x],'baru'=>$baru[$x],'status'=>"update");
            }
            return $ar_ruta;
        }
    }

    public function get_ruta2($kab,$kec,$bulan){
        $ar_ruta = array();
        $this->db->where('kota',$kab);
        $this->db->where('kecamatan',$kec);
        $this->db->like('tahun',$bulan);
        $query = $this->db->get('data_ruta');
        if($query->row_array()==null){
            $this->db->where('nmkab',$kab);
            $this->db->where('nmkec',$kec);
            $this->db->order_by('nmdesa','DSC');
            $query = $this->db->get('kode_wil_kemendagri');
            $data['kel'] = $query->result_array();
            for($x=0;$x<count($data['kel']);$x++){
                $ar_ruta[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'perbaiki'=>"0",'keluar'=>"0",'baru'=>"0",'status'=>"baru");
            }
            return $ar_ruta;
        }
        else{
            $data['ruta'] = $query->row_array();
            $kel = explode(",",$data['ruta']['kelurahan']);
            $perbaiki = explode(",",$data['ruta']['perbaiki']);
            $keluar = explode(",",$data['ruta']['keluar']);
            $baru = explode(",",$data['ruta']['baru']);
            for($x=0;$x<count($kel);$x++){
                $ar_ruta[$x] = array('id'=>$data['ruta']['id'],'nmdesa'=>$kel[$x],'perbaiki'=>$perbaiki[$x],'keluar'=>$keluar[$x],'baru'=>$baru[$x],'status'=>"update");
            }
            return $ar_ruta;
        }
    }

    public function get_art($kab,$kec,$bulan){
        $ar_art = array();
        $this->db->where('kota',$kab);
        $this->db->where('kecamatan',$kec);
        $this->db->like('tahun',date('Y-m'));
        $query = $this->db->get('data_art');
        if($query->row_array()==null){
            $this->db->where('nmkab',$kab);
            $this->db->where('nmkec',$kec);
            $this->db->order_by('nmdesa','DSC');
            $query = $this->db->get('kode_wil_kemendagri');
            $data['kel'] = $query->result_array();
            for($x=0;$x<count($data['kel']);$x++){
                $ar_art[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'tinggal_di_ruta'=>"0",'meninggal'=>"0",'pindah'=>"0",'baru'=>'0','kesalahan_prelist'=>'0','tidak_ditemukan'=>'0','usulan_baru'=>'0','status'=>"baru");
            }
            return $ar_art;
        }
        else{
            $data['art'] = $query->row_array();
            $kel = explode(",",$data['art']['kelurahan']);
            $tinggal_di_ruta = explode(",",$data['art']['tinggal_di_ruta']);
            $meninggal = explode(",",$data['art']['meninggal']);
            $pindah = explode(",",$data['art']['pindah']);
            $baru = explode(",",$data['art']['baru']);
            $kesalahan_prelist = explode(",",$data['art']['kesalahan_prelist']);
            $tidak_ditemukan = explode(",",$data['art']['tidak_ditemukan']);
            $usulan_baru = explode(",",$data['art']['usulan_baru']);
            for($x=0;$x<count($kel);$x++){
                $ar_art[$x] = array('id'=>$data['art']['id'],'nmdesa'=>$kel[$x],'tinggal_di_ruta'=>$tinggal_di_ruta[$x],'meninggal'=>$meninggal[$x],'pindah'=>$pindah[$x],'baru'=>$baru[$x],'kesalahan_prelist'=>$kesalahan_prelist[$x],'tidak_ditemukan'=>$tidak_ditemukan[$x],'usulan_baru'=>$usulan_baru[$x],'status'=>"update");
            }
            return $ar_art;
        }
    }

    public function get_art2($kab,$kec,$bulan){
        $ar_art = array();
        $this->db->where('kota',$kab);
        $this->db->where('kecamatan',$kec);
        $this->db->like('tahun',$bulan);
        $query = $this->db->get('data_art');
        if($query->row_array()==null){
            $this->db->where('nmkab',$kab);
            $this->db->where('nmkec',$kec);
            $this->db->order_by('nmdesa','DSC');
            $query = $this->db->get('kode_wil_kemendagri');
            $data['kel'] = $query->result_array();
            for($x=0;$x<count($data['kel']);$x++){
                $ar_art[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'tinggal_di_ruta'=>"0",'meninggal'=>"0",'pindah'=>"0",'baru'=>'0','kesalahan_prelist'=>'0','tidak_ditemukan'=>'0','usulan_baru'=>'0','status'=>"baru");
            }
            return $ar_art;
        }
        else{
            $data['art'] = $query->row_array();
            $kel = explode(",",$data['art']['kelurahan']);
            $tinggal_di_ruta = explode(",",$data['art']['tinggal_di_ruta']);
            $meninggal = explode(",",$data['art']['meninggal']);
            $pindah = explode(",",$data['art']['pindah']);
            $baru = explode(",",$data['art']['baru']);
            $kesalahan_prelist = explode(",",$data['art']['kesalahan_prelist']);
            $tidak_ditemukan = explode(",",$data['art']['tidak_ditemukan']);
            $usulan_baru = explode(",",$data['art']['usulan_baru']);
            for($x=0;$x<count($kel);$x++){
                $ar_art[$x] = array('id'=>$data['art']['id'],'nmdesa'=>$kel[$x],'tinggal_di_ruta'=>$tinggal_di_ruta[$x],'meninggal'=>$meninggal[$x],'pindah'=>$pindah[$x],'baru'=>$baru[$x],'kesalahan_prelist'=>$kesalahan_prelist[$x],'tidak_ditemukan'=>$tidak_ditemukan[$x],'usulan_baru'=>$usulan_baru[$x],'status'=>"update");
            }
            return $ar_art;
        }
    }

    public function input_ruta($kab,$bln){
        $kec = $this->input->post('kec', true);
        $this->db->where('nmkab',$kab);
        $this->db->where('nmkec',$kec);
        $this->db->limit(1);
        $query = $this->db->get('kode_wil_kemendagri');
        $data['kec'] = $query->row_array();
        $kota = $data['kec']['nmkab'];
        $tahun_bulan = $this->input->post('bulan', true);
        $kel = $this->input->post('kel[]', true);
        $perbaiki = $this->input->post('perbaiki[]', true);
        $keluar = $this->input->post('keluar[]', true);
        $baru = $this->input->post('baru[]', true);
        $_kel = "";
        $_perbaiki = "";
        $_keluar = "";
        $_baru = "";
        for($x=0;$x<count($kel);$x++){
            if($x==0){
                $_kel = $kel[$x];
                $_perbaiki = $perbaiki[$x];
                $_keluar = $keluar[$x];
                $_baru = $baru[$x];
            }
            else{
                $_kel = $_kel.",".$kel[$x];
                $_perbaiki = $_perbaiki.",".$perbaiki[$x];
                $_keluar = $_keluar.",".$keluar[$x];
                $_baru = $_baru.",".$baru[$x];
            }
        }


        $huruf = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

        $a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);$e = rand(0,25);

        $id = $huruf[$a].$c.$d.$huruf[$b].$huruf[$e];
        $data = [
            "id" => $id."-".$bln."-1",
            "kota" => $kota,
            "kecamatan" => $kec,
            "kelurahan" => $_kel,
            "perbaiki" => $_perbaiki,
            "keluar" => $_keluar,
            "baru" => $_baru,
            "tahun" => $bln."-1",
            "bulan" => $this->Kec_model->bulan_ini_saja()
        ];
        $this->db->insert('data_ruta',$data);
        $this->session->set_flashdata('input_ruta','Data Rumah Tangga Kecamatan'.$kec.' Berhasil Ditambahkan, Anda Bisa Mengeditnya Kapan Saja');
        redirect('rumahtangga/ruta');   
    }

    public function input_art($kab){
        $kec = $this->input->post('kec', true);
        $this->db->where('nmkab',$kab);
        $this->db->where('nmkec',$kec);
        $this->db->limit(1);
        $query = $this->db->get('kode_wil_kemendagri');
        $data['kec'] = $query->row_array();
        $kota = $data['kec']['nmkab'];
        $tahun_bulan = $this->input->post('bulan', true);
        $kel = $this->input->post('kel[]', true);
        $tinggal_di_ruta = $this->input->post('tinggal_di_ruta[]', true);
        $meninggal = $this->input->post('meninggal[]', true);
        $pindah = $this->input->post('pindah[]', true);
        $baru = $this->input->post('baru[]', true);
        $kesalahan_prelist = $this->input->post('kesalahan_prelist[]', true);
        $tidak_ditemukan = $this->input->post('tidak_ditemukan[]', true);
        $usulan_baru = $this->input->post('usulan_baru[]', true);
        $_kel = "";
        $_tinggal_di_ruta = "";
        $_meninggal = "";
        $_pindah = "";
        $_baru = "";
        $_kesalahan_prelist = "";
        $_tidak_ditemukan = "";
        $_usulan_baru = "";
        for($x=0;$x<count($kel);$x++){
            if($x==0){
                $_kel = $kel[$x];
                $_tinggal_di_ruta = $tinggal_di_ruta[$x];
                $_meninggal = $meninggal[$x];
                $_pindah = $pindah[$x];
                $_baru = $baru[$x];
                $_kesalahan_prelist = $kesalahan_prelist[$x];
                $_tidak_ditemukan = $tidak_ditemukan[$x];
                $_usulan_baru = $usulan_baru[$x];
            } 
            else{
                $_kel = $_kel.",".$kel[$x];
                $_tinggal_di_ruta = $_tinggal_di_ruta.",".$tinggal_di_ruta[$x];
                $_meninggal = $_meninggal.",".$meninggal[$x];
                $_pindah = $_pindah.",".$pindah[$x];
                $_baru = $_baru.",".$baru[$x];
                $_kesalahan_prelist = $_kesalahan_prelist.",".$kesalahan_prelist[$x];
                $_tidak_ditemukan = $_tidak_ditemukan.",".$tidak_ditemukan[$x];
                $_usulan_baru = $_usulan_baru.",".$usulan_baru[$x];
            }
        }


        $huruf = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

        $a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);$e = rand(0,25);

        $id = $huruf[$a].$c.$d.$huruf[$b].$huruf[$e];
        $data = [
            "id" => $id."-".date('Y-m-d'),
            "kota" => $kota,
            "kecamatan" => $kec,
            "kelurahan" => $_kel,
            "tinggal_di_ruta" => $_tinggal_di_ruta,
            "meninggal" => $_meninggal,
            "pindah" => $_pindah,
            "baru" => $_baru,
            "kesalahan_prelist" => $_kesalahan_prelist,
            "tidak_ditemukan" => $_tidak_ditemukan,
            "usulan_baru" => $_usulan_baru,
            "tahun" => date('Y-m-d'),
            "bulan" => $this->Kec_model->bulan_ini_saja()
        ];
        $this->db->insert('data_art',$data);
        $this->session->set_flashdata('input_art','Data Anggota Rumah Tangga Kecamatan'.$kec.' Berhasil Ditambahkan, Anda Bisa Mengeditnya Kapan Saja');
        redirect('rumahtangga/art');   
    }

    public function update_ruta(){
        $id = $this->input->post('id', true);
        $kec = $this->input->post('kec', true);
        $tahun_bulan = $this->input->post('bulan', true);
        $kel = $this->input->post('kel[]', true);
        $perbaiki = $this->input->post('perbaiki[]', true);
        $keluar = $this->input->post('keluar[]', true);
        $baru = $this->input->post('baru[]', true);
        $_kel = "";
        $_perbaiki = "";
        $_keluar = "";
        $_baru = "";
        for($x=0;$x<count($kel);$x++){
            if($x==0){
                $_perbaiki = $perbaiki[$x];
                $_keluar = $keluar[$x];
                $_baru = $baru[$x];
            }
            else{
                $_perbaiki = $_perbaiki.",".$perbaiki[$x];
                $_keluar = $_keluar.",".$keluar[$x];
                $_baru = $_baru.",".$baru[$x];
            }
        }
        $data = [
            "perbaiki" => $_perbaiki,
            "keluar" => $_keluar,
            "baru" => $_baru
        ];
        $this->db->where('id',$id);
        $this->db->update('data_ruta',$data);
        $this->session->set_flashdata('update_ruta','Data Rumah Tangga Kecamatan'.$kec.' Berhasil Diperbaharui, Anda Bisa Mengeditnya Kapan Saja');
        redirect('rumahtangga/ruta');   
    }

    public function update_art(){
        $id = $this->input->post('id',true);
        $kec = $this->input->post('kec', true);
        $tahun_bulan = $this->input->post('bulan', true);
        $kel = $this->input->post('kel[]', true);
        $tinggal_di_ruta = $this->input->post('tinggal_di_ruta[]', true);
        $meninggal = $this->input->post('meninggal[]', true);
        $pindah = $this->input->post('pindah[]', true);
        $baru = $this->input->post('baru[]', true);
        $kesalahan_prelist = $this->input->post('kesalahan_prelist[]', true);
        $tidak_ditemukan = $this->input->post('tidak_ditemukan[]', true);
        $usulan_baru = $this->input->post('usulan_baru[]', true);
        $_kel = "";
        $_tinggal_di_ruta = "";
        $_meninggal = "";
        $_pindah = "";
        $_baru = "";
        $_kesalahan_prelist = "";
        $_tidak_ditemukan = "";
        $_usulan_baru = "";
        for($x=0;$x<count($kel);$x++){
            if($x==0){
                $_tinggal_di_ruta = $tinggal_di_ruta[$x];
                $_meninggal = $meninggal[$x];
                $_pindah = $pindah[$x];
                $_baru = $baru[$x];
                $_kesalahan_prelist = $kesalahan_prelist[$x];
                $_tidak_ditemukan = $tidak_ditemukan[$x];
                $_usulan_baru = $usulan_baru[$x];
            } 
            else{
                $_tinggal_di_ruta = $_tinggal_di_ruta.",".$tinggal_di_ruta[$x];
                $_meninggal = $_meninggal.",".$meninggal[$x];
                $_pindah = $_pindah.",".$pindah[$x];
                $_baru = $_baru.",".$baru[$x];
                $_kesalahan_prelist = $_kesalahan_prelist.",".$kesalahan_prelist[$x];
                $_tidak_ditemukan = $_tidak_ditemukan.",".$tidak_ditemukan[$x];
                $_usulan_baru = $_usulan_baru.",".$usulan_baru[$x];
            }
        }
        $data = [
            "tinggal_di_ruta" => $_tinggal_di_ruta,
            "meninggal" => $_meninggal,
            "pindah" => $_pindah,
            "baru" => $_baru,
            "kesalahan_prelist" => $_kesalahan_prelist,
            "tidak_ditemukan" => $_tidak_ditemukan,
            "usulan_baru" => $_usulan_baru
        ];
        $this->db->where('id', $id);
        $this->db->update('data_art',$data);
        $this->session->set_flashdata('update_art','Data Anggota Rumah Tangga Kecamatan'.$kec.' Berhasil Diperbaharui, Anda Bisa Mengeditnya Kapan Saja');
        redirect('rumahtangga/art');   
    }

    public function laporan_kec_ruta($kab,$kec,$bulan,$tahun){
        $ar_ruta = array();
        if($bulan!="semua"){
            $this->db->where('kota',$kab);
            $this->db->where('kecamatan',$kec);
            $this->db->like('tahun',$tahun."-".$bulan);
            $query = $this->db->get('data_ruta');
            if($query->row_array()==null){
                $this->db->where('nmkab',$kab);
                $this->db->where('nmkec',$kec);
                $this->db->order_by('nmdesa','DSC');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kel'] = $query->result_array();
                for($x=0;$x<count($data['kel']);$x++){
                    $ar_ruta[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'perbaiki'=>"0",'keluar'=>"0",'baru'=>"0",'status'=>"baru");
                }
                return $ar_ruta;
            }
            else{
                $data['ruta'] = $query->row_array();
                $kel = explode(",",$data['ruta']['kelurahan']);
                $perbaiki = explode(",",$data['ruta']['perbaiki']);
                $keluar = explode(",",$data['ruta']['keluar']);
                $baru = explode(",",$data['ruta']['baru']);
                for($x=0;$x<count($kel);$x++){
                    $ar_ruta[$x] = array('id'=>$data['ruta']['id'],'nmdesa'=>$kel[$x],'perbaiki'=>$perbaiki[$x],'keluar'=>$keluar[$x],'baru'=>$baru[$x],'status'=>"update");
                }
                return $ar_ruta;
            }
        }
        else{//semua bulan
            $this->db->where('kecamatan',$kec);
            $this->db->like('tahun',$tahun);
            $query = $this->db->get('data_ruta');
            if($query->row_array()==null){
                $this->db->where('nmkec',$kec);
                $this->db->order_by('nmdesa','DSC');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kel'] = $query->result_array();
                for($x=0;$x<count($data['kel']);$x++){
                    $ar_ruta[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'perbaiki'=>"0",'keluar'=>"0",'baru'=>"0",'status'=>"baru");
                }
                return $ar_ruta;
            }
            else{
                if($query->num_rows()==1){
                    $data['ruta'] = $query->row_array();
                    $kel = explode(",",$data['ruta']['kelurahan']);
                    $perbaiki = explode(",",$data['ruta']['perbaiki']);
                    $keluar = explode(",",$data['ruta']['keluar']);
                    $baru = explode(",",$data['ruta']['baru']);
                    for($x=0;$x<count($kel);$x++){
                        $ar_ruta[$x] = array('id'=>$data['ruta']['id'],'nmdesa'=>$kel[$x],'perbaiki'=>$perbaiki[$x],'keluar'=>$keluar[$x],'baru'=>$baru[$x],'status'=>"update");
                    }
                    return $ar_ruta;
                }
                else{
                    $data['ruta'] = $query->result_array();
                    $kel = explode(",",$data['ruta'][0]['kelurahan']);
                    $perbaiki_ = array();
                    $keluar_ = array();
                    $baru_ = array();
                    $perbaiki_2 = 0;
                    $keluar_2 = 0;
                    $baru_2 = 0;
                    for($n=0;$n<$query->num_rows();$n++){
                        $perbaiki = explode(",",$data['ruta'][$n]['perbaiki']);
                        $keluar = explode(",",$data['ruta'][$n]['keluar']);
                        $baru = explode(",",$data['ruta'][$n]['baru']);
                        $perbaiki_[$n] = $perbaiki;
                        $keluar_[$n] = $keluar;
                        $baru_[$n] = $baru;
                    }
                    for($x=0;$x<count($kel);$x++){
                        for($n=0;$n<$query->num_rows();$n++){
                            $perbaiki_2 = $perbaiki_2 + $perbaiki_[$n][$x];
                            $keluar_2 = $keluar_2 + $keluar_[$n][$x];
                            $baru_2 = $baru_2 + $baru_[$n][$x];
                        }
                        $ar_ruta[$x] = array('nmdesa'=>$kel[$x],'perbaiki'=>$perbaiki_2,'keluar'=>$keluar_2,'baru'=>$baru_2,'status'=>"update");
                        $perbaiki_2 = 0; $keluar_2 = 0; $baru_2 = 0;
                    }
                    return $ar_ruta;
                }
            }
        }
    }

    public function laporan_kec_art($kab,$kec,$bulan,$tahun){
        $ar_art = array();
        if($bulan!="semua"){
            $this->db->where('kota',$kab);
            $this->db->where('kecamatan',$kec);
            $this->db->like('tahun',$tahun."-".$bulan);
            $query = $this->db->get('data_art');
            if($query->row_array()==null){
                $this->db->where('nmkab',$kab);
                $this->db->where('nmkec',$kec);
                $this->db->order_by('nmdesa','DSC');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kel'] = $query->result_array();
                for($x=0;$x<count($data['kel']);$x++){
                    $ar_art[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'tinggal_di_ruta'=>"0",'meninggal'=>"0",'pindah'=>"0",'baru'=>'0','kesalahan_prelist'=>'0','tidak_ditemukan'=>'0','usulan_baru'=>'0','status'=>"baru");
                }
                return $ar_art;
            }
            else{
                if($query->num_rows()==1){
                    $data['art'] = $query->row_array();
                    $kel = explode(",",$data['art']['kelurahan']);
                    $tinggal_di_ruta = explode(",",$data['art']['tinggal_di_ruta']);
                    $meninggal = explode(",",$data['art']['meninggal']);
                    $pindah = explode(",",$data['art']['pindah']);
                    $baru = explode(",",$data['art']['baru']);
                    $kesalahan_prelist = explode(",",$data['art']['kesalahan_prelist']);
                    $tidak_ditemukan = explode(",",$data['art']['tidak_ditemukan']);
                    $usulan_baru = explode(",",$data['art']['usulan_baru']);
                    for($x=0;$x<count($kel);$x++){
                        $ar_art[$x] = array('id'=>$data['art']['id'],'nmdesa'=>$kel[$x],'tinggal_di_ruta'=>$tinggal_di_ruta[$x],'meninggal'=>$meninggal[$x],'pindah'=>$pindah[$x],'baru'=>$baru[$x],'kesalahan_prelist'=>$kesalahan_prelist[$x],'tidak_ditemukan'=>$tidak_ditemukan[$x],'usulan_baru'=>$usulan_baru[$x],'status'=>"update");
                    }
                    return $ar_art;
                }
                else{
                    $data['art'] = $query->result_array();
                    $kel = explode(",",$data['art']['kelurahan']);
                    $tinggal_di_ruta = explode(",",$data['art']['tinggal_di_ruta']);
                    $meninggal = explode(",",$data['art']['meninggal']);
                    $pindah = explode(",",$data['art']['pindah']);
                    $baru = explode(",",$data['art']['baru']);
                    $kesalahan_prelist = explode(",",$data['art']['kesalahan_prelist']);
                    $tidak_ditemukan = explode(",",$data['art']['tidak_ditemukan']);
                    $usulan_baru = explode(",",$data['art']['usulan_baru']);
                    for($x=0;$x<count($kel);$x++){
                        $ar_art[$x] = array('id'=>$data['art']['id'],'nmdesa'=>$kel[$x],'tinggal_di_ruta'=>$tinggal_di_ruta[$x],'meninggal'=>$meninggal[$x],'pindah'=>$pindah[$x],'baru'=>$baru[$x],'kesalahan_prelist'=>$kesalahan_prelist[$x],'tidak_ditemukan'=>$tidak_ditemukan[$x],'usulan_baru'=>$usulan_baru[$x],'status'=>"update");
                    }
                    return $ar_art;
                }
            }
        }
        else{//semua bulan
            $this->db->where('kecamatan',$kec);
            $this->db->like('tahun',$tahun);
            $query = $this->db->get('data_art');
            if($query->row_array()==null){
                $this->db->where('nmkec',$kec);
                $this->db->order_by('nmdesa','DSC');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kel'] = $query->result_array();
                for($x=0;$x<count($data['kel']);$x++){
                    $ar_art[$x] = array('nmdesa'=>$data['kel'][$x]['nmdesa'],'tinggal_di_ruta'=>"0",'meninggal'=>"0",'pindah'=>"0",'baru'=>'0','kesalahan_prelist'=>'0','tidak_ditemukan'=>'0','usulan_baru'=>'0','status'=>"baru");
                }
                return $ar_art;
            }
            else{
                if($query->num_rows()==1){
                    $data['art'] = $query->row_array();
                    $kel = explode(",",$data['art']['kelurahan']);
                    $tinggal_di_ruta = explode(",",$data['art']['tinggal_di_ruta']);
                    $meninggal = explode(",",$data['art']['meninggal']);
                    $pindah = explode(",",$data['art']['pindah']);
                    $baru = explode(",",$data['art']['baru']);
                    $kesalahan_prelist = explode(",",$data['art']['kesalahan_prelist']);
                    $tidak_ditemukan = explode(",",$data['art']['tidak_ditemukan']);
                    $usulan_baru = explode(",",$data['art']['usulan_baru']);
                    for($x=0;$x<count($kel);$x++){
                        $ar_art[$x] = array('id'=>$data['art']['id'],'nmdesa'=>$kel[$x],'tinggal_di_ruta'=>$tinggal_di_ruta[$x],'meninggal'=>$meninggal[$x],'pindah'=>$pindah[$x],'baru'=>$baru[$x],'kesalahan_prelist'=>$kesalahan_prelist[$x],'tidak_ditemukan'=>$tidak_ditemukan[$x],'usulan_baru'=>$usulan_baru[$x],'status'=>"update");
                    }
                    return $ar_art;
                }
                else{
                    $data['art'] = $query->result_array();
                    $kel = explode(",",$data['art'][0]['kelurahan']);
                    $tinggal_di_ruta_ = array();
                    $meninggal_ = array();
                    $pindah_ = array();
                    $baru_ = array();
                    $kesalahan_prelist_ = array();
                    $tidak_ditemukan_ = array();
                    $usulan_baru_ = array();
                    
                    $tinggal_di_ruta_2 = 0;
                    $meninggal_2 = 0;
                    $pindah_2 = 0;
                    $baru_2 = 0;
                    $kesalahan_prelist_2 = 0;
                    $tidak_ditemukan_2 = 0;
                    $usulan_baru_2 = 0;
    
                    for($n=0;$n<$query->num_rows();$n++){
                        $tinggal_di_ruta = explode(",",$data['art'][$n]['tinggal_di_ruta']);
                        $meninggal = explode(",",$data['art'][$n]['meninggal']);
                        $pindah = explode(",",$data['art'][$n]['pindah']);
                        $baru = explode(",",$data['art'][$n]['baru']);
                        $kesalahan_prelist = explode(",",$data['art'][$n]['kesalahan_prelist']);
                        $tidak_ditemukan = explode(",",$data['art'][$n]['tidak_ditemukan']);
                        $usulan_baru = explode(",",$data['art'][$n]['usulan_baru']);
    
                        $tinggal_di_ruta_[$n] = $tinggal_di_ruta;
                        $meninggal_[$n] = $meninggal;
                        $pindah_[$n] = $pindah;
                        $baru_[$n] = $baru;
                        $kesalahan_prelist_[$n] = $kesalahan_prelist;
                        $tidak_ditemukan_[$n] = $tidak_ditemukan;
                        $usulan_baru_[$n] = $usulan_baru;
                    }
                    for($x=0;$x<count($kel);$x++){
                        for($n=0;$n<$query->num_rows();$n++){
                            $tinggal_di_ruta_2 = $tinggal_di_ruta_2 + $tinggal_di_ruta_[$n][$x];
                            $meninggal_2 = $meninggal_2 + $meninggal_[$n][$x];
                            $pindah_2 = $pindah_2 + $pindah_[$n][$x];
                            $baru_2 = $baru_2 + $baru_[$n][$x];
                            $kesalahan_prelist_2 = $kesalahan_prelist_2 + $kesalahan_prelist_[$n][$x];
                            $tidak_ditemukan_2 = $tidak_ditemukan_2 + $tidak_ditemukan_[$n][$x];
                            $usulan_baru_2 = $usulan_baru_2 + $usulan_baru_[$n][$x]; 
                        }
                        $ar_art[$x] = array('nmdesa'=>$kel[$x],'tinggal_di_ruta'=>$tinggal_di_ruta_2,'meninggal'=>$meninggal_2,'pindah'=>$pindah_2,'baru'=>$baru_2,'kesalahan_prelist'=>$kesalahan_prelist_2,'tidak_ditemukan'=>$tidak_ditemukan_2,'usulan_baru'=>$usulan_baru_2,'status'=>"update");
                        $tinggal_di_ruta_2 = 0; $meninggal_2 = 0; $pindah_2 = 0;
                        $baru_2 = 0; $kesalahan_prelist_2 = 0; $tidak_ditemukan_2 = 0;
                        $usulan_baru_2 = 0;
                    }
                    return $ar_art;
                }
            }
        }
        
    }

    public function update_data_ruta(){
        $id = $this->input->post('id', true);
        $kec = $this->input->post('kec', true);
        $tahun_bulan = $this->input->post('bulan', true);
        $tb = explode("-",$tahun_bulan);
        $tahun = $tb[0];
        $bulan = $tb[1];
        $kel = $this->input->post('kel[]', true);
        $perbaiki = $this->input->post('perbaiki[]', true);
        $keluar = $this->input->post('keluar[]', true);
        $baru = $this->input->post('baru[]', true);
        $_kel = "";
        $_perbaiki = "";
        $_keluar = "";
        $_baru = "";
        for($x=0;$x<count($kel);$x++){
            if($x==0){
                $_perbaiki = $perbaiki[$x];
                $_keluar = $keluar[$x];
                $_baru = $baru[$x];
            }
            else{
                $_perbaiki = $_perbaiki.",".$perbaiki[$x];
                $_keluar = $_keluar.",".$keluar[$x];
                $_baru = $_baru.",".$baru[$x];
            }
        }
        $data = [
            "perbaiki" => $_perbaiki,
            "keluar" => $_keluar,
            "baru" => $_baru
        ];
        $this->db->where('id',$id);
        $this->db->update('data_ruta',$data);
        $this->session->set_flashdata('update_ruta','Data Rumah Tangga Kecamatan'.$kec.' Bulan '.$bulan.' Tahun '.$tahun.' Berhasil Diperbaharui, Anda Bisa Mengeditnya Kapan Saja');
        redirect('rumahtangga/lap_ruta_kec/'.$kec.'/'.$bulan.'/'.$tahun);   
    }

    public function update_data_art(){
        $id = $this->input->post('id',true);
        $kec = $this->input->post('kec', true);
        $tahun_bulan = $this->input->post('bulan', true);
        $tb = explode("-",$tahun_bulan);
        $tahun = $tb[0];
        $bulan = $tb[1];
        $kel = $this->input->post('kel[]', true);
        $tinggal_di_ruta = $this->input->post('tinggal_di_ruta[]', true);
        $meninggal = $this->input->post('meninggal[]', true);
        $pindah = $this->input->post('pindah[]', true);
        $baru = $this->input->post('baru[]', true);
        $kesalahan_prelist = $this->input->post('kesalahan_prelist[]', true);
        $tidak_ditemukan = $this->input->post('tidak_ditemukan[]', true);
        $usulan_baru = $this->input->post('usulan_baru[]', true);
        $_kel = "";
        $_tinggal_di_ruta = "";
        $_meninggal = "";
        $_pindah = "";
        $_baru = "";
        $_kesalahan_prelist = "";
        $_tidak_ditemukan = "";
        $_usulan_baru = "";
        for($x=0;$x<count($kel);$x++){
            if($x==0){
                $_tinggal_di_ruta = $tinggal_di_ruta[$x];
                $_meninggal = $meninggal[$x];
                $_pindah = $pindah[$x];
                $_baru = $baru[$x];
                $_kesalahan_prelist = $kesalahan_prelist[$x];
                $_tidak_ditemukan = $tidak_ditemukan[$x];
                $_usulan_baru = $usulan_baru[$x];
            } 
            else{
                $_tinggal_di_ruta = $_tinggal_di_ruta.",".$tinggal_di_ruta[$x];
                $_meninggal = $_meninggal.",".$meninggal[$x];
                $_pindah = $_pindah.",".$pindah[$x];
                $_baru = $_baru.",".$baru[$x];
                $_kesalahan_prelist = $_kesalahan_prelist.",".$kesalahan_prelist[$x];
                $_tidak_ditemukan = $_tidak_ditemukan.",".$tidak_ditemukan[$x];
                $_usulan_baru = $_usulan_baru.",".$usulan_baru[$x];
            }
        }
        $data = [
            "tinggal_di_ruta" => $_tinggal_di_ruta,
            "meninggal" => $_meninggal,
            "pindah" => $_pindah,
            "baru" => $_baru,
            "kesalahan_prelist" => $_kesalahan_prelist,
            "tidak_ditemukan" => $_tidak_ditemukan,
            "usulan_baru" => $_usulan_baru
        ];
        $this->db->where('id', $id);
        $this->db->update('data_art',$data);
        $bulans = $this->Kec_model->bulan_ini_saja2($bulan);
        $this->session->set_flashdata('update_art','Data Anggota Rumah Tangga Kecamatan'.$kec.' Bulan '.$bulans.' Tahun '.$tahun.' Berhasil Diperbaharui, Anda Bisa Mengeditnya Kapan Saja');
        redirect('rumahtangga/lap_art_kec/'.$kec.'/'.$bulan.'/'.$tahun);  
    }

    public function rekap_kota_ruta($kota,$kec_,$kel_,$bulan,$tahun){
        $ar_ruta = array();
        $this->db->where('kota',$kota);
        if($kec_!="semua"){
            $this->db->where('kecamatan',$kec_);
        }
        if($bulan!="semua"){
            $this->db->like('tahun',$tahun."-".$bulan);
        }
        else{
            $this->db->like('tahun',$tahun);
        }
        $this->db->order_by('kecamatan');
        $query = $this->db->get('data_ruta');
        $data['num'] = $query->num_rows();
        if($kec_=="semua"){
            if($data['num']==1){
                $data['ruta'] = $query->row_array();
                $kec = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                $ar_ruta = array();
                $ar_kec = array(); $ar_perbaiki = array(); $ar_dikeluarkan = array(); $ar_baru = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->group_by('nmkec');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    if($data['kab'][$x]['nmkec'] == $data['ruta']['kecamatan'] && $data['kab'][$x]['nmkab'] == $data['ruta']['kota']){
                        $list_perbaiki = explode(",",$data['ruta']['perbaiki']);
                        for($a=0;$a<count($list_perbaiki);$a++){
                            $perbaiki = $perbaiki + $list_perbaiki[$a];
                        }
                        $list_keluar = explode(",",$data['ruta']['keluar']);
                        for($a=0;$a<count($list_keluar);$a++){
                            $dikeluarkan = $dikeluarkan + $list_keluar[$a];
                        }
                        $list_baru = explode(",",$data['ruta']['baru']);
                        for($a=0;$a<count($list_baru);$a++){
                            $baru = $baru + $list_baru[$a];
                        }
                    }
                    
                    $kec = $data['kab'][$x]['nmkec']; 
                    $ar_ruta[$x] = array('kec'=>$kec,'perbaiki'=>$perbaiki,'keluar'=>$dikeluarkan,'baru'=>$baru);
                    $kec = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                }
                return $ar_ruta;
            }
            elseif($data['num']>1){
                $data['ruta'] = $query->result_array();
                
                $kec = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                $ar_ruta = array();
                $ar_kec = array(); $ar_perbaiki = array(); $ar_dikeluarkan = array(); $ar_baru = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->group_by('nmkec');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    for($z=0;$z<count($data['ruta']);$z++){
                        if($data['kab'][$x]['nmkec'] == $data['ruta'][$z]['kecamatan'] && $data['kab'][$x]['nmkab'] == $data['ruta'][$z]['kota']){
                            $list_perbaiki = explode(",",$data['ruta'][$z]['perbaiki']);
                            for($a=0;$a<count($list_perbaiki);$a++){
                                $perbaiki = $perbaiki + $list_perbaiki[$a];
                            }
                            $list_keluar = explode(",",$data['ruta'][$z]['keluar']);
                            for($a=0;$a<count($list_keluar);$a++){
                                $dikeluarkan = $dikeluarkan + $list_keluar[$a];
                            }
                            $list_baru = explode(",",$data['ruta'][$z]['baru']);
                            for($a=0;$a<count($list_baru);$a++){
                                $baru = $baru + $list_baru[$a];
                            }
                        }
                    }
                    $kec = $data['kab'][$x]['nmkec']; 
                    $ar_ruta[$x] = array('kec'=>$kec,'perbaiki'=>$perbaiki,'keluar'=>$dikeluarkan,'baru'=>$baru);
                    $kec = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                }
                return $ar_ruta;
            }
            else{
                $kec = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                $ar_ruta = array();
                $ar_kec = array(); $ar_perbaiki = array(); $ar_dikeluarkan = array(); $ar_baru = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->group_by('nmkec');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    $kec = $data['kab'][$x]['nmkec']; 
                    $ar_ruta[$x] = array('kec'=>$kec,'perbaiki'=>$perbaiki,'keluar'=>$dikeluarkan,'baru'=>$baru);
                    $kec = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                }
                return $ar_ruta;
            }
        }
        else{//pilih_kecamatan
            if($data['num']==1){
                $data['ruta'] = $query->row_array();
                $the_desa = explode(",",$data['ruta']['kelurahan']);
                $kec = ""; $kel = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                $ar_ruta = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->where('nmkec',$kec_);
                $this->db->order_by('nmdesa','DSC');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                $list_perbaiki = explode(",",$data['ruta']['perbaiki']);
                $list_keluar = explode(",",$data['ruta']['keluar']);
                $list_baru = explode(",",$data['ruta']['baru']);
                for($x=0;$x<count($the_desa);$x++){
                    $perbaiki = $list_perbaiki[$x];
                    $dikeluarkan = $list_keluar[$x];
                    $baru = $list_baru[$x];
                    
                    $kec = $data['kab'][$x]['nmkec']; 
                    $kel = $data['kab'][$x]['nmdesa'];
                    $ar_ruta[$x] = array('kec'=>$kec,'kel'=>$kel,'perbaiki'=>$perbaiki,'keluar'=>$dikeluarkan,'baru'=>$baru);
                    $kec = ""; $kel = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                }
                return $ar_ruta;
            }
            elseif($data['num']>1){
                $data['ruta'] = $query->result_array();
                $kel = explode(",",$data['ruta'][0]['kelurahan']);
                $perbaiki_ = array();
                $keluar_ = array();
                $baru_ = array();
                $perbaiki_2 = 0;
                $keluar_2 = 0;
                $baru_2 = 0;
                for($n=0;$n<$query->num_rows();$n++){
                    $perbaiki = explode(",",$data['ruta'][$n]['perbaiki']);
                    $keluar = explode(",",$data['ruta'][$n]['keluar']);
                    $baru = explode(",",$data['ruta'][$n]['baru']);
                    $perbaiki_[$n] = $perbaiki;
                    $keluar_[$n] = $keluar;
                    $baru_[$n] = $baru;
                }
                for($x=0;$x<count($kel);$x++){
                    for($n=0;$n<$query->num_rows();$n++){
                        $perbaiki_2 = $perbaiki_2 + $perbaiki_[$n][$x];
                        $keluar_2 = $keluar_2 + $keluar_[$n][$x];
                        $baru_2 = $baru_2 + $baru_[$n][$x];
                    }
                    $ar_ruta[$x] = array('kec'=>$kec_,'kel'=>$kel[$x],'perbaiki'=>$perbaiki_2,'keluar'=>$keluar_2,'baru'=>$baru_2,'status'=>"update");
                    $perbaiki_2 = 0; $keluar_2 = 0; $baru_2 = 0;
                }
                return $ar_ruta;
            }
            else{
                $data['ruta'] = $query->row_array();
                $kec = ""; $kel = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                $ar_ruta = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->where('nmkec',$kec_);
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    $kec = $data['kab'][$x]['nmkec']; 
                    $kel = $data['kab'][$x]['nmdesa'];
                    $ar_ruta[$x] = array('kec'=>$kec,'kel'=>$kel,'perbaiki'=>$perbaiki,'keluar'=>$dikeluarkan,'baru'=>$baru);
                    $kec = ""; $kel = ""; $perbaiki = 0; $dikeluarkan = 0; $baru = 0;
                }
                return $ar_ruta;
            }
        }
    }

    public function rekap_kota_art($kota,$kec_,$kel_,$bulan,$tahun){
        $ar_ruta = array();
        $this->db->where('kota',$kota);
        if($kec_!="semua"){
            $this->db->where('kecamatan',$kec_);
        }
        if($bulan!="semua"){
            $this->db->like('tahun',$tahun."-".$bulan);
        }
        else{
            $this->db->like('tahun',$tahun);
        }
        $this->db->order_by('kecamatan');
        $query = $this->db->get('data_art');
        $data['num'] = $query->num_rows();
        if($kec_=="semua"){
            if($data['num']==1){
                $data['art'] = $query->row_array();
                $kec = ""; 
                $tinggal_di_ruta = 0; 
                $meninggal = 0; 
                $pindah = 0;
                $baru = 0;
                $kesalahan_prelist = 0;
                $tidak_ditemukan = 0;
                $usulan_baru = 0;

                $ar_ruta = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->group_by('nmkec');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    if($data['kab'][$x]['nmkec'] == $data['art']['kecamatan']){
                        $list_tinggal_di_ruta = explode(",",$data['art']['tinggal_di_ruta']);
                        for($a=0;$a<count($list_tinggal_di_ruta);$a++){
                            $tinggal_di_ruta = $tinggal_di_ruta + $list_tinggal_di_ruta[$a];
                        }
                        $list_meninggal = explode(",",$data['art']['meninggal']);
                        for($a=0;$a<count($list_meninggal);$a++){
                            $meninggal = $meninggal + $list_meninggal[$a];
                        }
                        $list_pindah = explode(",",$data['art']['pindah']);
                        for($a=0;$a<count($list_pindah);$a++){
                            $pindah = $pindah + $list_pindah[$a];
                        }
                        $list_baru = explode(",",$data['art']['baru']);
                        for($a=0;$a<count($list_baru);$a++){
                            $baru = $baru + $list_baru[$a];
                        }
                        $list_kesalahan_prelist = explode(",",$data['art']['kesalahan_prelist']);
                        for($a=0;$a<count($list_kesalahan_prelist);$a++){
                            $kesalahan_prelist = $kesalahan_prelist + $list_kesalahan_prelist[$a];
                        }
                        
                        $list_tidak_ditemukan = explode(",",$data['art']['tidak_ditemukan']);
                        for($a=0;$a<count($list_tidak_ditemukan);$a++){
                            $tidak_ditemukan = $tidak_ditemukan + $list_tidak_ditemukan[$a];
                        }
                        
                        $list_usulan_baru = explode(",",$data['art']['usulan_baru']);
                        for($a=0;$a<count($list_usulan_baru);$a++){
                            $usulan_baru = $usulan_baru + $list_usulan_baru[$a];
                        }
                    }
                    
                    $kec = $data['kab'][$x]['nmkec']; 
                    $ar_ruta[$x] = array('kec'=>$kec,'tinggal_di_ruta'=>$tinggal_di_ruta,'meninggal'=>$meninggal,'pindah'=>$pindah,'baru'=>$baru,'kesalahan_prelist'=>$kesalahan_prelist,'tidak_ditemukan'=>$tidak_ditemukan,'usulan_baru'=>$usulan_baru);
                    $kec = ""; $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0; $baru = 0;
                    $kesalahan_prelist = 0; $tidak_ditemukan = 0; $usulan_baru = 0;
                }
                return $ar_ruta;
            }
            elseif($data['num']>1){
                $data['art'] = $query->result_array();
                $kec = ""; 
                $tinggal_di_ruta = 0; 
                $meninggal = 0; 
                $pindah = 0;
                $baru = 0;
                $kesalahan_prelist = 0;
                $tidak_ditemukan = 0;
                $usulan_baru = 0;

                $ar_ruta = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->group_by('nmkec');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    for($z=0;$z<count($data['art']);$z++){
                        if($data['kab'][$x]['nmkec'] == $data['art'][$z]['kecamatan']){
                            $list_tinggal_di_ruta = explode(",",$data['art'][$z]['tinggal_di_ruta']);
                            for($a=0;$a<count($list_tinggal_di_ruta);$a++){
                                $tinggal_di_ruta = $tinggal_di_ruta + $list_tinggal_di_ruta[$a];
                            }
                            $list_meninggal = explode(",",$data['art'][$z]['meninggal']);
                            for($a=0;$a<count($list_meninggal);$a++){
                                $meninggal = $meninggal + $list_meninggal[$a];
                            }
                            $list_pindah = explode(",",$data['art'][$z]['pindah']);
                            for($a=0;$a<count($list_pindah);$a++){
                                $pindah = $pindah + $list_pindah[$a];
                            }
                            $list_baru = explode(",",$data['art'][$z]['baru']);
                            for($a=0;$a<count($list_baru);$a++){
                                $baru = $baru + $list_baru[$a];
                            }
                            $list_kesalahan_prelist = explode(",",$data['art'][$z]['kesalahan_prelist']);
                            for($a=0;$a<count($list_kesalahan_prelist);$a++){
                                $kesalahan_prelist = $kesalahan_prelist + $list_kesalahan_prelist[$a];
                            }
                            
                            $list_tidak_ditemukan = explode(",",$data['art'][$z]['tidak_ditemukan']);
                            for($a=0;$a<count($list_tidak_ditemukan);$a++){
                                $tidak_ditemukan = $tidak_ditemukan + $list_tidak_ditemukan[$a];
                            }
                            
                            $list_usulan_baru = explode(",",$data['art'][$z]['usulan_baru']);
                            for($a=0;$a<count($list_usulan_baru);$a++){
                                $usulan_baru = $usulan_baru + $list_usulan_baru[$a];
                            }
                        
                        }
                    }
                    $kec = $data['kab'][$x]['nmkec']; 
                    $ar_ruta[$x] = array('kec'=>$kec,'tinggal_di_ruta'=>$tinggal_di_ruta,'meninggal'=>$meninggal,'pindah'=>$pindah,'baru'=>$baru,'kesalahan_prelist'=>$kesalahan_prelist,'tidak_ditemukan'=>$tidak_ditemukan,'usulan_baru'=>$usulan_baru);
                    $kec = ""; $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0; $baru = 0;
                    $kesalahan_prelist = 0; $tidak_ditemukan = 0; $usulan_baru = 0;
                }
                return $ar_ruta;
            }
            else{
                $data['art'] = $query->row_array();
                $kec = ""; 
                $tinggal_di_ruta = 0; 
                $meninggal = 0; 
                $pindah = 0;
                $baru = 0;
                $kesalahan_prelist = 0;
                $tidak_ditemukan = 0;
                $usulan_baru = 0;

                $ar_ruta = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->group_by('nmkec');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    $kec = $data['kab'][$x]['nmkec']; 
                    $ar_ruta[$x] = array('kec'=>$kec,'tinggal_di_ruta'=>$tinggal_di_ruta,'meninggal'=>$meninggal,'pindah'=>$pindah,'baru'=>$baru,'kesalahan_prelist'=>$kesalahan_prelist,'tidak_ditemukan'=>$tidak_ditemukan,'usulan_baru'=>$usulan_baru);
                    $kec = ""; $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0; $baru = 0;
                    $kesalahan_prelist = 0; $tidak_ditemukan = 0; $usulan_baru = 0;
                }
                return $ar_ruta;
            }
        }
        else{//pilih kecamatan
            if($data['num']==1){
                $data['art'] = $query->row_array();
                $the_desa = explode(",",$data['art']['kelurahan']);
                $kec = ""; $kel = "";
                $tinggal_di_ruta = 0; 
                $meninggal = 0; 
                $pindah = 0;
                $baru = 0;
                $kesalahan_prelist = 0;
                $tidak_ditemukan = 0;
                $usulan_baru = 0;
                $ar_ruta = array();
                
                $this->db->where('nmkab',$kota);
                $this->db->where('nmkec',$kec_);
                $this->db->order_by('nmdesa','DSC');
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();

                $list_tinggal_di_ruta = explode(",",$data['art']['tinggal_di_ruta']);
                $list_meninggal = explode(",",$data['art']['meninggal']);
                $list_pindah = explode(",",$data['art']['pindah']);
                $list_baru = explode(",",$data['art']['baru']);
                $list_kesalahan_prelist = explode(",",$data['art']['kesalahan_prelist']);
                $list_tidak_ditemukan = explode(",",$data['art']['tidak_ditemukan']);
                $list_usulan_baru = explode(",",$data['art']['usulan_baru']);

                for($x=0;$x<count($the_desa);$x++){
                    $tinggal_di_ruta = $list_tinggal_di_ruta[$x];
                    $meninggal = $list_meninggal[$x];
                    $pindah = $list_pindah[$x];
                    $baru = $list_baru[$x];
                    $kesalahan_prelist = $list_kesalahan_prelist[$x];
                    $tidak_ditemukan = $list_tidak_ditemukan[$x];
                    $usulan_baru = $list_usulan_baru[$x];

                    $kec = $data['kab'][$x]['nmkec']; 
                    $kel = $data['kab'][$x]['nmdesa']; 
                    $ar_ruta[$x] = array('kec'=>$kec,'kel'=>$kel,'tinggal_di_ruta'=>$tinggal_di_ruta,'meninggal'=>$meninggal,'pindah'=>$pindah,'baru'=>$baru,'kesalahan_prelist'=>$kesalahan_prelist,'tidak_ditemukan'=>$tidak_ditemukan,'usulan_baru'=>$usulan_baru);
                    $kec = ""; $kel = ""; $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0; $baru = 0;
                    $kesalahan_prelist = 0; $tidak_ditemukan = 0; $usulan_baru = 0;
                }
                return $ar_ruta;
            }
            elseif($data['num']>1){
                $data['art'] = $query->row_array();
                $kel = explode(",",$data['art'][0]['kelurahan']);
                $tinggal_di_ruta_ = array(); 
                $meninggal_ = array(); 
                $pindah_ = array();
                $baru_ = array();
                $kesalahan_prelist_ = array();
                $tidak_ditemukan_ = array();
                $usulan_baru_ = array();

                $tinggal_di_ruta_2 = 0; 
                $meninggal_2 = 0; 
                $pindah_2 = 0;
                $baru_2 = 0;
                $kesalahan_prelist_2 = 0;
                $tidak_ditemukan_2 = 0;
                $usulan_baru_2 = 0;
                $ar_ruta = array();
                for($n=0;$n<$query->num_rows();$n++){
                    $tinggal_di_ruta = explode(",",$data['art'][$n]['tinggal_di_ruta']);
                    $meninggal = explode(",",$data['art'][$n]['meninggal']);
                    $pindah = explode(",",$data['art'][$n]['pindah']);
                    $baru = explode(",",$data['art'][$n]['baru']);
                    $kesalahan_prelist = explode(",",$data['art'][$n]['kesalahan_prelist']);
                    $tidak_ditemukan = explode(",",$data['art'][$n]['tidak_ditemukan']);
                    $usulan_baru = explode(",",$data['art'][$n]['usulan_baru']);

                    $tinggal_di_ruta_ = $tinggal_di_ruta; 
                    $meninggal_ = $meninggal; 
                    $pindah_ = $pindah;
                    $baru_ = $baru;
                    $kesalahan_prelist_ = $kesalahan_prelist;
                    $tidak_ditemukan_ = $tidak_ditemukan;
                    $usulan_baru_ = $usulan_baru;
                }
                for($x=0;$x<count($kel);$x++){
                    for($n=0;$n<$query->num_rows();$n++){
                        $perbaiki_2 = $perbaiki_2 + $perbaiki_[$n][$x];
                        $keluar_2 = $keluar_2 + $keluar_[$n][$x];
                        $baru_2 = $baru_2 + $baru_[$n][$x];

                        $tinggal_di_ruta_2 = $tinggal_di_ruta_2 + $tinggal_di_ruta_[$n][$x]; 
                        $meninggal_2 = $meninggal_2 + $meninggal_[$n][$x]; 
                        $pindah_2 = $pindah_2 + $pindah_[$n][$x];
                        $baru_2 = $baru_2 + $baru_[$n][$x];
                        $kesalahan_prelist_2 = $kesalahan_prelist_2 + $kesalahan_prelist_[$n][$x];
                        $tidak_ditemukan_2 = $tidak_ditemukan_2 + $tidak_ditemukan_[$n][$x];
                        $usulan_baru_2 = $usulan_baru_2 + $usulan_baru_[$n][$x];
                    }
                    $ar_ruta[$x] = array('kec'=>$kec_,'kel'=>$kel[$x],'tinggal_di_ruta'=>$tinggal_di_ruta_2,'meninggal'=>$meninggal_2,'pindah'=>$pindah_2,'baru'=>$baru_2,'kesalahan_prelist'=>$kesalahan_prelist_2,'tidak_ditemukan'=>$tidak_ditemukan_2,'usulan_baru'=>$usulan_baru_2);
                    $tinggal_di_ruta_2 = 0; $meninggal_2 = 0; $pindah_2 = 0;
                    $baru_2 = 0; $kesalahan_prelist_2 = 0; $tidak_ditemukan_2 = 0;
                    $usulan_baru_2 = 0;
                }
                return $ar_ruta;
            }
            else{
                $data['ruta'] = $query->row_array();
                $kec = ""; $kel = "";
                $ar_ruta = array();

                $this->db->where('nmkab',$kota);
                $this->db->where('nmkec',$kec_);
                $query = $this->db->get('kode_wil_kemendagri');
                $data['kab'] = $query->result_array();
                for($x=0;$x<count($data['kab']);$x++){
                    $kec = $data['kab'][$x]['nmkec']; 
                    $kel = $data['kab'][$x]['nmdesa'];
                    $ar_ruta[$x] = array('kec'=>$kec_,'kel'=>$kel,'tinggal_di_ruta'=>0,'meninggal'=>0,'pindah'=>0,'baru'=>0,'kesalahan_prelist'=>0,'tidak_ditemukan'=>0,'usulan_baru'=>0);
                    $kec = ""; $kel = "";
                }
                return $ar_ruta;
            }
        }
    }





}