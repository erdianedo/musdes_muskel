<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musdes_model extends CI_Model
{
    function show_kecamatan($id_kota){
        $this->db->like('kode',$id_kota);
        $this->db->order_by('nmkec','ASC');
        $this->db->group_by('nmkec');
        $query = $this->db->get('kode_wil_kemendagri');
        $output = '<option value="semua">-- Seluruh Kecamatan --</option>'; 
        foreach ($query->result_array() as $row) {
            $kode = explode(".",$row['kode']);
            $kd = $kode[0].".".$kode[1].".".$kode[2];
            $output .= '<option value="'.$kd.'_'.$row['nmkec'].'">'.$row['nmkec'].'</option>';
        }
        return $output;
    }    
    
    function show_kelurahan($kec){
        if($kec=="0_0"){
            $output = '<option value="semua">-- Seluruh Kelurahan --</option>';
        }
        else{
            $id_kec = explode("_",$kec);
            $this->db->like('kode',$id_kec[0]);
            $this->db->order_by('nmdesa','ASC');
            $query = $this->db->get('kode_wil_kemendagri');
            $output = '<option value="semua">-- Seluruh Kelurahan --</option>'; 
            foreach ($query->result_array() as $row) {
                $output .= '<option value="'.$row['kode'].'_'.$row['nmdesa'].'">'.$row['nmdesa'].'</option>';
            }
        }
        
        return $output;
    }    

    public function get_musdes($kec){
        $this->db->like('kode',$kec);
        $this->db->order_by('nmdesa','ASC');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->row_array();
    }

    public function get_kel($kec){
        $this->db->where('nmkec',$kec);
        $this->db->order_by('nmdesa','ASC');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->result_array();
    }

    public function get_kec_by_kota($kota){
        $this->db->where('nmkab',$kota);
        $this->db->order_by('nmkec','ASC');
        $this->db->group_by('nmkec');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->result_array();
    }

    public function laporan_kec_kel($kab,$kec,$bulan,$tahun){
        if($kec!="dinsos"){//user kecamatan
            $ar_all = array();
            $this->db->where('nmkab',$kab);
            $this->db->where('nmkec',$kec);
            $this->db->order_by('nmdesa','ASC');
            $query = $this->db->get('kode_wil_kemendagri');
            $data['kel'] = $query->result_array();
            $tgl = "";
            for($x=0;$x<count($data['kel']);$x++){
                $tgl = "";
                if($bulan=="semua"){
                    $this->db->where('nmkab',$data['kel'][$x]['nmkab']);
                    $this->db->where('nmkec',$data['kel'][$x]['nmkec']);
                    $this->db->where('nmdesa',$data['kel'][$x]['nmdesa']);
                    $this->db->like('tanggal',$tahun);
                    $this->db->order_by('tanggal','ASC');
                    $query1 = $this->db->get('laporan');
                    $data['num'] = $query1->num_rows();
                }
                else{
                    $this->db->where('nmkab',$data['kel'][$x]['nmkab']);
                    $this->db->where('nmkec',$data['kel'][$x]['nmkec']);
                    $this->db->where('nmdesa',$data['kel'][$x]['nmdesa']);
                    $this->db->like('tanggal',$tahun."-".$bulan);
                    $this->db->order_by('tanggal','ASC');
                    $query1 = $this->db->get('laporan');
                    $data['num'] = $query1->num_rows();
                }
                if($data['num']==1){
                    $data['kel2'] = $query1->row_array();
                    $hari = date("l", strtotime($data['kel2']['tanggal']));
                    $hari_ = "";
                    switch($hari){
                        case 'Monday':
                            $hari_ = "Senin";
                        break;
                        case 'Tuesday':
                            $hari_ = "Selasa";
                        break;
                        case 'Wednesday':
                            $hari_ = "Rabu";
                        break;
                        case 'Thursday':
                            $hari_ = "Kamis";
                        break;
                        case 'Friday':
                            $hari_ = "Jumat";
                        break;
                        case 'Saturday':
                            $hari_ = "Sabtu";
                        break;
                        case 'Sunday':
                            $hari_ = "Minggu";
                        break;
                        default:
                        break;
                    }
                    $bulan_ = explode("-",$data['kel2']['tanggal']);
                    $nm_bln = $this->Kec_model->bulan_ini_saja2($bulan_[1]);
                    $tgl = "- ".$hari_.",".$bulan_[2]." ".$nm_bln." ".$bulan_[0];
                }
                elseif($data['num']>1){
                    $data['kel2'] = $query1->result_array();
                    for($z=0;$z<$data['num'];$z++){
                        $hari = date("l", strtotime($data['kel2'][$z]['tanggal']));
                        $hari_ = "";
                        switch($hari){
                            case 'Monday':
                                $hari_ = "Senin";
                            break;
                            case 'Tuesday':
                                $hari_ = "Selasa";
                            break;
                            case 'Wednesday':
                                $hari_ = "Rabu";
                            break;
                            case 'Thursday':
                                $hari_ = "Kamis";
                            break;
                            case 'Friday':
                                $hari_ = "Jumat";
                            break;
                            case 'Saturday':
                                $hari_ = "Sabtu";
                            break;
                            case 'Sunday':
                                $hari_ = "Minggu";
                            break;
                            default:
                            break;
                        }
                        $bulan_ = explode("-",$data['kel2'][$z]['tanggal']);
                        $nm_bln = $this->Kec_model->bulan_ini_saja2($bulan_[1]);
                        if($data['num']==1){
                            $tgl = "- ".$hari_.", ".$bulan_[2]." ".$nm_bln." ".$bulan_[0];
                        } 
                        else{
                            if($z==0){
                                $tgl = "- ".$hari_.", ".$bulan_[2]." ".$nm_bln." ".$bulan_[0];
                            }
                            else{
                                $tgl = $tgl."<br>- ".$hari_.", ".$bulan_[2]." ".$nm_bln." ".$bulan_[0];
                            }
                        }
                    }
                }
                else{
                    $tgl = "<span style='color:red'>Belum Melaksanakan</span>";
                }
                
                $ar_all[$x] = array("nmdesa"=>$data['kel'][$x]['nmdesa'],"total"=>$data['num'],"tgl"=>$tgl);
                $tgl = "";
            }
            return $ar_all;
        }
        else{//user provinsi
            $this->db->order_by('nmkab','ASC');
            $this->db->group_by('nmkab');
            $query = $this->db->get('kode_wil_kemendagri');
            return $query->result_array();

        }
    }

    public function laporan_dinsos($bulan,$tahun){
        $this->db->order_by('nmkab','ASC');
        $query = $this->db->get('kode_wil_kemendagri');
        $data['kota'] = $query->result_array();
        $ar_all = array();
        $temp_kota = "kosong";
        $jml_kel = 0;
        $jml_belum = 0;
        $jml_sudah = 0;
        $jml_query = count($data['kota']);
        $index = 0;
        for($x=0;$x<$jml_query;$x++){
            
            $this->db->where('nmdesa',$data['kota'][$x]['nmdesa']);
            $this->db->like('tanggal',$tahun."-".$bulan);
            $query = $this->db->get('laporan');
            $data['lap'] = $query->row_array();
            $jml = $query->num_rows();
            if($x+1 == $jml_query){
                $ar_all[$index] = array("nmkab"=>$temp_kota,"total_kel"=>$jml_kel,"jml_sudah"=>$jml_sudah,"jml_belum"=>$jml_belum);
                $jml_kel = 0; $jml_belum = 0; $jml_sudah = 0;
                $index++;
            }
            elseif($x == 0 || $temp_kota == "kosong" || $temp_kota == $data['kota'][$x]['nmkab']){
                $jml_kel++;
                if($jml>0){
                    $jml_sudah++;
                }   
                else{
                    $jml_belum++;
                }
            }
            elseif($temp_kota != $data['kota'][$x]['nmkab'] || $x == 8558){
                $ar_all[$index] = array("nmkab"=>$temp_kota,"total_kel"=>$jml_kel,"jml_sudah"=>$jml_sudah,"jml_belum"=>$jml_belum);
                $jml_kel = 0; $jml_belum = 0; $jml_sudah = 0;
                $index++;
            }
            $temp_kota = $data['kota'][$x]['nmkab'];
        }
        return $ar_all;
    }

    public function laporan_kec_bykota($kota,$bulan,$tahun){
        $this->db->where('nmkab',$kota);
        $this->db->order_by('nmkec','ASC');
        $this->db->group_by('nmkec');
        $query = $this->db->get('kode_wil_kemendagri');
        $data['kota'] = $query->result_array();
        $ar_all = array();

        for($x=0;$x<count($data['kota']);$x++){
            $this->db->where('nmkab',$data['kota'][$x]['nmkab']);
            $this->db->where('nmkec',$data['kota'][$x]['nmkec']);
            $this->db->like('tanggal',$tahun."-".$bulan);
            $this->db->order_by('nmkec','ASC');
            $query = $this->db->get('laporan');
            $data['lap'] = $query->result_array();
            $jml = $query->num_rows();
            $ar_all[$x] = array("nmkec"=>$data['kota'][$x]['nmkec'],"jml"=>$jml);
        }
        return $ar_all;
    }
    public function laporan_kec_bykota2($kota,$bulan,$tahun){
        $this->db->where('nmkab',$kota);
        $this->db->order_by('nmkec','ASC');
        $query = $this->db->get('kode_wil_kemendagri');
        $data['kota'] = $query->result_array();
        $ar_all2 = array();
        for($x=0;$x<count($data['kota']);$x++){
            $this->db->where('nmkab',$data['kota'][$x]['nmkab']);
            $this->db->where('nmkec',$data['kota'][$x]['nmkec']);
            $this->db->where('nmdesa',$data['kota'][$x]['nmdesa']);
            $this->db->like('tanggal',$tahun."-".$bulan);
            $query = $this->db->get('laporan');
            $data['lap'] = $query->result_array();
            if($query->num_rows()>0){
                $sts = "<span style='color:green'>Sudah Melaksanakan</span>";
            }
            else{
                $sts = "<span style='color:red'>Belum Melaksanakan</span>";
            }
            $jml = $query->num_rows();
            $ar_all2[$x] = array("nmkec"=>$data['kota'][$x]['nmkec'], "nmdesa"=>$data['kota'][$x]['nmdesa'],"status"=>$sts, "jml"=>$jml);
        }
        return $ar_all2;
    }

    public function laporan_rekap($kota,$kec,$kel,$bulan,$tahun){
        if($kota!="semua"){//kota pilih
            if($kec=="semua"){//kota pilih, semua kecamatan, semua kelurahan
                $this->db->where('nmkab',$kota);
                $this->db->order_by('nmkab');
            }
            else{//kota pilih, semua kecamatan, semua kelurahan
                if($kel=="semua"){
                    $this->db->where('nmkab',$kota);
                    $this->db->where('nmkec',$kec);
                }
                else{
                    $this->db->where('nmkab',$kota);
                    $this->db->where('nmkec',$kec);
                    $this->db->where('nmdesa',$kel);
                }
                
            }
        }
        
        $this->db->order_by('nmkab');
        $query = $this->db->get('kode_wil_kemendagri');
        $data['rekap'] = $query->result_array();

        

        $nmkab = array();
        $nmkec = array();
        $nmdesa = array();
        $jml_musdes = array();
        $tgl_musdes = array();
        $foto1 = array();
        $foto2 = array();
        $berita = array();

        $final_rekap = array();

        for($x=0;$x<count($data['rekap']);$x++){
            $nmkab[$x] = $data['rekap'][$x]['nmkab'];
            $nmkec[$x] = $data['rekap'][$x]['nmkec'];
            $nmdesa[$x] = $data['rekap'][$x]['nmdesa'];
            $tgl = "";
            $this->db->where('nmkec',$data['rekap'][$x]['nmkec']);
            $this->db->where('nmdesa',$data['rekap'][$x]['nmdesa']);
            if($bulan=="semua"){
                $this->db->like('tanggal',$tahun);
            }
            else{
                $this->db->like('tanggal',$tahun."-".$bulan);
            }
            $this->db->order_by('tanggal','ASC');
            $query = $this->db->get('laporan');
            $data['musdes'] = $query->result_array();
            
            $jml_musdes[$x] = count($data['musdes']);
            if(count($data['musdes'])>0){
                for($z=0;$z<count($data['musdes']);$z++){
                    if($data['musdes'][$z]['foto1']!=""){
                        $foto1[$x] = $data['musdes'][$z]['foto1'];
                    }
                    else{
                        $foto1[$x] = "kosong";
                    }

                    if($data['musdes'][$z]['foto2']!=""){
                        $foto2[$x] = $data['musdes'][$z]['foto2'];
                    }
                    else{
                        $foto2[$x] = "kosong";
                    }

                    if($data['musdes'][$z]['berita_acara']!=""){
                        $berita[$x] = $data['musdes'][$z]['berita_acara'];
                    }
                    else{
                        $berita[$x] = "kosong";
                    }

                    $hari = date("l", strtotime($data['musdes'][$z]['tanggal']));
                    $hari_ = "";
                    switch($hari){
                        case 'Monday':
                            $hari_ = "Senin";
                        break;
                        case 'Tuesday':
                            $hari_ = "Selasa";
                        break;
                        case 'Wednesday':
                            $hari_ = "Rabu";
                        break;
                        case 'Thursday':
                            $hari_ = "Kamis";
                        break;
                        case 'Friday':
                            $hari_ = "Jumat";
                        break;
                        case 'Saturday':
                            $hari_ = "Sabtu";
                        break;
                        case 'Sunday':
                            $hari_ = "Minggu";
                        break;
                        default:
                        break;
                    }
                    $tgl_msds = explode("-",$data['musdes'][$z]['tanggal']);
                    $nm_bulan = $this->Kec_model->bulan_ini_saja2($tgl_msds[1]);
                    if(count($data['musdes'])==1){
                        $tgl = "- ".$hari_.", ".$tgl_msds[2]." ".$nm_bulan." ".$tgl_msds[0];
                    } 
                    elseif(count($data['musdes'])>1){
                        if($z==0){
                            $tgl = "- ".$hari_.", ".$tgl_msds[2]." ".$nm_bulan." ".$tgl_msds[0];
                        }
                        else{
                            $tgl = $tgl."<br>- ".$hari_.", ".$tgl_msds[2]." ".$nm_bulan." ".$tgl_msds[0];
                        }
                    }
                }
            }
            else{
                $tgl = "<span style='color:red'>Belum Melaksanakan</span>";
                $foto1[$x] = "kosong";
                $foto2[$x] = "kosong";
                $berita[$x] = "kosong";
            }
            $tgl_musdes[$x] = $tgl;
            $final_rekap[$x] = array('no'=>$x+1,'nmkab'=>$nmkab[$x],'nmkec'=>$nmkec[$x],'nmdesa'=>$nmdesa[$x],'jml_musdes'=>$jml_musdes[$x],'tgl_musdes'=>$tgl_musdes[$x],'foto1'=>$foto1[$x],'foto2'=>$foto2[$x],'berita'=>$berita[$x]);
        }
        return $final_rekap;
    }

    // data table
    // data table
    function barang_list($kota,$kec,$kel,$bulan,$tahun){
        if($kota!="semua"){//kota pilih
            if($kec=="semua"){//kota pilih, semua kecamatan, semua kelurahan
                $this->db->where('nmkab',$kota);
                $this->db->order_by('nmkab');
            }
            else{//kota pilih, semua kecamatan, semua kelurahan
                if($kel=="semua"){
                    $this->db->where('nmkab',$kota);
                    $this->db->where('nmkec',$kec);
                }
                else{
                    $this->db->where('nmkab',$kota);
                    $this->db->where('nmkec',$kec);
                    $this->db->where('nmdesa',$kel);
                }
                
            }
        }
        $this->db->order_by('nmkab');
        $query = $this->db->get('kode_wil_kemendagri');
        $data['rekap'] = $query->result_array();

        $nmkab = array();
        $nmkec = array();
        $nmdesa = array();
        $jml_musdes = array();
        $tgl_musdes = array();
        $foto1 = array();
        $foto2 = array();
        $berita = array();

        $final_rekap = array();

        for($x=0;$x<count($data['rekap']);$x++){
            $nmkab[$x] = $data['rekap'][$x]['nmkab'];
            $nmkec[$x] = $data['rekap'][$x]['nmkec'];
            $nmdesa[$x] = $data['rekap'][$x]['nmdesa'];
            $tgl = "";
            $this->db->where('nmdesa',$data['rekap'][$x]['nmdesa']);
            if($bulan=="semua"){
                $this->db->like('tanggal',$tahun);
            }
            else{
                $this->db->like('tanggal',$tahun."-".$bulan);
            }
            $this->db->order_by('tanggal','ASC');
            $query = $this->db->get('laporan');
            $data['musdes'] = $query->result_array();
            $jml_musdes[$x] = count($data['musdes']);
            if(count($data['musdes'])>0){
                for($z=0;$z<count($data['musdes']);$z++){
                    if($data['musdes'][$z]['foto1']!=""){
                        $foto1[$x] = $data['musdes'][$z]['foto1'];
                    }
                    else{
                        $foto1[$x] = "kosong";
                    }

                    if($data['musdes'][$z]['foto2']!=""){
                        $foto2[$x] = $data['musdes'][$z]['foto2'];
                    }
                    else{
                        $foto2[$x] = "kosong";
                    }

                    if($data['musdes'][$z]['berita_acara']!=""){
                        $berita[$x] = $data['musdes'][$z]['berita_acara'];
                    }
                    else{
                        $berita[$x] = "kosong";
                    }

                    $hari = date("l", strtotime($data['musdes'][$z]['tanggal']));
                    $hari_ = "";
                    switch($hari){
                        case 'Monday':
                            $hari_ = "Senin";
                        break;
                        case 'Tuesday':
                            $hari_ = "Selasa";
                        break;
                        case 'Wednesday':
                            $hari_ = "Rabu";
                        break;
                        case 'Thursday':
                            $hari_ = "Kamis";
                        break;
                        case 'Friday':
                            $hari_ = "Jumat";
                        break;
                        case 'Saturday':
                            $hari_ = "Sabtu";
                        break;
                        case 'Sunday':
                            $hari_ = "Minggu";
                        break;
                        default:
                        break;
                    }
                    if(count($data['musdes'])==1){
                        $tgl = "- ".$hari_.", ".$data['musdes'][$z]['tanggal'];
                    } 
                    elseif(count($data['musdes'])>1){
                        if($z==0){
                            $tgl = "- ".$hari_.", ".$data['musdes'][$z]['tanggal'];
                        }
                        else{
                            $tgl = $tgl."<br>- ".$hari_.", ".$data['musdes'][$z]['tanggal'];
                        }
                    }
                }
            }
            else{
                $tgl = "<span style='color:red'>Belum Melaksanakan</span>";
                $foto1[$x] = "kosong";
                $foto2[$x] = "kosong";
                $berita[$x] = "kosong";
            }
            $tgl_musdes[$x] = $tgl;
            $final_rekap[$x] = array('no'=>$x+1,'nmkab'=>$nmkab[$x],'nmkec'=>$nmkec[$x],'nmdesa'=>$nmdesa[$x],'jml_musdes'=>$jml_musdes[$x],'tgl_musdes'=>$tgl_musdes[$x],'foto1'=>$foto1[$x],'foto2'=>$foto2[$x],'berita'=>$berita[$x]);
        }
        return $final_rekap;
    }
    // end data table
    // end data table

    public function laporan_kec($kec,$kel,$bulan,$tahun){
        if($kel != "semua" && $bulan!="semua"){
            $this->db->where('nmdesa',$kel);
            $this->db->like('id_desa',$kec);
            $this->db->like('tanggal',$tahun."-".$bulan);
            $this->db->order_by('tanggal','ASC');
            $query = $this->db->get('laporan');
        }
        elseif($kel!="semua" && $bulan=="semua"){
            $this->db->where('nmdesa',$kel);
            $this->db->like('id_desa',$kec);
            $this->db->like('tanggal',$tahun);
            $this->db->order_by('tanggal','ASC');
            $query = $this->db->get('laporan');
        }
        elseif($kel=="semua" && $bulan!="semua"){
            $this->db->like('id_desa',$kec);
            $this->db->like('tanggal',$tahun."-".$bulan);
            $this->db->order_by('tanggal','ASC');
            $query = $this->db->get('laporan');
        }
        elseif($kel=="semua" && $bulan=="semua"){
            $this->db->like('id_desa',$kec);
            $this->db->like('tanggal',$tahun);
            $this->db->order_by('tanggal','ASC');
            $query = $this->db->get('laporan');
        }
        
        return $query->result_array();
    }

    public function getMusdesByID($id){
        $this->db->where('nmkec',$kec);
        $this->db->order_by('nmdesa','ASC');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->row_array();
    }

    public function getKelByDesaByKec($kab,$kec){
        $this->db->where('nmkab',$kab);
        $this->db->where('nmkec',$kec);
        $this->db->order_by('nmdesa','ASC');
        $query = $this->db->get('kode_wil_kemendagri');
        return $query->result_array();
    }

    public function input_laporan(){
        $kel = $this->input->post('desa', true);
        $tgl = explode("/",$this->input->post('tanggal_pelaksanaan', true));
        $desa = explode("_",$kel);
        $tanggal = $tgl[2]."-".$tgl[0]."-".$tgl[1];

        $huruf = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

        $a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);$e = rand(0,25);

        $id = $huruf[$a].$c.$d.$huruf[$b].$huruf[$e];

        $data = [
            "id" => $id."_".$tanggal,
            "id_desa" => $desa[0],
            "nmkab" => $desa[3],
            "nmkec" => $desa[2],
            "nmdesa" => $desa[1],
            "tanggal" => $tanggal
        ];
        $this->db->insert('laporan',$data);
        $this->session->set_flashdata('flash_input','Tanggal Musdes/Muskel Untuk Kelurahan '.$desa[1].' Berhasil Ditambahkan');
        redirect('musdes/laporan');   
    }

    public function edit_musdes($id,$kec,$desa,$bulan,$tahun,$nmdesa,$tgl_){
        $tgl = explode("/",$tgl_);
        $tanggal = $tgl[2]."-".$tgl[0]."-".$tgl[1];
        $data = [
            "tanggal" => $tanggal
        ];
        $this->db->where('id',$id);
        $this->db->update('laporan',$data);
        $this->session->set_flashdata('flash_edit','Tanggal Musdes/Muskel Untuk Kelurahan <b>'.$nmdesa.'</b> Berhasil Diperbaharui');
        redirect('musdes/laporan_kecamatan/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
    }

    public function upload_foto1(){

        $huruf = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

        $a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);
        $a2 = rand(0,25);$b2 = rand(0,25);$c2 = rand(0,9);$d2 = rand(0,9);
        $a3 = rand(0,25);$b3 = rand(0,25);$c3 = rand(0,9);$d3 = rand(0,9);

        $id = $huruf[$a].$c.$d.$huruf[$b];
        $id2 = $huruf[$a2].$c2.$d2.$huruf[$b2];
        $id3 = $huruf[$a3].$c3.$d3.$huruf[$b3];

        $kel = $this->input->post('desa', true);
        $tgl = explode("/",$this->input->post('tanggal_pelaksanaan', true));
        $desa = explode("_",$kel);
        $tanggal = $tgl[2]."-".$tgl[0]."-".$tgl[1];

        $filename=preg_replace('/[^A-Za-z0-9\.]/', '', $_FILES['dok1_img']['name']);
        $filename2=preg_replace('/[^A-Za-z0-9\.]/', '', $_FILES['dok2_img']['name']);
        $filename3=preg_replace('/[^A-Za-z0-9\.]/', '', $_FILES['berita_acara']['name']);

        $nama_gambar1 = $id."_".$tanggal."_".$desa[1];
        $nama_gambar2 = $id2."_".$tanggal."_".$desa[1];
        $nama_gambar3 = $id3."_".$tanggal."_".$desa[1];

        $config1['upload_path'] = './assets/images/dokumentasi_musdes/foto';
        $config1['allowed_types'] = 'jpg|jpeg';
        $config1['file_name'] = $nama_gambar1; 
        $config1['max_size'] = 520;//520kb max
        $this->load->library('upload', $config1);

        $config2['upload_path'] = './assets/images/dokumentasi_musdes/foto';
        $config2['allowed_types'] = 'jpg|jpeg';
        $config2['file_name'] = $nama_gambar2; 
        $config2['max_size'] = 520;//520kb max
        $this->load->library('upload', $config2);

        $config3['upload_path'] = './assets/images/dokumentasi_musdes/berita_acara';
        $config3['allowed_types'] = 'jpg|jpeg|pdf';
        $config3['file_name'] = $nama_gambar3; 
        $config3['max_size'] = 520;//520kb max
        $this->load->library('upload', $config3);

        $tipe1 =  explode("/", $_FILES['dok1_img']['type']);
        $tipe2 =  explode("/", $_FILES['dok2_img']['type']);
        $tipe3 =  explode("/", $_FILES['berita_acara']['type']);
        
        // var_dump($nama_gambar2.".".$tipe2[1]);
        // exit();
        
        if (!$this->upload->do_upload('dok1_img') || !$this->upload->do_upload('dok2_img') || !$this->upload->do_upload('berita_acara')) {
            
            $error = array('error' => $this->upload->display_errors());
            
            var_dump($error);

            $ukuran1 = $_FILES['dok1_img']['size'];
            $ukuran2 = $_FILES['dok2_img']['size'];
            $ukuran3 = $_FILES['berita_acara']['size'];
            
            if($tipe1[0] != "image"){
                $this->session->set_flashdata('flash_gambar1_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
            }
            if($ukuran1 > 520000){
                $this->session->set_flashdata('flash_gambar1_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Ukuran File Terlalu Besar');
            }
            // redirect('musdes/laporan');
        }
        else {
            
            $data = [
                "id" => $id."_".$tanggal,
                "id_desa" => $desa[0],
                "nmkec" => $desa[2],
                "nmdesa" => $desa[1],
                "tanggal" => $tanggal,
                "foto1" => $nama_gambar1.".".$tipe1[1],
                "foto2" => $nama_gambar2.".".$tipe2[1],
                "berita_acara" => $nama_gambar3.".".$tipe3[1]
            ];
            $this->db->insert('laporan',$data);
                $this->session->set_flashdata('flash_gambar','Gambar Profil Berhasil Diunggah');
            // redirect('profile/account/'.$id);   
        }
    }

}