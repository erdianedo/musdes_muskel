<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    //fungsi cek session
    public function logged_id()
    {
        return $this->session->userdata('user_id');
    }

    public function get_logged_id($id){
        return $this->db->get_where('user', ['id'=>$id])->row_array();
    }

    public function getAllUser(){
        $query = $this->db->get('pegawai');
        return $query->result_array();
    }

    public function getUserByID($id){
        $this->db->where('nip',$id);
        $query = $this->db->get('pegawa');
        return $query->row_array();
    }

    //fungsi check login
    public function check_login($table, $field1, $field2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field1);
        $this->db->where($field2);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    public function tambahAdmin(){

        $nip = $this->input->post('nip', true);
        $nama = $this->input->post('nama', true);
        $password = $this->input->post('password', true);
        $status = $this->input->post('status', true);
        $bidang = $this->input->post('bidang', true);
        $sub_bidang = $this->input->post('sub_bidang', true);
        $jabatan = $this->input->post('jabatan', true);
        $pangkat = $this->input->post('pangkat', true);
        $golongan = $this->input->post('golongan', true);
        
        $passw = md5($pass1);

        $data = [
            "nip" => $nip,
            "nama" => $nama,
            "password" => $passw,
            "status" => $status,
            "bidang" => $bidang,
            "sub_bidang" => $sub_bidang,
            "jabatan" => $jabatan,
            "pangkat" => $pangkat,
            "golongan" => $golongan,
            "tanggal_dinas" => ""
        ];
        $this->db->insert('pegawai',$data);
        
        
    }

    public function hapusDataAdmin($id){
        $data['pegawai'] = $this->Admin_model->get_logged_id($id);

        $this->db->where('nip',$id);
        $this->db->delete('pegawai');

    }

}