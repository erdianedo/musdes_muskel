<style type="text/css">
	.navigation .menu li.dashboard a{
		background-color: #63d5db !important;
		color: #fff;
	}
	.widget-stat {
	    text-align: left;
	}
</style>
<!-- script horizontal bar -->
	<style>
    #chart {
      max-width: 650px;
      margin: 35px auto;
    }
      
    </style>

    <script>
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/eligrey-classlist-js-polyfill@1.2.20171210/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="https://cdn.jsdelivr.net/npm/findindex_polyfill_mdn"><\/script>'
        )
    </script>

    
    <script src="https://cdn.jsdelivr.net/npm/react@16.12/umd/react.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-dom@16.12/umd/react-dom.production.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/prop-types@15.7.2/prop-types.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://cdn.jsdelivr.net/npm/react-apexcharts@1.3.6/dist/react-apexcharts.iife.min.js"></script>
    

    <script>
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42
      Math.random = function() {
        _seed = (_seed * 16807) % 2147483647
        return (_seed - 1) / 2147483646
      }
    </script>
<!-- end horizon bar -->
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height:600px">
					<h4 class="box-title">Dashboard</h4>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div><br>
						<?php $text = "Rekap Musdes / Muskel" ?>
						<?php $id = explode(".",$sesion_data['id']); ?>
						<?php if($id[1]=="00" && $id[2]=="00"): ?>
							<?php $text2 = "Kota / Kabupaten se-Jawa Tengah" ?>
						<?php elseif($id[1]!="00" && $id[2]=="00"):?>
							<?php $text2 = "se -".$sesion_data['nama'] ?>
						<?php else:?>
							<?php $text2 = "Kecamatan ".strtoupper( $sesion_data['nama']) ?>
						<?php endif;?>
						<?php $text3 = "Bulan ".$bulan." Tahun ".$tahun ?>
						<?php if($id[1]=="00" && $id[2]=="00"): ?>
							<div class="col-lg-12 col-xs-12">
								<div class="box-content">
									<h2><center><?= $text ?><br><?= $text2 ?><br><?= $text3 ?></center></h2>
									<div class="content">
										<div id="charts3"></div>
										<script>
										var options = {
											series: [{
											data: [<?php foreach($rekap_prov as $rk): ?>
													<?=$rk['jml_sudah']?>,
												<?php endforeach;?>]
											}],
											chart: {
											type: 'bar',
											height: 350
											},
											plotOptions: {
											bar: {
												horizontal: true,
											}
											},
											dataLabels: {
											enabled: false
											},
											xaxis: {
												categories: [<?php foreach($rekap_prov as $rk): ?>
													'<?=$rk['nmkab']?>',
												<?php endforeach;?>
												],
											}
											};

											var chart = new ApexCharts(document.querySelector("#charts3"), options);
											chart.render();
										</script>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-xs-12">
								<div class="table-responsive">
									<table id="zero_config" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kab / Kota</th>
												<th>Jumlah Kelurahan / Desa<br></th>
												<th>Sudah Melaksanakan</th>
												<th>Belum Melaksanakan</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($rekap_prov as $pr2): ?>
												<tr>
													<td><?= $no++ ?></td>
													<td><?= $pr2['nmkab'] ?></td>
													<td><?= $pr2['total_kel'] ?></td>
													<td><?= $pr2['jml_sudah'] ?></td>
													<td><?= $pr2['jml_belum'] ?></td>
												</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>
							</div>
						<?php elseif($id[1]!="00" && $id[2]=="00"): ?>
							<div class="col-lg-12 col-xs-12">
								<div class="box-content">
									<h2><center><?= $text ?> <?= $text2 ?><br><?= $text3 ?></center></h2>
									<div class="content">
										<div id="charts3"></div>
										<script>
										var options = {
											series: [{
											data: [<?php foreach($rekap_kota as $rk): ?>
													<?=$rk['jml']?>,
												<?php endforeach;?>]
											}],
											chart: {
											type: 'bar',
											height: 350
											},
											plotOptions: {
											bar: {
												horizontal: true,
											}
											},
											dataLabels: {
											enabled: false
											},
											xaxis: {
												categories: [<?php foreach($rekap_kota as $rk): ?>
													'<?=$rk['nmkec']?>',
												<?php endforeach;?>
												],
											}
											};

											var chart = new ApexCharts(document.querySelector("#charts3"), options);
											chart.render();
										</script>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-xs-12">
								<div class="table-responsive">
									<table id="zero_config" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kecamatan</th>
												<th>Kelurahan / Desa<br></th>
												<th>Total Pelaksanaan</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($rekap_kota2 as $rk2): ?>
												<tr>
													<td><?= $no++ ?></td>
													<td><?= $rk2['nmkec'] ?></td>
													<td><?= $rk2['nmdesa'] ?></td>
													<td><?= $rk2['jml'] ?></td>
													<td><?= $rk2['status'] ?></td>
												</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>
							</div>
						<?php elseif($id[1]!="00" && $id[2]!="00"): ?>
							<div class="col-lg-12 col-xs-12">
								<div class="box-content">
									<h2><center><?= $text ?> <?= $text2 ?><br><?= $text3 ?></center></h2>
									<div class="content">
										<!-- <div id="chart-2" class="js__chart" data-type="column" 
											data-chart="'Kelurahan'/'Terlaksana'
											<?php foreach($rekap_kec as $rk): ?>
												|'<?=$rk['nmdesa']?>'/<?=$rk['total']?>
											<?php endforeach;?>
											">
										</div> -->
										<div id="charts2"></div>
                                <script>
      
                                    var options = {
                                        series: [{
                                        data: [<?php foreach($rekap_kec as $rk): ?>
												<?=$rk['total']?>,
											<?php endforeach;?>]
                                        }],
                                        chart: {
                                        type: 'bar',
                                        height: 350
                                        },
                                        plotOptions: {
                                        bar: {
                                            horizontal: true,
                                        }
                                        },
                                        dataLabels: {
                                        enabled: false
                                        },
                                        xaxis: {
                                            categories: [<?php foreach($rekap_kec as $rk): ?>
												'<?=$rk['nmdesa']?>',
											<?php endforeach;?>
                ],
                                        }
                                        };

                                        var chart = new ApexCharts(document.querySelector("#charts2"), options);
                                        chart.render();

                                                                                                      
                                </script>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-xs-12">
								<div class="table-responsive">
									<table id="zero_config" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>No.</th>
												<th>Kelurahan / Desa<br></th>
												<th>Total Pelaksanaan</th>
												<th>Tanggal</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($rekap_kec as $rk): ?>
												<tr>
													<td><?= $no++ ?></td>
													<td><?= $rk['nmdesa'] ?></td>
													<td><?= $rk['total'] ?></td>
													<td><?= $rk['tgl'] ?></td>
												</tr>
											<?php endforeach;?>
										</tbody>
									</table>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
