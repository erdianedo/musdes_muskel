<style type="text/css">
	.navigation .menu li a.input{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.art a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    thead tr th,tfoot tr th{
        text-align:center;
        vertical-align:middle !important;
    }
</style>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height:600px">
					<?php if($this->session->flashdata('input_art')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('input_art'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
					<?php elseif($this->session->flashdata('update_art')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('update_art'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
					<?php endif; ?>
					<form method="post" action="<?=base_url()?>rumahtangga/tgl_art">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<p>Bulan</p>
									<select class="form-control select2_1"  name="bulan_" required>
										<option value="">-- Bulan --</option>
										<option value="01">Januari</option>
										<option value="02">Februari</option>
										<option value="03">Maret</option>
										<option value="04">April</option>
										<option value="05">Mei</option>
										<option value="06">Juni</option>
										<option value="07">Juli</option>
										<option value="08">Agustus</option>
										<option value="09">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<p>Tahun</p>
									<?php $thn = date('Y') ?>
										<select class="form-control select2_1"  name="tahun_" required>
									<?php for($x=2020;$x<=$thn;$x++): ?>
											<option value="<?= $x ?>"><?= $x ?></option>
									<?php endfor;?>
										</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12"><br>
								<div class="card">
									<div class="card-body">
										<center>
										<div class="card-body">
											<button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
										</div>
										</center>
									</div>
								</div>
							</div>
						</div>
					</form>
					<h2 style="text-align:center">
                        Input Data Anggota Rumah Tangga <br>Kecamatan <?= $kec['nmkec'] ?><br><?= $kec['nmkab'] ?>
                        <br>
                        <?php
							$thn_bln_ = "";
							$bln_ = explode(" ",$bulan_ini);
							if(count($bln_)>1){
								echo $bulan_ini;
								$thn_bln = date('Y-m');
							}
							else{
								echo $bulan_ini." ".$tahun_ini;
								$thn_bln_ = $thn_bln_;
							}
						?>
                    </h2>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div><br>
							<div class="col-lg-12 col-xs-12">
								<div class="table-responsive">
                                <form action="" method="post">
                                    <input type="text" class="form-control hidden" name="kota" value="<?= $kec['nmkab'] ?>" required>
                                    <input type="text" class="form-control hidden" name="kec" value="<?= $kec['nmkec'] ?>" required>
                                    <input type="text" class="form-control hidden" name="bulan" value="<?= $thn_bln ?>" required>
									<table id="zero_configs" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th style="width:30px !important">No.</th>
												<th style="width:165px !important">Kelurahan</th>
												<th style="width:80px !important">ART Tinggal di Ruta</th>
												<th style="width:80px !important">ART Meninggal</th>
												<th style="width:80px !important">ART Pindah</th>
												<th style="width:80px !important">ART Baru</th>
												<th style="width:80px !important">Kesalahan Prelist</th>
												<th style="width:80px !important">ART Tidak Ditemukan</th>
												<th style="width:80px !important">ART Usulan Baru</th>
											</tr>
										</thead>
                                        <tfoot>
											<tr>
												<th>No.</th>
												<th>Kelurahan</th>
												<th>ART Tinggal di Ruta</th>
												<th>ART Meninggal</th>
												<th>ART Pindah</th>
												<th>ART Baru</th>
												<th>Kesalahan Prelist</th>
												<th>ART Tidak Ditemukan</th>
												<th>ART Usulan Baru</th>
											</tr>
										</tfoot>
                                        <tbody>
                                        	<?php $xx=0; $no=1; foreach($art as $ds): ?>
												<tr>
													<td><?= $no++ ?></td>
													<td><?= $ds['nmdesa'] ?>
                                                    <input type="text" min="0" class="hidden" name="kel[]" value="<?= $ds['nmdesa'] ?>" required>
                                                    </td>
													<td><input type="number" min="0" class="form-control" name="tinggal_di_ruta[]" value="<?= $ds['tinggal_di_ruta'] ?>" style="width:90px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="meninggal[]" value="<?= $ds['meninggal'] ?>" style="width:90px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="pindah[]" value="<?= $ds['pindah'] ?>" style="width:90px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="baru[]" value="<?= $ds['baru'] ?>" style="width:90px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="kesalahan_prelist[]" value="<?= $ds['kesalahan_prelist'] ?>" style="width:90px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="tidak_ditemukan[]" value="<?= $ds['tidak_ditemukan'] ?>" style="width:90px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="usulan_baru[]" value="<?= $ds['usulan_baru'] ?>" style="width:90px !important;height:35px !important" required></td>
												</tr>
											<?php $xx++; endforeach;?>
										</tbody>
									</table>
                                    <div class="col-md-12"><br>
                                        <div class="card">
                                            <div class="card-body">
                                                <center>
                                                <div class="card-body">
                                                    <input type="text" class="hidden" name="status" value="<?= $art[0]['status'] ?>" required>
                                                    <?php if($art[0]['status']=="update"): ?>
                                                        <input type="text" class="hidden" name="id" value="<?= $art[0]['id'] ?>" required>
                                                        <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Update</button>
                                                    <?php else:?>
                                                        <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                                    <?php endif;?>
                                                </div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </form>
								</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
