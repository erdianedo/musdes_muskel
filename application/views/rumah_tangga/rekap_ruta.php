<style type="text/css">
	.navigation .menu li a.rekap{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.rekap_ruta a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
                    <?php if($this->session->flashdata('flash_gambar_gagal')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <div class="alert alert-danger" role="alert">
                                <?= $this->session->flashdata('flash_gambar_gagal'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Bulan</p>
                                    <select class="form-control select2_1"  name="bulan" required>
                                        <option value="">-- Bulan --</option>
                                        <option value="semua">Seluruh Bulan</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Tahun</p>
                                    <?php $thn = date('Y') ?>
                                        <select class="form-control select2_1"  name="tahun" required>
                                    <?php for($x=2020;$x<=$thn;$x++): ?>
                                            <option value="<?= $x ?>"><?= $x ?></option>
                                    <?php endfor;?>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br>
                                <div class="card">
                                    <div class="card-body">
                                        <center>
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                        </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <h3><center>Rekap Data Rumah Tangga<br>Kecamatan <?= strtoupper( $sesion_data['nama']) ?><br>
                        Bulan <?= $bulan_2 ?> Tahun <?= $tahun_ ?>
                        </center></h3>
                        <a href="<?=base_url()?>excel/download_ruta_kec/<?= $kota ?>/<?= $kec_2 ?>/<?= $bulan ?>/<?= $tahun_ ?>">
                        <button class='btn btn-success' style='font-size: 15px;'>
                                Download Laporan 
                        </button>
                        </a><br><br>
                        <?php $perbaiki=0; $keluar=0; $baru=0; ?>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width:30px !important">No.</th>
                                        <th style="width:165px !important">Kelurahan</th>
                                        <th style="width:165px !important">Data Ruta Diperbaiki</th>
                                        <th style="width:165px !important">Data Ruta Dikeluarkan</th>
                                        <th style="width:165px !important">Usulan Ruta Baru</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kelurahan</th>
                                        <th>Data Ruta Diperbaiki</th>
                                        <th>Data Ruta Dikeluarkan</th>
                                        <th>Usulan Ruta Baru</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php $no=1; foreach($lap as $ds): ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td>
                                                <?= $ds['nmdesa'] ?>
                                            </td>
                                            <td>
                                                <?php if($ds['perbaiki']!=0): ?>
                                                <?= $ds['perbaiki'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['keluar']!=0):?>
                                                <?= $ds['keluar'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['baru']!=0):?>
                                                <?= $ds['baru'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                        <?php $perbaiki+=$ds['perbaiki']; $keluar+=$ds['keluar']; $baru+=$ds['baru']; ?>
                                    <?php endforeach;?>
                                </tbody>
                            </table><br>
                            <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="background-color:#a6a1a1"></th>
                                            <th><center>Data Ruta Diperbaiki</center></th>
                                            <th><center>Data Ruta Dikeluarkan</center></th>
                                            <th><center>Usulan Ruta Baru</center></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <tr>
                                            <th><center>Total</center></th>
                                            <th><center><?= $perbaiki ?></center></th>
                                            <th><center><?= $keluar ?></center></th>
                                            <th><center><?= $baru ?></center></th>
                                        </tr>
                                    <tbody>
                                </table>
                            </div>
                        </div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
	<!-- Datepicker -->
	<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>
<script>
jQuery('.mydatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>