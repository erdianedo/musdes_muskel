<style type="text/css">
	.navigation .menu li a.rekap{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.rekap_art a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
                    <?php if($this->session->flashdata('flash_gambar_gagal')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <div class="alert alert-danger" role="alert">
                                <?= $this->session->flashdata('flash_gambar_gagal'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Bulan</p>
                                    <select class="form-control select2_1"  name="bulan" required>
                                        <option value="">-- Bulan --</option>
                                        <option value="semua">Seluruh Bulan</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Tahun</p>
                                    <?php $thn = date('Y') ?>
                                        <select class="form-control select2_1"  name="tahun" required>
                                    <?php for($x=2020;$x<=$thn;$x++): ?>
                                            <option value="<?= $x ?>"><?= $x ?></option>
                                    <?php endfor;?>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br>
                                <div class="card">
                                    <div class="card-body">
                                        <center>
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                        </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <h3><center>Reakap Data Anggota Rumah Tangga<br>Kecamatan <?= strtoupper( $sesion_data['nama']) ?><br>
                        Bulan <?= $bulan_2 ?> Tahun <?= $tahun_ ?>
                        </center></h3>
                        <a href="<?=base_url()?>excel/download_art_kec/<?= $kota ?>/<?= $kec_2 ?>/<?= $bulan ?>/<?= $tahun_ ?>">
                        <button class='btn btn-success' style='font-size: 15px;'>
                                Download Laporan 
                        </button>
                        </a><br><br>
                        <?php 
                            $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0;
                            $baru = 0; $kesalahan_prelist = 0; $tidak_ditemukan = 0; 
                            $usulan_baru = 0;
                        ?>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width:30px !important">No.</th>
                                        <th style="width:165px !important">Kelurahan</th>
                                        <th style="width:80px !important">ART Tinggal di Ruta</th>
                                        <th style="width:80px !important">ART Meninggal</th>
                                        <th style="width:80px !important">ART Pindah</th>
                                        <th style="width:80px !important">ART Baru</th>
                                        <th style="width:80px !important">Kesalahan Prelist</th>
                                        <th style="width:80px !important">ART Tidak Ditemukan</th>
                                        <th style="width:80px !important">ART Usulan Baru</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kelurahan</th>
                                        <th>ART Tinggal di Ruta</th>
                                        <th>ART Meninggal</th>
                                        <th>ART Pindah</th>
                                        <th>ART Baru</th>
                                        <th>Kesalahan Prelist</th>
                                        <th>ART Tidak Ditemukan</th>
                                        <th>ART Usulan Baru</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php $no=1; foreach($lap as $ds): ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $ds['nmdesa'] ?></td>
                                            <td>
                                                <?php if($ds['tinggal_di_ruta']!=0):?>
                                                <?= $ds['tinggal_di_ruta'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['meninggal']!=0):?> 
                                                <?= $ds['meninggal'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['pindah']!=0):?>
                                                <?= $ds['pindah'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['baru']!=0):?>
                                                <?= $ds['baru'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['kesalahan_prelist']!=0):?>    
                                                <?= $ds['kesalahan_prelist'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['tidak_ditemukan']!=0):?>
                                                <?= $ds['tidak_ditemukan'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                            <td>
                                                <?php if($ds['usulan_baru']!=0):?>
                                                <?= $ds['usulan_baru'] ?>
                                                <?php else:?>
                                                -
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                        <?php 
                                            $tinggal_di_ruta = $tinggal_di_ruta + $ds['tinggal_di_ruta']; 
                                            $meninggal = $meninggal + $ds['meninggal']; 
                                            $pindah = $pindah + $ds['pindah'];
                                            $baru = $baru + $ds['baru']; 
                                            $kesalahan_prelist = $kesalahan_prelist + $ds['kesalahan_prelist']; 
                                            $tidak_ditemukan = $tidak_ditemukan + $ds['tidak_ditemukan']; 
                                            $usulan_baru = $usulan_baru + $ds['usulan_baru'];
                                        ?>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                            <br>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="background-color:#a6a1a1"></th>
                                        <th>ART Tinggal di Ruta</th>
                                        <th>ART Meninggal</th>
                                        <th>ART Pindah</th>
                                        <th>ART Baru</th>
                                        <th>Kesalahan Prelist</th>
                                        <th>ART Tidak Ditemukan</th>
                                        <th>ART Usulan Baru</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr>
                                        <th><center>Total</center></th>
                                        <th><center><?= $tinggal_di_ruta ?></center></th>
                                        <th><center><?= $meninggal ?></center></th>
                                        <th><center><?= $pindah ?></center></th>
                                        <th><center><?= $baru ?></center></th>
                                        <th><center><?= $kesalahan_prelist ?></center></th>
                                        <th><center><?= $tidak_ditemukan ?></center></th>
                                        <th><center><?= $usulan_baru ?></center></th>
                                    </tr>
                                <tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
	<!-- Datepicker -->
	<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>
<script>
jQuery('.mydatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>