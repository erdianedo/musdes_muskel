<style type="text/css">
	.navigation .menu li a.data{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.data_ruta a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
                    <?php if($this->session->flashdata('update_ruta')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('update_ruta'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Bulan</p>
                                    <select class="form-control select2_1"  name="bulan" required>
                                        <option value="">-- Bulan --</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Tahun</p>
                                    <?php $thn = date('Y') ?>
                                        <select class="form-control select2_1"  name="tahun" required>
                                    <?php for($x=2020;$x<=$thn;$x++): ?>
                                            <option value="<?= $x ?>"><?= $x ?></option>
                                    <?php endfor;?>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br>
                                <div class="card">
                                    <div class="card-body">
                                        <center>
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                        </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <h3><center>Data Rumah Tangga<br>Kecamatan <?= strtoupper( $sesion_data['nama']) ?><br>
                        Bulan <?= $bulan_2 ?> Tahun <?= $tahun_ ?>
                        </center></h3>
                        <div class="table-responsive">
                        <?php if($lap[0]['status']=="baru"):?>
                        <span style="font-size:14px;color:#d93a3a">*Belum Ada Data Rumah Tangga</span>
                        <?php endif;?>
                        <form action="<?= base_url() ?>/rumahtangga/update_data_ruta" method="post">
                            <input type="text" class="form-control hidden" name="kec" value="<?= $kec['nmkec'] ?>" required>
                            <input type="text" class="form-control hidden" name="bulan" value="<?= date('Y-m') ?>" required>
                            <table id="zero_configs" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width:30px !important">No.</th>
                                        <th style="width:165px !important">Kelurahan</th>
                                        <th style="width:165px !important">Data Ruta Diperbaiki</th>
                                        <th style="width:165px !important">Data Ruta Dikeluarkan</th>
                                        <th style="width:165px !important">Usulan Ruta Baru</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kelurahan</th>
                                        <th>Data Ruta Diperbaiki</th>
                                        <th>Data Ruta Dikeluarkan</th>
                                        <th>Usulan Ruta Baru</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php if($lap[0]['status']=="update"):?>
                                    <?php $no=1; foreach($lap as $ds): ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $ds['nmdesa'] ?>
                                            <input type="text" min="0" class="hidden" name="kel[]" value="<?= $ds['nmdesa'] ?>" required>
                                            </td>
                                            <td><input type="number" min="0" class="form-control" name="perbaiki[]" value="<?= $ds['perbaiki'] ?>" style="width:150px !important;height:35px !important" required></td>
                                            <td><input type="number" min="0" class="form-control" name="keluar[]" value="<?= $ds['keluar'] ?>" style="width:150px !important;height:35px !important" required></td>
                                            <td><input type="number" min="0" class="form-control" name="baru[]" value="<?= $ds['baru'] ?>" style="width:150px !important;height:35px !important" required></td>
                                        </tr>
                                    <?php endforeach;?>
                                    <?php else:?>
                                        <?php $no=1; foreach($lap as $ds): ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $ds['nmdesa'] ?>
                                            <input type="text" min="0" class="hidden" name="kel[]" value="<?= $ds['nmdesa'] ?>" required>
                                            </td>
                                            <td><input type="text" class="form-control" name="perbaiki[]" value="<?= $ds['perbaiki'] ?>" style="width:150px !important;height:35px !important" readonly></td>
                                            <td><input type="text" class="form-control" name="keluar[]" value="<?= $ds['keluar'] ?>" style="width:150px !important;height:35px !important" readonly></td>
                                            <td><input type="text" class="form-control" name="baru[]" value="<?= $ds['baru'] ?>" style="width:150px !important;height:35px !important" readonly></td>
                                        </tr>
                                    <?php endforeach;?>
                                    <?php endif;?>
                                </tbody>
                            </table>
                            <div class="col-md-12"><br>
                                <div class="card">
                                    <div class="card-body">
                                        <?php if($lap!=null): ?>
                                            <input type="text" class="hidden" name="status" value="<?= $lap[0]['status'] ?>" required>
                                            <?php if($lap[0]['status']=="update"): ?>
                                            <input type="checkbox" name="yakin" value="yakin" required>&nbsp;&nbsp;Saya Yakin Data Yang Saya Isikan Sudah benar
                                                <center>
                                                <input type="text" class="hidden" name="id" value="<?= $lap[0]['id'] ?>" required>
                                                <button type="submit" class="btn btn-primary" name="update" style="margin-bottom: 3%">Update</button>
                                                </center>
                                            <?php endif;?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
	<!-- Datepicker -->
	<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>
<script>
jQuery('.mydatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>