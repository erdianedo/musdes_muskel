<style type="text/css">
	.navigation .menu li a.rekap{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.rekap_art a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
    table thead tr th,table tfoot tr th,table tbody tr td{
        text-align:center;
        vertical-align:middle;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
					<br>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="">
                            <div class="row">
                            <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Kab / Kota</p>
                                        <input type="text" value="<?= strtoupper($sesion_data['nama']) ?>" class="form-control" name="kota" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Kecamatan</p>
                                        <select class="form-control select2_1" id="kecamatan"  name="kec" required>
                                                <option value="semua" selected>-- Seluruh Kecamatan --</option>
                                                <?php foreach($kec_ as $kc): ?>
                                                    <?php $kode = explode(".",$kc['kode']) ?>
                                                    <option value="<?= $kode[0].'.'.$kode[1].'.'.$kode[2] ?>_<?= $kc['nmkec'] ?>"><?= $kc['nmkec'] ?></option>
                                                <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Kelurahan</p>
                                        <select class="form-control select2_1" id="kelurahan"  name="kel" required>
                                            <option value="semua" selected>-- Seluruh Kelurahan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Bulan</p>
                                        <select class="form-control select2_1"  name="bulan" required>
                                            <option value="">-- Bulan --</option>
                                            <option value="semua">Seluruh Bulan</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Tahun</p>
                                        <?php $thn = date('Y') ?>
                                            <select class="form-control select2_1"  name="tahun" required>
                                        <?php for($x=2020;$x<=$thn;$x++): ?>
                                                <option value="<?= $x ?>"><?= $x ?></option>
                                        <?php endfor;?>
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <center>
                                            <div class="card-body">
                                                <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Rekap</button>
                                            </div>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                            switch($bulan){
                                case '01':
                                    $bulan_r = "Januari";
                                break;
                                case '02':
                                    $bulan_r = "Februari";
                                break;
                                case '03':
                                    $bulan_r = "Maret";
                                break;
                                case '04':
                                    $bulan_r = "April";
                                break;
                                case '05':
                                    $bulan_r = "Mei";
                                break;
                                case '06':
                                    $bulan_r = "Juni";
                                break;
                                case '07':
                                    $bulan_r = "Juli";
                                break;
                                case '08':
                                    $bulan_r = "Agustus";
                                break;
                                case '09':
                                    $bulan_r = "September";
                                break;
                                case '10':
                                    $bulan_r = "Oktober";
                                break;
                                case '11':
                                    $bulan_r = "November";
                                break;
                                case '12':
                                    $bulan_r = "Desember";
                                break;
                                default:
                                    $bulan_r = "Tidak di ketahui";     
                                break;
                            }
                        ?>
                        <h3><center>
                            <?php 
                                    if($kec=="semua"){//kota pilih, semua kecamatan, semua kelurahan
                                        echo "Rekap Rumah Tangga<br>".$kota;
                                    }
                                    else{//kota pilih, semua kecamatan, semua kelurahan
                                        if($kel=="semua"){
                                            echo "Rekap Rumah Tangga<br>".$kota."<br>Kecamatan ".$kec;
                                        }
                                        else{
                                            echo "Rekap Rumah Tangga<br>".$kota."<br>Kecamatan ".$kec."<br>Desa / Kelurahan ".$kel;
                                        }
                                    }
                            ?>
                            <br>
                            <?php if($bulan!="semua"): ?>
                            Bulan <?= $bulan_r ?> Tahun <?= $tahun ?>
                            <?php else:?>
                            Bulan Januari - Desember Tahun <?= $tahun ?>
                            <?php endif;?>
                        </center></h3>
                        </center></h3>
                        <a href="<?=base_url()?>excel/download_art/<?= $sesion_data['nama'] ?>/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                        <button class='btn btn-success' style='font-size: 15px;'>
                                Download Laporan 
                        </button>
                        </a><br><br>
                        <div class="row">
                            <div class="table-responsive">
                                <?php 
                                    $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0;
                                    $baru = 0; $kesalahan_prelist = 0; $tidak_ditemukan = 0; 
                                    $usulan_baru = 0;
                                ?>
                                <table id="zero_config" class="table table-striped table-bordered">
                                <?php if($kec=="semua" && $kel=="semua"): ?>
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kelurahan</th>
                                            <th>ART Tinggal di Ruta</th>
                                            <th>ART Meninggal</th>
                                            <th>ART Pindah</th>
                                            <th>ART Baru</th>
                                            <th>Kesalahan Prelist</th>
                                            <th>ART Tidak Ditemukan</th>
                                            <th>ART Usulan Baru</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($lap as $rk): ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td>
                                                    <?= $rk['kec']; ?>
                                                </td>
                                                <td>
                                                <?php if($rk['tinggal_di_ruta']!="0"):?>
                                                    <?= $rk['tinggal_di_ruta']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['meninggal']!="0"):?>
                                                    <?= $rk['meninggal']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['pindah']!="0"):?>
                                                    <?= $rk['pindah']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['baru']!="0"):?>
                                                    <?= $rk['baru']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['kesalahan_prelist']!="0"):?>
                                                    <?= $rk['kesalahan_prelist']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['tidak_ditemukan']!="0"):?>
                                                    <?= $rk['tidak_ditemukan']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['usulan_baru']!="0"):?>
                                                    <?= $rk['usulan_baru']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                            <?php 
                                                $tinggal_di_ruta+= $rk['tinggal_di_ruta']; 
                                                $meninggal+= $rk['meninggal']; 
                                                $pindah+= $rk['pindah'];
                                                $baru+= $rk['baru']; 
                                                $kesalahan_prelist+= $rk['kesalahan_prelist']; 
                                                $tidak_ditemukan+= $rk['tidak_ditemukan']; 
                                                $usulan_baru+= $rk['usulan_baru'];
                                            ?>
                                        <?php endforeach;?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kelurahan</th>
                                            <th>ART Tinggal di Ruta</th>
                                            <th>ART Meninggal</th>
                                            <th>ART Pindah</th>
                                            <th>ART Baru</th>
                                            <th>Kesalahan Prelist</th>
                                            <th>ART Tidak Ditemukan</th>
                                            <th>ART Usulan Baru</th>
                                        </tr>
                                    </tfoot>
                                <?php elseif($kec!="semua" && $kel=="semua"): ?>
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kecamatan / Kelurahan</th>
                                            <th>ART Tinggal di Ruta</th>
                                            <th>ART Meninggal</th>
                                            <th>ART Pindah</th>
                                            <th>ART Baru</th>
                                            <th>Kesalahan Prelist</th>
                                            <th>ART Tidak Ditemukan</th>
                                            <th>ART Usulan Baru</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($lap as $rk): ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td>
                                                    <?= $rk['kec']; ?> / <b><?= $rk['kel']; ?></b>
                                                </td>
                                                <td>
                                                <?php if($rk['tinggal_di_ruta']!="0"):?>
                                                    <?= $rk['tinggal_di_ruta']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['meninggal']!="0"):?>
                                                    <?= $rk['meninggal']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['pindah']!="0"):?>
                                                    <?= $rk['pindah']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['baru']!="0"):?>
                                                    <?= $rk['baru']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['kesalahan_prelist']!="0"):?>
                                                    <?= $rk['kesalahan_prelist']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['tidak_ditemukan']!="0"):?>
                                                    <?= $rk['tidak_ditemukan']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['usulan_baru']!="0"):?>
                                                    <?= $rk['usulan_baru']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                            <?php 
                                                $tinggal_di_ruta+= $rk['tinggal_di_ruta']; 
                                                $meninggal+= $rk['meninggal']; 
                                                $pindah+= $rk['pindah'];
                                                $baru+= $rk['baru']; 
                                                $kesalahan_prelist+= $rk['kesalahan_prelist']; 
                                                $tidak_ditemukan+= $rk['tidak_ditemukan']; 
                                                $usulan_baru+= $rk['usulan_baru'];
                                            ?>
                                        <?php endforeach;?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kecamatan / Kelurahan</th>
                                            <th>ART Tinggal di Ruta</th>
                                            <th>ART Meninggal</th>
                                            <th>ART Pindah</th>
                                            <th>ART Baru</th>
                                            <th>Kesalahan Prelist</th>
                                            <th>ART Tidak Ditemukan</th>
                                            <th>ART Usulan Baru</th>
                                        </tr>
                                    </tfoot>
                                <?php elseif($kec!="semua" && $kel!="semua"): ?>
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kecamatan / Kelurahan</th>
                                            <th>ART Tinggal di Ruta</th>
                                            <th>ART Meninggal</th>
                                            <th>ART Pindah</th>
                                            <th>ART Baru</th>
                                            <th>Kesalahan Prelist</th>
                                            <th>ART Tidak Ditemukan</th>
                                            <th>ART Usulan Baru</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($lap as $rk): ?>
                                        <?php if($rk['kel'] == $kel):?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td>
                                                    <?= $rk['kec']; ?> / <b><?= $rk['kel']; ?></b>
                                                </td>
                                                <td>
                                                <?php if($rk['tinggal_di_ruta']!="0"):?>
                                                    <?= $rk['tinggal_di_ruta']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['meninggal']!="0"):?>
                                                    <?= $rk['meninggal']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['pindah']!="0"):?>
                                                    <?= $rk['pindah']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['baru']!="0"):?>
                                                    <?= $rk['baru']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['kesalahan_prelist']!="0"):?>
                                                    <?= $rk['kesalahan_prelist']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['tidak_ditemukan']!="0"):?>
                                                    <?= $rk['tidak_ditemukan']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                <?php if($rk['usulan_baru']!="0"):?>
                                                    <?= $rk['usulan_baru']; ?>
                                                    <?php else:?>
                                                    -
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                            <?php 
                                                $tinggal_di_ruta+= $rk['tinggal_di_ruta']; 
                                                $meninggal+= $rk['meninggal']; 
                                                $pindah+= $rk['pindah'];
                                                $baru+= $rk['baru']; 
                                                $kesalahan_prelist+= $rk['kesalahan_prelist']; 
                                                $tidak_ditemukan+= $rk['tidak_ditemukan']; 
                                                $usulan_baru+= $rk['usulan_baru'];
                                            ?>
                                        <?php endif;?>
                                        <?php endforeach;?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kecamatan / Kelurahan</th>
                                            <th>ART Tinggal di Ruta</th>
                                            <th>ART Meninggal</th>
                                            <th>ART Pindah</th>
                                            <th>ART Baru</th>
                                            <th>Kesalahan Prelist</th>
                                            <th>ART Tidak Ditemukan</th>
                                            <th>ART Usulan Baru</th>
                                        </tr>
                                    </tfoot>
                                <?php endif;?>
                                </table>
                            </div>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="background-color:#a6a1a1"></th>
                                        <th>ART Tinggal di Ruta</th>
                                        <th>ART Meninggal</th>
                                        <th>ART Pindah</th>
                                        <th>ART Baru</th>
                                        <th>Kesalahan Prelist</th>
                                        <th>ART Tidak Ditemukan</th>
                                        <th>ART Usulan Baru</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr>
                                        <th><center>Total</center></th>
                                        <th><center><?= $tinggal_di_ruta ?></center></th>
                                        <th><center><?= $meninggal ?></center></th>
                                        <th><center><?= $pindah ?></center></th>
                                        <th><center><?= $baru ?></center></th>
                                        <th><center><?= $kesalahan_prelist ?></center></th>
                                        <th><center><?= $tidak_ditemukan ?></center></th>
                                        <th><center><?= $usulan_baru ?></center></th>
                                    </tr>
                                <tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
<!-- Datepicker -->
<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>

<script>
    $(document).ready(function(){
        $('#kota').change(function(){
            var id_kota = $('#kota').val();
            if(id_kota != ''){
                $.ajax({
                    url: "<?= base_url(); ?>musdes/show_kecamatan/"+id_kota,
                    method: "POST",
                    data: {id_kota,id_kota},
                    success:function(data){
                        $('#kecamatan').html(data);
                    } 
                })
                $.ajax({
                    url: "<?= base_url(); ?>musdes/show_kelurahan/0",
                    method: "POST",
                    data: {id_kota,id_kota},
                    success:function(data){
                        $('#kelurahan').html(data);
                    } 
                })
            }
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('#kecamatan').change(function(){
            var id_kec = $('#kecamatan').val();
            if(id_kec != ''){
                $.ajax({
                    url: "<?= base_url(); ?>musdes/show_kelurahan_kota/"+id_kec,
                    method: "POST",
                    data: {id_kec,id_kec},
                    success:function(data){
                        $('#kelurahan').html(data);
                    } 
                })
            }
        });
    });
</script>