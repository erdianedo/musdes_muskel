<style type="text/css">
	.navigation .menu li a.input{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.ruta a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
</style>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height:600px">
					<?php if($this->session->flashdata('input_ruta')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('input_ruta'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
					<?php elseif($this->session->flashdata('update_ruta')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('update_ruta'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
					<?php endif; ?>
					<form method="post" action="<?=base_url()?>rumahtangga/tgl_ruta">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<p>Bulan</p>
									<select class="form-control select2_1"  name="bulan_" required>
										<option value="">-- Bulan --</option>
										<option value="01">Januari</option>
										<option value="02">Februari</option>
										<option value="03">Maret</option>
										<option value="04">April</option>
										<option value="05">Mei</option>
										<option value="06">Juni</option>
										<option value="07">Juli</option>
										<option value="08">Agustus</option>
										<option value="09">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<p>Tahun</p>
									<?php $thn = date('Y') ?>
										<select class="form-control select2_1"  name="tahun_" required>
									<?php for($x=2020;$x<=$thn;$x++): ?>
											<option value="<?= $x ?>"><?= $x ?></option>
									<?php endfor;?>
										</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12"><br>
								<div class="card">
									<div class="card-body">
										<center>
										<div class="card-body">
											<button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
										</div>
										</center>
									</div>
								</div>
							</div>
						</div>
					</form>
					<h2 style="text-align:center">
                        Input Data Rumah Tangga <br>Kecamatan <?= $kec['nmkec'] ?><br><?= $kec['nmkab'] ?>
                        <br>
						<?php
							$thn_bln_ = "";
							$bln_ = explode(" ",$bulan_ini);
							if(count($bln_)>1){
								echo $bulan_ini;
								$thn_bln = date('Y-m');
							}
							else{
								echo $bulan_ini." ".$tahun_ini;
								$thn_bln_ = $thn_bln_;
							}
						?>
                    </h2>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div><br>
							<div class="col-lg-12 col-xs-12">
								<div class="table-responsive">
								
                                <form action="<?= base_url() ?>/rumahtangga/ruta" method="post">
                                    <input type="text" class="form-control hidden" name="kec" value="<?= $kec['nmkec'] ?>" required>
                                    <input type="text" class="form-control" name="bulan" value="<?= $thn_bln ?>" required>
									<table id="zero_configs" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th style="width:30px !important">No.</th>
												<th style="width:165px !important">Kelurahan</th>
												<th style="width:165px !important">Data Ruta Diperbaiki</th>
												<th style="width:165px !important">Data Ruta Dikeluarkan</th>
												<th style="width:165px !important">Usulan Ruta Baru</th>
											</tr>
										</thead>
                                        <tfoot>
											<tr>
												<th>No.</th>
												<th>Kelurahan</th>
												<th>Data Ruta Diperbaiki</th>
												<th>Data Ruta Dikeluarkan</th>
												<th>Usulan Ruta Baru</th>
											</tr>
										</tfoot>
                                        <tbody>
                                        	<?php $no=1; foreach($ruta as $ds): ?>
												<tr>
													<td><?= $no++ ?></td>
													<td><?= $ds['nmdesa'] ?>
                                                    <input type="text" min="0" class="hidden" name="kel[]" value="<?= $ds['nmdesa'] ?>" required>
                                                    </td>
													<td><input type="number" min="0" class="form-control" name="perbaiki[]" value="<?= $ds['perbaiki'] ?>" style="width:150px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="keluar[]" value="<?= $ds['keluar'] ?>" style="width:150px !important;height:35px !important" required></td>
													<td><input type="number" min="0" class="form-control" name="baru[]" value="<?= $ds['baru'] ?>" style="width:150px !important;height:35px !important" required></td>
												</tr>
											<?php endforeach;?>
										</tbody>
									</table>
                                    <div class="col-md-12"><br>
                                        <div class="card">
                                            <div class="card-body">
                                                <center>
                                                <div class="card-body">
                                                    <input type="text" class="hidden" name="status" value="<?= $ruta[0]['status'] ?>" required>
                                                    <?php if($ruta[0]['status']=="update"): ?>
                                                        <input type="text" class="hidden" name="id" value="<?= $ruta[0]['id'] ?>" required>
                                                    	<button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Update</button>
                                                    <?php else:?>
                                                        <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                                    <?php endif;?>
                                                </div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </form>
								</div>
							</div>
						
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
