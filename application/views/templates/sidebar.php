<style type="text/css">
	.navigation ul li a:hover{
		color: black !important;
	}
	.navigation ul li a i.fa:hover{
		color: black !important;
	}
	.navigation ul li:hover >> .navigation ul li a i{
		color: black !important;
	}
</style>

<div class="main-menu">
	<header class="header">
		<a href="" class="logo" style="background-color: #63d5db !important">Musdes Muskel - DINSOS</a>
		<button type="button" class="button-close fa fa-times js__menu_close"></button>
		<div class="user">
			<a href="" class="avatar"><img src="<?= base_url(); ?>assets/img/jateng.png" alt=""></a>
			<?php $id = explode(".",$sesion_data['id']); ?>
			<?php if($id[1]=="00" && $id[2]=="00"): ?>
				<h5 class="name">Dinas Sosial</h5>
				<h5 class="position">Prov. Jateng</h5>
			<?php elseif($id[1]!="00" && $id[2]=="00"):?>
				<h5 class="name">
					<?php $k = explode(" ",$sesion_data['nama']) ?>
					<center><?= $k[0]."<br>".$k[1] ?></center>
				</h5>
			<?php else:?>
				<h5 class="name"><?= $kec['nmkec'] ?></h5>
				<h5 class="position"><?= $kec['nmkab'] ?></h5>
			<?php endif;?>
		</div>
	</header>
	<div class="content">
		<div class="navigation">
			<h5 class="title" style="margin-top: 20px">Menu</h5>
			<ul class="menu js__accordion">
				<li class="dashboard">
					<a class="waves-effect" href="<?= base_url() ?>dashboard"><i class="menu-icon fa fa-home"></i><span>Dashboard</span></a>
				</li>
				<li class="account">
					<a class="waves-effect" href="<?= base_url() ?>account/setting"><i class="menu-icon fa fa-gear"></i><span>Pengaturan Akun</span></a>
				</li>
				<?php if($id[1]!="00" && $id[2]=="00"): ?>
					<li>
						<a class="waves-effect" target="blank" href="https://cloud.dinsos.jatengprov.go.id/index.php/s/kLDqJgkRk479oB9"><i class="menu-icon fa fa-download"></i><span>Download Format Laporan</span></a>
					</li>
				<?php endif;?>
				<?php if($id[1]!="00" && $id[2]!="00"): ?>
				<li>
					<a class="input waves-effect parent-item js__control" href="#"><i class="menu-icon fa fa-pencil"></i><span>Tambah Data</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
						<li class="input">
							<a class="waves-effect" href="<?= base_url() ?>musdes/laporan"><span>Musdes / Muskel</span></a>
						</li>
						<li class="ruta">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/ruta"><span>Data Rumah Tangga</span></a>
						</li>
						<li class="art">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/art"><span>Anggota Rumah Tangga</span></a>
						</li>
					</ul>
				</li>
				<li>
					<a class="data waves-effect parent-item js__control" href="#"><i class="menu-icon fa fa-file"></i><span>Lihat / Perbarui Data</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
					<li class="data_input">
						<a class="waves-effect" href="<?= base_url() ?>musdes/laporan_kec"><span>Musdes / Muskel</span></a>
					</li>
					<li class="data_ruta">
						<a class="waves-effect" href="<?= base_url() ?>rumahtangga/laporan_ruta_kec"><span>Rumah Tangga</span></a>
					</li>
					<li class="data_art">
						<a class="waves-effect" href="<?= base_url() ?>rumahtangga/laporan_art_kec"><span>Anggota Rumah Tangga</span></a>
					</li>
					</ul>
				</li>
				<?php endif;?>
				<li>
					<a class="rekap waves-effect parent-item js__control" href="#"><i class="menu-icon fa fa-archive"></i><span>Rekap Data</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
					<?php if($id[1]=="00" && $id[2]=="00"): ?>  <!-- DINSOS -->
						<li class="rekap_musdes">
							<a class="waves-effect" href="<?= base_url() ?>musdes/rekap_prov"><span>Musdes / Muskel</span></a>
						</li>
						<!-- <li class="rekap_ruta">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/rekap_ruta_prov"><span>Rumah Tangga</span></a>
						</li>
						<li class="rekap_art">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/rekap_art_prov"><span>Anggota Rumah Tangga</span></a>
						</li> -->
					<?php elseif($id[1]!="00" && $id[2]=="00"): ?>
						<li class="rekap_musdes">
							<a class="waves-effect" href="<?= base_url() ?>musdes/rekap_kota"><span>Musdes / Muskel</span></a>
						</li>
						<li class="rekap_ruta">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/rekap_ruta_kota"><span>Rumah Tangga</span></a>
						</li>
						<li class="rekap_art">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/rekap_art_kota"><span>Anggota Rumah Tangga</span></a>
						</li>
					<?php else:?>
						<li class="rekap_musdes">
							<a class="waves-effect" href="<?= base_url() ?>musdes/rekap"><span>Musdes / Muskel</span></a>
						</li>
						<li class="rekap_ruta">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/rekap_ruta"><span>Rumah Tangga</span></a>
						</li>
						<li class="rekap_art">
							<a class="waves-effect" href="<?= base_url() ?>rumahtangga/rekap_art"><span>Anggota Rumah Tangga</span></a>
						</li>
					<?php endif;?>
					</ul>
				</li>
				<?php if($id[1]!="00" && $id[2]!="00"): ?>
				<li class="ktp">
					<a class="waves-effect" href="<?= base_url() ?>account/ktp"><i class="menu-icon fa fa-user"></i><span>Upload KTP & Buku Rekening</span></a>
				</li>
				<?php endif;?>
			</ul>
		</div>
	</div>
</div>