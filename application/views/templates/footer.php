
	<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
	<script src="<?= base_url() ?>assets/scripts/modernizr.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/nprogress/nprogress.js"></script>
	<script src="<?= base_url() ?>assets/plugin/sweet-alert/sweetalert.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/waves/waves.min.js"></script>
	<!-- Full Screen Plugin -->
	<script src="<?= base_url() ?>assets/plugin/fullscreen/jquery.fullscreen-min.js"></script>

	<!-- Percent Circle -->
	<script src="<?= base_url() ?>assets/plugin/percircle/js/percircle.js"></script>

	<!-- Google Chart -->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<!-- Chartist Chart -->
	<script src="<?= base_url() ?>assets/plugin/chart/chartist/chartist.min.js"></script>
	<script src="<?= base_url() ?>assets/scripts/chart.chartist.init.min.js"></script>

	<!-- FullCalendar -->
	<script src="<?= base_url() ?>assets/plugin/moment/moment.js"></script>
	<script src="<?= base_url() ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>
	<script src="<?= base_url() ?>assets/scripts/fullcalendar.init.js"></script>

	<!-- Data Tables -->
	<script src="<?= base_url() ?>assets/plugin/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/datatables/extensions/Responsive/js/responsive.bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/scripts/datatables.demo.min.js"></script>

	<!-- Remodal -->
	<script src="<?= base_url() ?>assets/plugin/modal/remodal/remodal.min.js"></script>

	<!-- Datepicker -->
	<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>

	<!-- Select2 -->
	<script src="<?= base_url() ?>assets/plugin/select2/js/select2.min.js"></script>

	<!-- Multi Select -->
	<script src="<?= base_url() ?>assets/plugin/multiselect/multiselect.min.js"></script>


	<!-- paling bawah -->
	<script src="<?= base_url() ?>assets/scripts/main.min.js"></script>
	<script src="<?= base_url() ?>assets/color-switcher/color-switcher.min.js"></script>


	<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>

    <!-- Data Tables -->
	<script src="<?= base_url() ?>assets/plugin/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?= base_url() ?>assets/plugin/datatables/extensions/Responsive/js/responsive.bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/scripts/datatables.demo.min.js"></script>
	<script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#zero_config').DataTable();
    </script>
	
	<!-- Lightview -->
	<script src="<?= base_url() ?>assets/plugin/lightview/js/lightview/lightview.js"></script> 
</body>
</html>