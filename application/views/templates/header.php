<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/img/jateng.png">
	<title>Musdes / Muskel</title>

	<!-- Main Styles -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/styles/style.min.css">

	<!-- mCustomScrollbar -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

	<!-- Waves Effect -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/waves/waves.min.css">

	<!-- Sweet Alert -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/sweet-alert/sweetalert.css">
	
	<!-- Percent Circle -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/percircle/css/percircle.css">

	<!-- Chartist Chart -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/chart/chartist/chartist.min.css">

	<!-- FullCalendar -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/fullcalendar/fullcalendar.print.css" media='print'>

	<!-- Color Picker -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/color-switcher/color-switcher.min.css">

	<!-- Data Tables -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/datatables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/datatables/media/css/jquery.dataTables.min.css">

	<!-- Remodal -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/modal/remodal/remodal.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/modal/remodal/remodal-default-theme.css">

	<!-- Datepicker -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/datepicker/css/bootstrap-datepicker.min.css">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/select2/css/select2.min.css">
	
	<!-- Lightview -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugin/lightview/css/lightview/lightview.css">
	
</head>
<body>
<div class="fixed-navbar" style="background-color: #63d5db !important">
	<div class="pull-left">
		<button type="button" class="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile" style="background-color: #63d5db !important"></button>
	</div>
	<div class="pull-right">
		<a href="" class="ico-item fa fa-power-off" data-toggle="modal" data-target="#boostrapModal-1"style="color: #000" ></a>
	</div>
</div>

	<!-- MODAL START -->
	<div class="modal fade" id="boostrapModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">LOGOUT</h4>
				</div>
				<div class="modal-body">
					<p>Apakah Anda Yakin Untuk Logout?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-sm waves-effect waves-light" data-dismiss="modal">Tidak</button>
					<a href="<?=base_url()?>dashboard/logout"><button type="button" class="btn btn-primary btn-sm waves-effect waves-light">Ya</button></a>
				</div>
			</div>
		</div>
	</div>
	<!-- end modal logout -->
	
	
	