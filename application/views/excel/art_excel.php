<?php
$set = ""; 
$ko = explode(" ",$kota);
if($kec!="semua"){
    if($kel=="semua"){
        $set = $ko[0].$ko[1]."_".$kec."_".$bulan."_".$tahun;
    }
    else{
        $set = $ko[0].$ko[1]."_".$kec."_".$kel."_".$bulan."_".$tahun;
    }
}
else{
    $set = "Kecamatan_Se-".$ko[0].$ko[1]."_".$bulan."_".$tahun;
}
$nama = "art_".$set.".xls";
header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=".$nama);

header("Pragma: no-cache");

header("Expires: 0");

?>
<?php 
    $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0;
    $baru = 0; $kesalahan_prelist = 0; $tidak_ditemukan = 0; 
    $usulan_baru = 0;
?>
<style>
    table .table,table thead tr th,table tbody tr td{
        border-collapse:collapse !important;
        border:1px solid black !important;
    }
</style>
<table class="table">
    <tr>
        <th colspan="9">
        <?php 
                if($kec=="semua"){//kota pilih, semua kecamatan, semua kelurahan
                    echo "<h3>Rekap Anggota Rumah Tangga<br> Kecamatan Se-".$kota;
                }
                else{//kota pilih, semua kecamatan, semua kelurahan
                    if($kel=="semua"){
                        echo "<h3>Rekap Anggota Rumah Tangga<br>".$kota."<br>Kecamatan ".$kec;
                    }
                    else{
                        echo "<h3>Rekap Anggota Rumah Tangga<br>".$kota."<br>Kecamatan ".$kec."<br>Desa / Kelurahan ".$kel;
                    }
                }
        ?><br>
        Bulan <?= $bulan ?> Tahun <?= $tahun ?></h3>
        </th>
    </tr>
</table>
<table id="zero_config" class="table table-striped table-bordered">
<?php if($kec=="semua" && $kel=="semua"): ?>
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kelurahan</th>
            <th style="border:1px solid black">ART Tinggal di Ruta</th>
            <th style="border:1px solid black">ART Meninggal</th>
            <th style="border:1px solid black">ART Pindah</th>
            <th style="border:1px solid black">ART Baru</th>
            <th style="border:1px solid black">Kesalahan Prelist</th>
            <th style="border:1px solid black">ART Tidak Ditemukan</th>
            <th style="border:1px solid black">ART Usulan Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $rk): ?>
            <tr>
                <td style="border:1px solid black"><?= $no++; ?></td>
                <td style="border:1px solid black">
                    <?= $rk['kec']; ?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['tinggal_di_ruta']!="0"):?>
                    <?= $rk['tinggal_di_ruta']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['meninggal']!="0"):?>
                    <?= $rk['meninggal']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['pindah']!="0"):?>
                    <?= $rk['pindah']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['baru']!="0"):?>
                    <?= $rk['baru']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['kesalahan_prelist']!="0"):?>
                    <?= $rk['kesalahan_prelist']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['tidak_ditemukan']!="0"):?>
                    <?= $rk['tidak_ditemukan']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['usulan_baru']!="0"):?>
                    <?= $rk['usulan_baru']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
            </tr>
            <?php 
                $tinggal_di_ruta+= $rk['tinggal_di_ruta']; 
                $meninggal+= $rk['meninggal']; 
                $pindah+= $rk['pindah'];
                $baru+= $rk['baru']; 
                $kesalahan_prelist+= $rk['kesalahan_prelist']; 
                $tidak_ditemukan+= $rk['tidak_ditemukan']; 
                $usulan_baru+= $rk['usulan_baru'];
            ?>
        <?php endforeach;?>
    </tbody>
<?php elseif($kec!="semua" && $kel=="semua"): ?>
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kecamatan / Kelurahan</th>
            <th style="border:1px solid black">ART Tinggal di Ruta</th>
            <th style="border:1px solid black">ART Meninggal</th>
            <th style="border:1px solid black">ART Pindah</th>
            <th style="border:1px solid black">ART Baru</th>
            <th style="border:1px solid black">Kesalahan Prelist</th>
            <th style="border:1px solid black">ART Tidak Ditemukan</th>
            <th style="border:1px solid black">ART Usulan Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $rk): ?>
            <tr>
                <td style="border:1px solid black"><?= $no++; ?></td>
                <td style="border:1px solid black">
                    <?= $rk['kec']; ?> / <b><?= $rk['kel']; ?></b>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['tinggal_di_ruta']!="0"):?>
                    <?= $rk['tinggal_di_ruta']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['meninggal']!="0"):?>
                    <?= $rk['meninggal']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['pindah']!="0"):?>
                    <?= $rk['pindah']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['baru']!="0"):?>
                    <?= $rk['baru']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['kesalahan_prelist']!="0"):?>
                    <?= $rk['kesalahan_prelist']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['tidak_ditemukan']!="0"):?>
                    <?= $rk['tidak_ditemukan']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['usulan_baru']!="0"):?>
                    <?= $rk['usulan_baru']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
            </tr>
            <?php 
                $tinggal_di_ruta+= $rk['tinggal_di_ruta']; 
                $meninggal+= $rk['meninggal']; 
                $pindah+= $rk['pindah'];
                $baru+= $rk['baru']; 
                $kesalahan_prelist+= $rk['kesalahan_prelist']; 
                $tidak_ditemukan+= $rk['tidak_ditemukan']; 
                $usulan_baru+= $rk['usulan_baru'];
            ?>
        <?php endforeach;?>
    </tbody>
<?php elseif($kec!="semua" && $kel!="semua"): ?>
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kecamatan / Kelurahan</th>
            <th style="border:1px solid black">ART Tinggal di Ruta</th>
            <th style="border:1px solid black">ART Meninggal</th>
            <th style="border:1px solid black">ART Pindah</th>
            <th style="border:1px solid black">ART Baru</th>
            <th style="border:1px solid black">Kesalahan Prelist</th>
            <th style="border:1px solid black">ART Tidak Ditemukan</th>
            <th style="border:1px solid black">ART Usulan Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $rk): ?>
        <?php if($rk['kel'] == $kel):?>
            <tr>
                <td style="border:1px solid black"><?= $no++; ?></td>
                <td style="border:1px solid black">
                    <?= $rk['kec']; ?> / <b><?= $rk['kel']; ?></b>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['tinggal_di_ruta']!="0"):?>
                    <?= $rk['tinggal_di_ruta']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['meninggal']!="0"):?>
                    <?= $rk['meninggal']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['pindah']!="0"):?>
                    <?= $rk['pindah']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['baru']!="0"):?>
                    <?= $rk['baru']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['kesalahan_prelist']!="0"):?>
                    <?= $rk['kesalahan_prelist']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['tidak_ditemukan']!="0"):?>
                    <?= $rk['tidak_ditemukan']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                <?php if($rk['usulan_baru']!="0"):?>
                    <?= $rk['usulan_baru']; ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
            </tr>
            <?php 
                $tinggal_di_ruta+= $rk['tinggal_di_ruta']; 
                $meninggal+= $rk['meninggal']; 
                $pindah+= $rk['pindah'];
                $baru+= $rk['baru']; 
                $kesalahan_prelist+= $rk['kesalahan_prelist']; 
                $tidak_ditemukan+= $rk['tidak_ditemukan']; 
                $usulan_baru+= $rk['usulan_baru'];
            ?>
        <?php endif;?>
        <?php endforeach;?>
    </tbody>
<?php endif;?>
</table>
<br><br>
<table id="zero_config" class="table table-striped table-bordered">
<thead>
    <tr>
        <th colspan="2" style="background-color:#a6a1a1"></th>
        <th style="border:1px solid black">ART Tinggal di Ruta</th>
        <th style="border:1px solid black">ART Meninggal</th>
        <th style="border:1px solid black">ART Pindah</th>
        <th style="border:1px solid black">ART Baru</th>
        <th style="border:1px solid black">Kesalahan Prelist</th>
        <th style="border:1px solid black">ART Tidak Ditemukan</th>
        <th style="border:1px solid black">ART Usulan Baru</th>
    </tr>
</thead>
<tbody> 
    <tr>
        <th colspan="2" style="border:1px solid black"><center>Total</center></th>
        <th style="border:1px solid black"><center><?= $tinggal_di_ruta ?></center></th>
        <th style="border:1px solid black"><center><?= $meninggal ?></center></th>
        <th style="border:1px solid black"><center><?= $pindah ?></center></th>
        <th style="border:1px solid black"><center><?= $baru ?></center></th>
        <th style="border:1px solid black"><center><?= $kesalahan_prelist ?></center></th>
        <th style="border:1px solid black"><center><?= $tidak_ditemukan ?></center></th>
        <th style="border:1px solid black"><center><?= $usulan_baru ?></center></th>
    </tr>
<tbody>
</table>