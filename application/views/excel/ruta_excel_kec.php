<?php
$set = ""; 
$ko = explode("%20",$kota);
$set = $ko[0].$ko[1]."_".$kec."_".$bulan."_".$tahun;

$nama = "ruta_".$set.".xls";
header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=".$nama);

header("Pragma: no-cache");

header("Expires: 0");

?>
<style>
    table.table,table.table thead tr th,table.table tbody tr td{
        border:1px solid black !important;
        border-collapse:collapse !important;
    }
</style>
<table class="table">
    <tr>
        <th colspan="5">
        <?php 
            echo "<h3>Rekap Rumah Tangga<br> Kecamatan ".$kec."<br> ".$ko[0]." ".$ko[1];
        ?><br>
        Bulan <?= $bulan ?> Tahun <?= $tahun ?></h3>
        </th>
    </tr>
</table>
<?php $perbaiki=0; $keluar=0; $baru=0; ?>
<table id="zero_config" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kelurahan</th>
            <th style="border:1px solid black">Data Ruta Diperbaiki</th>
            <th style="border:1px solid black">Data Ruta Dikeluarkan</th>
            <th style="border:1px solid black">Usulan Ruta Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $ds): ?>
            <tr>
                <td style="border:1px solid black"><?= $no++ ?></td>
                <td style="border:1px solid black">
                    <?= $ds['nmdesa'] ?>
                </td>
                <td style="border:1px solid black">
                    <?php if($ds['perbaiki']!=0): ?>
                    <?= $ds['perbaiki'] ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                    <?php if($ds['keluar']!=0):?>
                    <?= $ds['keluar'] ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                    <?php if($ds['baru']!=0):?>
                    <?= $ds['baru'] ?>
                    <?php else:?>
                    -
                    <?php endif;?>
                </td>
            </tr>
            <?php $perbaiki+=$ds['perbaiki']; $keluar+=$ds['keluar']; $baru+=$ds['baru']; ?>
        <?php endforeach;?>
    </tbody>
</table>
<br>
<table>
    <thead>
        <tr>
            <th colspan="2" style="background-color:#a6a1a1;border:1px solid black"></th>
            <th style="border:1px solid black"><center>Data Ruta Diperbaiki</center></th>
            <th style="border:1px solid black"><center>Data Ruta Dikeluarkan</center></th>
            <th style="border:1px solid black"><center>Usulan Ruta Baru</center></th>
        </tr>
    </thead>
    <tbody> 
        <tr>
            <th colspan="2" style="border:1px solid black"><center>Total</center></th>
            <th style="border:1px solid black"><center><?= $perbaiki ?></center></th>
            <th style="border:1px solid black"><center><?= $keluar ?></center></th>
            <th style="border:1px solid black"><center><?= $baru ?></center></th>
        </tr>
    <tbody>
</table>