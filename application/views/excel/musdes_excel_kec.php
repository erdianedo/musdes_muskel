<?php
$set = ""; 
$ko = explode("%20",$kota);
$set = $ko[0].$ko[1]."_".$kec."_".$bulan."_".$tahun;

$nama = "musdes_".$set.".xls";
header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=".$nama);

header("Pragma: no-cache");

header("Expires: 0");

?>
<style>
    table.table,table.table thead tr th,table.table tbody tr td{
        border:1px solid black !important;
        border-collapse:collapse !important;
    }
</style>
<table class="table">
    <tr>
        <th colspan="4">
        <?php 
            echo "<h3>Rekap Musdes/Muskel<br> Kecamatan ".$kec."<br> ".$ko[0]." ".$ko[1];
        ?><br>
        Bulan <?= $bulan ?> Tahun <?= $tahun ?></h3>
        </th>
    </tr>
</table>
<table class="table">
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Nama Kelurahan / Desa</th>
            <th style="border:1px solid black">Total Pelaksanaan</th>
            <th style="border:1px solid black">Tanggal Pelaksanaan<br></th>
        </tr>
    </thead>
    <tbody>
    <?php $no=1; ?>
    <?php foreach($rekap as $ds): ?>
            <tr>
                <td style="border:1px solid black"><?= $no++ ?></td>
                <td style="border:1px solid black"><?= $ds['nmdesa'] ?></td>
                <td style="border:1px solid black"><?= $ds['total'] ?></td>
                <td style="border:1px solid black"><?= $ds['tgl'] ?></td>
            </tr>
    <?php endforeach;?>
    </tbody>
</table>