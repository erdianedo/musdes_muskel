<?php
$set = ""; 
$ko = explode("%20",$kota);
$set = $ko[0].$ko[1]."_".$kec."_".$bulan."_".$tahun;

$nama = "art_".$set.".xls";
header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=".$nama);

header("Pragma: no-cache");

header("Expires: 0");

?>
<?php 
    $tinggal_di_ruta = 0; $meninggal = 0; $pindah = 0;
    $baru = 0; $kesalahan_prelist = 0; $tidak_ditemukan = 0; 
    $usulan_baru = 0;
?>

<table class="table">
    <tr>
        <th colspan="9">
        <?php 
            echo "<h3>Rekap Anggota Rumah Tangga<br> Kecamatan ".$kec."<br> ".$ko[0]." ".$ko[1];
        ?><br>
        Bulan <?= $bulan ?> Tahun <?= $tahun ?></h3>
        </th>
    </tr>
</table>
<table id="zero_config" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kelurahan</th>
            <th style="border:1px solid black">ART Tinggal di Ruta</th>
            <th style="border:1px solid black">ART Meninggal</th>
            <th style="border:1px solid black">ART Pindah</th>
            <th style="border:1px solid black">ART Baru</th>
            <th style="border:1px solid black">Kesalahan Prelist</th>
            <th style="border:1px solid black">ART Tidak Ditemukan</th>
            <th style="border:1px solid black">ART Usulan Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $ds): ?>
            <tr>
                <td style="border:1px solid black"><?= $no++ ?></td>
                <td style="border:1px solid black"><?= $ds['nmdesa'] ?></td>
                <td style="border:1px solid black">
                    <?= $ds['tinggal_di_ruta'] ?>
                </td>
                <td style="border:1px solid black">
                    <?= $ds['meninggal'] ?>
                </td>
                <td style="border:1px solid black">
                    <?= $ds['pindah'] ?>
                </td>
                <td style="border:1px solid black">
                    <?= $ds['baru'] ?>
                </td>
                <td style="border:1px solid black">
                    <?= $ds['kesalahan_prelist'] ?>
                </td>
                <td style="border:1px solid black">
                    <?= $ds['tidak_ditemukan'] ?>
                </td>
                <td style="border:1px solid black">
                    <?= $ds['usulan_baru'] ?>
                </td>
            </tr>
            <?php 
                $tinggal_di_ruta = $tinggal_di_ruta + $ds['tinggal_di_ruta']; 
                $meninggal = $meninggal + $ds['meninggal']; 
                $pindah = $pindah + $ds['pindah'];
                $baru = $baru + $ds['baru']; 
                $kesalahan_prelist = $kesalahan_prelist + $ds['kesalahan_prelist']; 
                $tidak_ditemukan = $tidak_ditemukan + $ds['tidak_ditemukan']; 
                $usulan_baru = $usulan_baru + $ds['usulan_baru'];
            ?>
        <?php endforeach;?>
    </tbody>
</table>
<br>
<table id="zero_config" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th colspan="2"  style="border:1px solid black;background-color:#a6a1a1"></th>
            <th style="border:1px solid black">ART Tinggal di Ruta</th>
            <th style="border:1px solid black">ART Meninggal</th>
            <th style="border:1px solid black">ART Pindah</th>
            <th style="border:1px solid black">ART Baru</th>
            <th style="border:1px solid black">Kesalahan Prelist</th>
            <th style="border:1px solid black">ART Tidak Ditemukan</th>
            <th style="border:1px solid black">ART Usulan Baru</th>
        </tr>
    </thead>
    <tbody> 
        <tr>
            <th colspan="2" style="border:1px solid black"><center>Total</center></th>
            <th style="border:1px solid black"><center><?= $tinggal_di_ruta ?></center></th>
            <th style="border:1px solid black"><center><?= $meninggal ?></center></th>
            <th style="border:1px solid black"><center><?= $pindah ?></center></th>
            <th style="border:1px solid black"><center><?= $baru ?></center></th>
            <th style="border:1px solid black"><center><?= $kesalahan_prelist ?></center></th>
            <th style="border:1px solid black"><center><?= $tidak_ditemukan ?></center></th>
            <th style="border:1px solid black"><center><?= $usulan_baru ?></center></th>
        </tr>
    <tbody>
</table>