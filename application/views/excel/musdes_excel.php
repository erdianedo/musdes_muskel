<?php
$set = ""; 
$ko = explode(" ",$kota);
if($kec!="semua"){
    if($kel=="semua"){
        $set = $ko[0].$ko[1]."_".$kec."_".$bulan."_".$tahun;
    }
    else{
        $set = $ko[0].$ko[1]."_".$kec."_".$kel."_".$bulan."_".$tahun;
    }
}
else{
    $set = "Kecamatan_Se-".$ko[0].$ko[1]."_".$bulan."_".$tahun;
}
$nama = "musdes_".$set.".xls";
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$nama);
header("Pragma: no-cache");
header("Expires: 0");
?>
<style>
    table, thead tr th, tbody tr td{
        border-collapse:collapse;
        border:1px solid black;
    }
</style>
    <table>
        <thead>
            <tr>
                <th colspan="6">
                <?php 
                    if($kota!="" && $kota!="semua"){//kota pilih
                        if($kec=="semua"){//kota pilih, semua kecamatan, semua kelurahan
                            echo "<h3>Rekap Musdes / Muskel<br>Kecamatan Se-".$kota;
                        }
                        else{//kota pilih, semua kecamatan, semua kelurahan
                            if($kel=="semua"){
                                echo "<h3>Rekap Musdes / Muskel<br>".$kota."<br>Kecamatan ".$kec;
                            }
                            else{
                                echo "<h3>Rekap Musdes / Muskel<br>".$kota."<br>Kecamatan ".$kec."<br>Desa / Kelurahan ".$kel;
                            }
                            
                        }
                    } 
                ?>
                <br>
                Bulan <?= $bulan ?> Tahun <?= $tahun ?></h3>
                Update : <?php date_default_timezone_set('Asia/Jakarta'); echo date('H:i:s') ?>
                </th>
            </tr>
        </thead>
    </table>
    <table>
        <thead>
            <tr>
                <th>No.</th>
                <th>Kab /<br>Kota</th>
                <th>Kecamatan</th>
                <th>Kelurahan</th>
                <th>Total Pelaksanaan</th>
                <th>Tanggal Pelaksanaan<br></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($rekap as $rk): ?>
                <?php $kota = explode(" ",$rk['nmkab']) ?>
                <tr>
                    <td><?= $rk['no'] ?></td>
                    <td><?= $kota[0]."<br>".$kota[1] ?></td>
                    <td><?= $rk['nmkec'] ?></td>
                    <td><?= $rk['nmdesa'] ?></td>
                    <td><?= $rk['jml_musdes'] ?></td>
                    <td><?= $rk['tgl_musdes'] ?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>