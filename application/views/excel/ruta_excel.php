<?php 
$set = ""; 
$ko = explode(" ",$kota);
if($kec!="semua"){
    if($kel=="semua"){
        $set = $ko[0].$ko[1]."_".$kec."_".$bulan."_".$tahun;
    }
    else{
        $set = $ko[0].$ko[1]."_".$kec."_".$kel."_".$bulan."_".$tahun;
    }
}
else{
    $set = "Kecamatan_Se-".$ko[0].$ko[1]."_".$bulan."_".$tahun;
}
$nama = "ruta_".$set.".xls";
header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=".$nama);

header("Pragma: no-cache");

header("Expires: 0");

?>
<style>
    table,table thead tr th,table tbody tr td{
        border-collapse:collapse !important;
        border:1px solid black !important;
    }
</style>
<table class="table">
    <tr>
        <th colspan="5">
        <?php 
                if($kec=="semua"){//kota pilih, semua kecamatan, semua kelurahan
                    echo "<h3>Rekap Rumah Tangga<br> Kecamatan Se-".$kota;
                }
                else{//kota pilih, semua kecamatan, semua kelurahan
                    if($kel=="semua"){
                        echo "<h3>Rekap Rumah Tangga<br>".$kota."<br>Kecamatan ".$kec;
                    }
                    else{
                        echo "<h3>Rekap Rumah Tangga<br>".$kota."<br>Kecamatan ".$kec."<br>Desa / Kelurahan ".$kel;
                    }
                }
        ?><br>
        Bulan <?= $bulan ?> Tahun <?= $tahun ?></h3>
        </th>
    </tr>
</table>
<?php $perbaiki=0; $keluar=0; $baru=0; ?>
<table>
<?php if($kec=="semua" && $kel=="semua"): ?>
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kecamatan</th>
            <th style="border:1px solid black">Data Ruta Diperbaiki</th>
            <th style="border:1px solid black">Data Ruta Dikeluarkan</th>
            <th style="border:1px solid black">Usulan Ruta Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $rk): ?>
            <tr>
                <td style="border:1px solid black"><?= $no++; ?></td>
                <td style="border:1px solid black"><?= $rk['kec'] ?></td>
                <td style="border:1px solid black">
                    <?php if($rk['perbaiki']=="0"): ?>
                    -
                    <?php else:?>
                    <?= $rk['perbaiki'] ?>
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                    <?php if($rk['keluar']=="0"): ?>
                    -
                    <?php else:?>
                        <?= $rk['keluar'] ?>
                    <?php endif;?>
                </td style="border:1px solid black">
                <td style="border:1px solid black">
                    <?php if($rk['baru']=="0"): ?>
                    -
                    <?php else:?>
                        <?= $rk['baru'] ?>
                    <?php endif;?>
                </td>
            </tr>
            <?php $perbaiki+=$rk['perbaiki']; $keluar+=$rk['keluar']; $baru+=$rk['baru']; ?>
        <?php endforeach;?>
    </tbody>
<?php elseif($kec!="semua" && $kel=="semua"):?>
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kecamatan / Kelurahan</th>
            <th style="border:1px solid black">Data Ruta Diperbaiki</th>
            <th style="border:1px solid black">Data Ruta Dikeluarkan</th>
            <th style="border:1px solid black">Usulan Ruta Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $rk): ?>
            <tr>
                <td style="border:1px solid black"><?= $no++; ?></td>
                <td style="border:1px solid black"><?= $rk['kec'] ?> / <b><?= $rk['kel'] ?></b></td>
                <td style="border:1px solid black">
                    <?php if($rk['perbaiki']=="0"): ?>
                    -
                    <?php else:?>
                    <?= $rk['perbaiki'] ?>
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                    <?php if($rk['keluar']=="0"): ?>
                    -
                    <?php else:?>
                        <?= $rk['keluar'] ?>
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                    <?php if($rk['baru']=="0"): ?>
                    -
                    <?php else:?>
                        <?= $rk['baru'] ?>
                    <?php endif;?>
                </td>
            </tr>
            <?php $perbaiki+=$rk['perbaiki']; $keluar+=$rk['keluar']; $baru+=$rk['baru']; ?>
        <?php endforeach;?>
    </tbody>
<?php elseif($kec!="semua" && $kel!="semua"):?>
    <thead>
        <tr>
            <th style="border:1px solid black">No.</th>
            <th style="border:1px solid black">Kecamatan / Kelurahan</th>
            <th style="border:1px solid black">Data Ruta Diperbaiki</th>
            <th style="border:1px solid black">Data Ruta Dikeluarkan</th>
            <th style="border:1px solid black">Usulan Ruta Baru</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach($rekap as $rk): ?>
            <?php if($rk['kel'] == $kel):?>
            <tr>
                <td style="border:1px solid black"><?= $no++; ?></td>
                <td style="border:1px solid black"><?= $rk['kec'] ?> / <b><?= $rk['kel'] ?></b></td>
                <td style="border:1px solid black">
                    <?php if($rk['perbaiki']=="0"): ?>
                    -
                    <?php else:?>
                    <?= $rk['perbaiki'] ?>
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                    <?php if($rk['keluar']=="0"): ?>
                    -
                    <?php else:?>
                        <?= $rk['keluar'] ?>
                    <?php endif;?>
                </td>
                <td style="border:1px solid black">
                    <?php if($rk['baru']=="0"): ?>
                    -
                    <?php else:?>
                        <?= $rk['baru'] ?>
                    <?php endif;?>
                </td>
            </tr>
            <?php $perbaiki+=$rk['perbaiki']; $keluar+=$rk['keluar']; $baru+=$rk['baru']; ?>
            <?php endif;?>
        <?php endforeach;?>
    </tbody>
<?php endif;?>
</table>
<br><br>
<table>
    <thead>
        <tr>
            <th colspan="2" style="background-color:#a6a1a1"></th>
            <th style="border:1px solid black"><center>Data Ruta Diperbaiki</center></th>
            <th style="border:1px solid black"><center>Data Ruta Dikeluarkan</center></th>
            <th style="border:1px solid black"><center>Usulan Ruta Baru</center></th>
        </tr>
    </thead>
    <tbody> 
        <tr>
            <th colspan="2" style="border:1px solid black"><center>Total</center></th>
            <th style="border:1px solid black"><center><?= $perbaiki ?></center></th>
            <th style="border:1px solid black"><center><?= $keluar ?></center></th>
            <th style="border:1px solid black"><center><?= $baru ?></center></th>
        </tr>
    <tbody>
</table>