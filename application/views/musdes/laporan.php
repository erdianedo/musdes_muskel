<style type="text/css">
	.navigation .menu li a.input{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.input a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
                    <?php if($this->session->flashdata('flash_input')) : ?>
                        <div class="row mt-3">
                            <div class="col-md-8">
                                <div class="alert alert-success" role="alert">
                                    <?= $this->session->flashdata('flash_input'); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
					<h3><center>Input Laporan Musdes / Muskel<br>Kecamatan <?= strtoupper( $sesion_data['nama']) ?></center></h3>
                    <br>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p class="margin-top-20">
                                        Pilih Nama Kelurahan / Desa
                                    </p>
                                    <select class="form-control select2_1"  name="desa" required>
                                    <option value="">-- Pilih Kelurahan / Desa --</option>
                                    <?php foreach($desa as $ds): ?>
                                        <option value="<?= $ds['kode'] ?>_<?= $ds['nmdesa']; ?>_<?= $ds['nmkec']; ?>_<?= $ds['nmkab']; ?>">
                                            <?= $ds['nmdesa']; ?>
                                        </option>
                                    <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row"><br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>Tanggal Pelaksanaan</p>
                                    <input type="text" class="form-control mydatepicker" name="tanggal_pelaksanaan" placeholder="mm/dd/yyyy" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"><br>
                            <div class="card">
                                <div class="card-body">
                                    <center>
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                    </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                        </form>
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
	<!-- Datepicker -->
	<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>
<script>
jQuery('.mydatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>