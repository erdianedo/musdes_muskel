<style type="text/css">
	.navigation .menu li a.rekap{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.rekap_musdes a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
    table thead tr th,table tfoot tr th,table tbody tr td{
        text-align:center;
        vertical-align:middle;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
					<br>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="">
                            <div class="row">
                            <div class="col-md-12">
                                    <div class="form-group">
                                        <p>Kab / Kota</p>
                                        <input type="text" value="<?= strtoupper($sesion_data['nama']) ?>" class="form-control" name="kota" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Kecamatan</p>
                                        <select class="form-control select2_1" id="kecamatan"  name="kec" required>
                                                <option value="semua" selected>-- Seluruh Kecamatan --</option>
                                                <?php foreach($kec_ as $kc): ?>
                                                    <?php $kode = explode(".",$kc['kode']) ?>
                                                    <option value="<?= $kode[0]?>.<?= $kode[1] ?>.<?= $kode[2] ?>_<?= $kc['nmkec'] ?>"><?= $kc['nmkec'] ?></option>
                                                <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Kelurahan</p>
                                        <select class="form-control select2_1" id="kelurahan"  name="kel" required>
                                            <option value="semua" selected>-- Seluruh Kelurahan --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Bulan</p>
                                        <select class="form-control select2_1"  name="bulan" required>
                                            <option value="">-- Bulan --</option>
                                            <option value="semua">Seluruh Bulan</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Tahun</p>
                                        <?php $thn = date('Y') ?>
                                            <select class="form-control select2_1"  name="tahun" required>
                                        <?php for($x=2020;$x<=$thn;$x++): ?>
                                                <option value="<?= $x ?>"><?= $x ?></option>
                                        <?php endfor;?>
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <center>
                                            <div class="card-body">
                                                <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Rekap</button>
                                            </div>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                            switch($bulan){
                                case '01':
                                    $bulan_r = "Januari";
                                break;
                                case '02':
                                    $bulan_r = "Februari";
                                break;
                                case '03':
                                    $bulan_r = "Maret";
                                break;
                                case '04':
                                    $bulan_r = "April";
                                break;
                                case '05':
                                    $bulan_r = "Mei";
                                break;
                                case '06':
                                    $bulan_r = "Juni";
                                break;
                                case '07':
                                    $bulan_r = "Juli";
                                break;
                                case '08':
                                    $bulan_r = "Agustus";
                                break;
                                case '09':
                                    $bulan_r = "September";
                                break;
                                case '10':
                                    $bulan_r = "Oktober";
                                break;
                                case '11':
                                    $bulan_r = "November";
                                break;
                                case '12':
                                    $bulan_r = "Desember";
                                break;
                                default:
                                    $bulan_r = "Tidak di ketahui";     
                                break;
                            }
                        ?>
                        <h3><center>
                            <?php 
                                if($kota!="" && $kota!="semua"){//kota pilih
                                    if($kec=="semua"){//kota pilih, semua kecamatan, semua kelurahan
                                        echo "Rekap Musdes / Muskel<br>".$kota;
                                    }
                                    else{//kota pilih, semua kecamatan, semua kelurahan
                                        if($kel=="semua"){
                                            echo "Rekap Musdes / Muskel<br>".$kota."<br>Kecamatan ".$kec;
                                        }
                                        else{
                                            echo "Rekap Musdes / Muskel<br>".$kota."<br>Kecamatan ".$kec."<br>Desa / Kelurahan ".$kel;
                                        }
                                        
                                    }
                                } 
                            ?>
                            <br>
                            <?php if($bulan!="semua"): ?>
                            Bulan <?= $bulan_r ?> Tahun <?= $tahun ?>
                            <?php else:?>
                            Tahun <?= $tahun ?>
                            <?php endif;?>
                        </center></h3>
                        <a href="<?=base_url()?>excel/download_musdes/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                        <button class='btn btn-success' style='font-size: 15px;'>
                                Download Laporan 
                        </button>
                        </a><br><br>
                        <div class="row">
                            <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kab /<br>Kota</th>
                                            <th>Kecamatan /<br>Kelurahan</th>
                                            <th>Total<br>Pelaksanaan</th>
                                            <th>Tanggal<br>Pelaksanaan<br></th>
                                            <th>Dokumentasi</th>
                                            <th>Berita<br>Acara</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($rekap as $rk): ?>
                                            <?php $kota = explode(" ",$rk['nmkab']) ?>
                                            <tr>
                                                <td><?= $rk['no'] ?></td>
                                                <td><?= $kota[0]."<br>".$kota[1] ?></td>
                                                <td><?= $rk['nmkec'] ?>/<br><b><?= $rk['nmdesa'] ?></b></td>
                                                <td><?= $rk['jml_musdes'] ?></td>
                                                <td><?= $rk['tgl_musdes'] ?></td>
                                                <td>
                                                    <?php if($rk['foto1']=="kosong" && $rk['foto2']=="kosong"): ?>
                                                        -
                                                    <?php elseif($rk['foto1']=="kosong" && $rk['foto2']!="kosong"): ?>
                                                        <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto2'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                            <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto2'] ?>" style="width:100px !important;height:100px !important">
                                                        </a>
                                                        <a href="<?=base_url()?>musdes/download_foto/<?=$rk['foto2']?>/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                                                        <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                                Download
                                                        </button>
                                                        </a>    
                                                    <?php elseif($rk['foto1']!="kosong" && $rk['foto2']=="kosong"): ?>
                                                        <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto1'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                            <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto1'] ?>" style="width:100px !important;height:100px !important">
                                                        </a>
                                                        <a href="<?=base_url()?>musdes/download_foto/<?=$rk['foto1']?>/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                                                        <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                                Download
                                                        </button>
                                                        </a>   
                                                    <?php else:?>
                                                        <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto1'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                            <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto1'] ?>" style="width:100px !important;height:100px !important">
                                                        </a>
                                                        <a href="<?=base_url()?>musdes/download_foto/<?=$rk['foto1']?>/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                                                        <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                                Download
                                                        </button>
                                                        </a><br><br>
                                                        <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto2'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                            <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $rk['foto2'] ?>" style="width:100px !important;height:100px !important">
                                                        </a>
                                                        <a href="<?=base_url()?>musdes/download_foto/<?=$rk['foto2']?>/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                                                        <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                                Download
                                                        </button>
                                                        </a>   
                                                    <?php endif;?>
                                                </td>
                                                <td>
                                                    <?php if($rk['berita']=="kosong"): ?>
                                                        -
                                                    <?php else:?>
                                                        <?php $tipe = explode(".",$rk['berita']) ?>
                                                        <?php if($tipe[count($tipe)-1]=="pdf"): ?>
                                                            <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/pdf.png" style="width:100px;height:100px"/>
                                                            <br><br>
                                                            <a target="blank" href="<?=base_url()?>musdes/download_berita/<?=$rk['berita']?>/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                                                            <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                                    Download
                                                            </button>
                                                            </a>
                                                            <br><br>
                                                        <?php else:?>
                                                            <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $rk['berita'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                                <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $rk['berita'] ?>" style="width:100px !important;height:100px !important">
                                                            </a>
                                                            <a target="blank" href="<?=base_url()?>musdes/download_berita/<?=$rk['berita']?>/<?= $kec ?>/<?= $kel ?>/<?= $bulan ?>/<?= $tahun ?>">
                                                            <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                                    Download
                                                            </button>
                                                            </a>
                                                            <br><br>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No.</th>
                                            <th>Kab / Kota</th>
                                            <th>Kecamatan / Kelurahan</th>
                                            <th>Total Pelaksanaan</th>
                                            <th>Tanggal Pelaksanaan<br></th>
                                            <th>Dokumentasi</th>
                                            <th>Berita Acara</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
<!-- Datepicker -->
<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>

<script>
    $(document).ready(function(){
        $('#kota').change(function(){
            var id_kota = $('#kota').val();
            if(id_kota != ''){
                $.ajax({
                    url: "<?= base_url(); ?>musdes/show_kecamatan/"+id_kota,
                    method: "POST",
                    data: {id_kota,id_kota},
                    success:function(data){
                        $('#kecamatan').html(data);
                    } 
                })
                $.ajax({
                    url: "<?= base_url(); ?>musdes/show_kelurahan/0",
                    method: "POST",
                    data: {id_kota,id_kota},
                    success:function(data){
                        $('#kelurahan').html(data);
                    } 
                })
            }
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('#kecamatan').change(function(){
            var id_kec = $('#kecamatan').val();
            if(id_kec != ''){
                $.ajax({
                    url: "<?= base_url(); ?>musdes/show_kelurahan/"+id_kec,
                    method: "POST",
                    data: {id_kec,id_kec},
                    success:function(data){
                        $('#kelurahan').html(data);
                    } 
                })
            }
        });
    });
</script>
<!-- <script type="text/javascript">
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?= base_url() ?>musdes/get_data_musdes/<?= $kota ?>/<?= $kec ?>/<?= $kelurahan ?>/<?= $bulan ?>/<?= $tahun ?>",
                "type": "POST"
            },
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            },],
        });
    });
</script> -->