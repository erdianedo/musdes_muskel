<style type="text/css">
	.navigation .menu li a.rekap{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.rekap_musdes a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
					<br>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Bulan</p>
                                        <select class="form-control select2_1"  name="bulan" required>
                                            <option value="">-- Bulan --</option>
                                            <option value="semua">Seluruh Bulan</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p>Tahun</p>
                                        <?php $thn = date('Y') ?>
                                            <select class="form-control select2_1"  name="tahun" required>
                                        <?php for($x=2020;$x<=$thn;$x++): ?>
                                                <option value="<?= $x ?>"><?= $x ?></option>
                                        <?php endfor;?>
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <center>
                                            <div class="card-body">
                                                <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                            </div>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                            switch($bulan){
                                case '01':
                                    $bulan_r = "Januari";
                                break;
                                case '02':
                                    $bulan_r = "Februari";
                                break;
                                case '03':
                                    $bulan_r = "Maret";
                                break;
                                case '04':
                                    $bulan_r = "April";
                                break;
                                case '05':
                                    $bulan_r = "Mei";
                                break;
                                case '06':
                                    $bulan_r = "Juni";
                                break;
                                case '07':
                                    $bulan_r = "Juli";
                                break;
                                case '08':
                                    $bulan_r = "Agustus";
                                break;
                                case '09':
                                    $bulan_r = "September";
                                break;
                                case '10':
                                    $bulan_r = "Oktober";
                                break;
                                case '11':
                                    $bulan_r = "November";
                                break;
                                case '12':
                                    $bulan_r = "Desember";
                                break;
                                default:
                                    $bulan_r = "Tidak di ketahui";     
                                break;
                            }
                        ?>
                        <h3><center>
                            Rekap Musdes / Muskel<br>Kecamatan <?= strtoupper( $sesion_data['nama']) ?>
                            <br>
                            <?php if($bulan!="semua"): ?>
                            Bulan <?= $bulan_r ?> Tahun <?= $tahun ?>
                            <?php else:?>
                            Tahun <?= $tahun ?>
                            <?php endif;?>
                        </center></h3>
                        <a href="<?=base_url()?>excel/download_musdes_kec/<?= $kota ?>/<?= $kec_ ?>/<?= $bulan ?>/<?= $tahun ?>">
                        <button class='btn btn-success' style='font-size: 15px;'>
                                Download Laporan 
                        </button>
                        </a><br><br>
                        <div class="row">
                            <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Kelurahan / Desa</th>
                                            <th>Total Pelaksanaan</th>
                                            <th>Tanggal Pelaksanaan<br></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no=1; ?>
                                    <?php foreach($rekap as $ds): ?>
                                            <tr>
                                                <td><?= $no++ ?></td>
                                                <td><?= $ds['nmdesa'] ?></td>
                                                <td><?= $ds['total'] ?></td>
                                                <td><?= $ds['tgl'] ?></td>
                                            </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Kelurahan / Desa</th>
                                            <th>Total Pelaksanaan</th>
                                            <th>Tanggal Pelaksanaan<br></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
<!-- Datepicker -->
<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>