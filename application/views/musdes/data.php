<style type="text/css">
	.navigation .menu li a.data{
		background-color: #63d5db !important;
		color: #fff;
	}
    .navigation .menu li.data_input a{
        background-color:#6be9f0 !important
    }
	.widget-stat {
	    text-align: left;
	}
    .head_foot{
        text-align: center;
        vertical-align: middle!important;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/styles/bootstrap-datepicker.min.css">

<?php $no=1; $no2=1; foreach($lap as $ds): ?>
<?php $tgl_ = explode("-",$ds['tanggal']); ?>
<?php $bln = $this->Kec_model->bulan_ini_saja2($tgl_[1]); ?>

<!-- MODAL edit taggal musdes START -->
<div class="modal fade" id="modal-edit-<?=$no++?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Tanggal Musdes</h4>
            </div>
            <form action="<?=base_url()?>musdes/edit_tgl_musdes/<?= $ds['id'] ?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= $bulan_ ?>/<?= $tahun_ ?>" method="post">
            <div class="modal-body">
                <p>Edit Tanggal Musdes Kelurahan / Desa <?= $ds['nmdesa'] ?></p><br>
                <input type="text" name="id" value="<?= $ds['id'] ?>" class="hidden" readonly>
                <input type="text" name="nmdesa" value="<?= $ds['nmdesa'] ?>" class="hidden" readonly>
                <label>Tanggal Lama</label>
                <input type="text" value="<?= $ds['tanggal'] ?>" class="form-control" readonly><br>
                <label>Tanggal Baru</label>
                <input type="text" class="form-control mydatepicker" name="tanggal_baru" placeholder="mm/dd/yyyy" required>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Edit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-hapus-<?=$no2++?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Hapus Data Musdes</h4>
            </div>
            <?php
                if($ds['foto1'] == ""){
                    $foto1 = "kosong";
                }
                else{
                    $foto1 = $ds['foto1'];
                }
                if($ds['foto2'] == ""){
                    $foto2 = "kosong";
                }
                else{
                    $foto2 = $ds['foto2'];
                }
                if($ds['berita_acara'] == ""){
                    $berita = "kosong";
                }
                else{
                    $berita = $ds['berita_acara'];
                }
            ?>
            <form action="<?=base_url()?>musdes/hapus_musdes/<?= $ds['id'] ?>/<?= $foto1 ?>/<?= $foto2 ?>/<?= $berita ?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= $bulan_ ?>/<?= $tahun_?>" method="post">
            <div class="modal-body">
                <p>Apakah Anda Ingin Menghapus Data Musdes Kelurahan <?= $ds['nmdesa'] ?><br>Tanggal <?= $tgl_[2]." ".$bln." ".$tgl_[0] ?> ?</p><br>
                <input type="text" name="id" value="<?= $ds['id'] ?>" class="hidden" readonly>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-sm waves-effect waves-light">Hapus</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit musdes -->
<?php endforeach;?>


<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content" style="min-height: 450px">
                    <?php if($this->session->flashdata('flash_gambar_gagal')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <div class="alert alert-danger" role="alert">
                                <?= $this->session->flashdata('flash_gambar_gagal'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php elseif($this->session->flashdata('flash_gambar')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('flash_gambar'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php elseif($this->session->flashdata('flash_edit')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('flash_edit'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php elseif($this->session->flashdata('hapus_musdes')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-8">
                            <div class="alert alert-success" role="alert">
                                <?= $this->session->flashdata('hapus_musdes'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
					<div class="content widget-stat">
						<div class="process-bar">
							<div class="bar js__width bg-warning" data-width="100%"></div>
						</div>
                        <br>
                        <form method="post" action="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p class="margin-top-20">
                                        Pilih Nama Kelurahan / Desa
                                    </p>
                                    <select class="form-control select2_1"  name="desa" required>
                                    <option value="">-- Pilih Kelurahan / Desa --</option>
                                    <option value="semua_<?= $sesion_data['id']; ?>">Semua Kelurahan / Desa</option>
                                    <?php foreach($desa as $ds): ?>
                                        <option value="<?= $ds['nmdesa']; ?>_<?= $sesion_data['id']; ?>">
                                            <?= $ds['nmdesa']; ?>
                                        </option>
                                    <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Bulan</p>
                                    <select class="form-control select2_1"  name="bulan" required>
                                        <option value="">-- Bulan --</option>
                                        <option value="semua">Seluruh Bulan</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p>Tahun</p>
                                    <?php $thn = date('Y') ?>
                                        <select class="form-control select2_1"  name="tahun" required>
                                    <?php for($x=2020;$x<=$thn;$x++): ?>
                                            <option value="<?= $x ?>"><?= $x ?></option>
                                    <?php endfor;?>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br>
                                <div class="card">
                                    <div class="card-body">
                                        <center>
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary" name="tambah" style="margin-bottom: 3%">Kirim</button>
                                        </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <h3><center>Data Laporan Musdes / Muskel<br>Kecamatan <?= strtoupper( $sesion_data['nama']) ?><br>
                        <?php if($desa_=="semua"): ?>
                            <?php if($bulan_=="semua"): ?>
                                Bulan <?= $bulan_2 ?> Tahun <?= $tahun_ ?>
                            <?php elseif($bulan_!="semua"):?>
                                Bulan <?= $bulan_2 ?> Tahun <?= $tahun_ ?>
                            <?php endif;?>
                        <?php elseif($desa_!="semua" && $desa_!=""):?>
                            <?php if($bulan_=="semua"): ?>
                                Kelurahan / Desa <?= $desa_ ?><br>Bulan <?= $bulan_2 ?> Tahun <?= $tahun_ ?>
                                <br>yang Sudah Melakukan Musdes / Muskel
                            <?php elseif($bulan_!="semua"):?>
                                Kelurahan / Desa <?= $desa_ ?><br>Bulan <?= $bulan_2 ?> Tahun <?= $tahun_ ?>
                            <?php endif;?>
                        <?php endif;?>
                        </center></h3>
                        <span style="font-size:20px;color:#d93a3a">*Hanya Menampilkan Kelurahan/Desa Yang Sudah Melakukan Musdes/Muskel</span><br><br>
                        <span style="font-size:14px;color:#d93a3a">*Ukuran File Max 5Mb</span><br>
                        <span style="font-size:14px;color:#d93a3a">*Untuk File Berta Acara Dapat Berupa Format PDF atau JPG/JPEG</span><br><br><br>
                        <span style="font-size:20px;color:#d93a3a">*HARAP MENG UPLOAD BERITA ACARA & DOKUMENTASI SESUAI TABELNYA</span><br><br>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width:5% !important" class="head_foot">No.</th>
                                        <th style="width:20% !important" class="head_foot">Nama Kelurahan / Desa</th>
                                        <th style="width:15% !important" class="head_foot">Tanggal Pelaksanaan</th>
                                        <th style="width:20% !important" class="head_foot" style="width:20% !important">Berita Acara<br></th>
                                        <th colspan="2" style="width:40% !important" class="head_foot">Dokumentasi<br></th>
                                    </tr>
                                </thead>
                                <tbody>
                        <?php $no=1; ?>
						<?php foreach($lap as $ds): ?>
                            <tr>
                                <td style="width:5% !important"><center><?= $no ?><center></td>
                                <td style="width:20% !important" class="head_foot">
                                    <center><?= $ds['nmdesa'] ?></center>
                                </td>
                                <td>
                                    <center>
                                        <?php $tgl_ = explode("-",$ds['tanggal']); ?>
                                        <?php $bln = $this->Kec_model->bulan_ini_saja2($tgl_[1]); ?>
                                        <?= $tgl_[2]." ".$bln." ".$tgl_[0] ?>
                                        <br>
                                        <button class='btn btn-primary' data-toggle="modal" data-target="#modal-edit-<?=$no?>" style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                Edit Tanggal
                                        </button>
                                    </center>
                                    
                                    <center>
                                        <?php $tgl_ = explode("-",$ds['tanggal']); ?>
                                        <?php $bln = $this->Kec_model->bulan_ini_saja2($tgl_[1]); ?>
                                        <br>
                                        <button class='btn btn-danger' data-toggle="modal" data-target="#modal-hapus-<?=$no?>" style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                Hapus Data
                                        </button>
                                    </center>
                                </td>
                                <td style="width:20% !important">
                                    <?php if($ds['berita_acara']!=""): ?>
                                        <center>
                                            <?php $tipe = explode(".",$ds['berita_acara']) ?>
                                            <?php if($tipe[count($tipe)-1]=="pdf"): ?>
                                                <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/pdf.png" style="width:100px;height:100px"/>
                                                <br><br>
                                                <a href="<?=base_url()?>musdes/download_berita/<?=$ds['berita_acara']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= $bulan_ ?>/<?= $tahun_ ?>">
                                                <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                        Download
                                                </button>
                                                </a>
                                                <br><br>
                                            <?php else:?>
                                                <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $ds['berita_acara'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                    <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $ds['berita_acara'] ?>" style="width:100px !important;height:100px !important">
                                                </a>
                                                <a href="<?=base_url()?>musdes/download_berita/<?=$ds['berita_acara']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= $bulan_ ?>/<?= $tahun_ ?>">
                                                <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                        Download
                                                </button>
                                                </a>
                                                <br><br>
                                            <?php endif;?>
                                        </center>
                                        <form method="post" action="<?=base_url().'musdes/berita/'.$ds['id']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= date('Y-m-d') ?>/<?= $ds['nmdesa'] ?>/<?= $ds['tanggal'] ?>" enctype="multipart/form-data">
                                            <input type="file" id="berita_img" name="berita_img" required />
                                            <input class="hidden" type="text" name="kec" value="<?= $kec_ ?>">
                                            <input class="hidden" type="text" name="desa" value="<?= $desa_ ?>">
                                            <input class="hidden" type="text" name="bulan" value="<?= $bulan_ ?>">
                                            <input class="hidden" type="text" name="tahun" value="<?= $tahun_ ?>">
                                            <input type="submit" value="Update Berita Acara" style="background:#a8a9e1;color:black"/>
                                        </form>
                                    <?php else:?>
                                    <form method="post" action="<?=base_url().'musdes/berita/'.$ds['id']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= date('Y-m-d') ?>/<?= $ds['nmdesa'] ?>/<?= $ds['tanggal'] ?>" enctype="multipart/form-data">
                                        <input type="file" id="berita_img" name="berita_img" required />
                                        <input class="hidden" type="text" name="kec" value="<?= $kec_ ?>">
                                        <input class="hidden" type="text" name="desa" value="<?= $desa_ ?>">
                                        <input class="hidden" type="text" name="bulan" value="<?= $bulan_ ?>">
                                        <input class="hidden" type="text" name="tahun" value="<?= $tahun_ ?>">
                                        <input type="submit" value="Upload Berita Acara" />
                                    </form>
                                    <?php endif;?>
                                </td>
                                <td style="width:20% !important">
                                    <?php if($ds['foto1']!=""): ?>
                                    <center>
                                        <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $ds['foto1'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                            <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $ds['foto1'] ?>" style="width:100px !important;height:100px !important">
                                        </a>
                                        <a href="<?=base_url()?>musdes/download_foto/<?=$ds['foto1']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= $bulan_ ?>/<?= $tahun_ ?>">
                                        <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                Download
                                        </button>
                                        </a>
                                        <br><br>
                                    </center>
                                    <form method="post" action="<?=base_url().'musdes/foto1/'.$ds['id']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= date('Y-m-d') ?>/<?= $ds['nmdesa'] ?>/<?= $ds['tanggal'] ?>" enctype="multipart/form-data">
                                        <input type="file" id="dok1_img1" name="dok1_img1" required />
                                        <input class="hidden" type="text" name="kec" value="<?= $kec_ ?>">
                                        <input class="hidden" type="text" name="desa" value="<?= $desa_ ?>">
                                        <input class="hidden" type="text" name="bulan" value="<?= $bulan_ ?>">
                                        <input class="hidden" type="text" name="tahun" value="<?= $tahun_ ?>">
                                        <input type="submit" value="Update Foto 1" style="background:#a8a9e1;color:black"/>
                                    </form>
                                    <?php else:?>
                                    <form method="post" action="<?=base_url().'musdes/foto1/'.$ds['id']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= date('Y-m-d') ?>/<?= $ds['nmdesa'] ?>/<?= $ds['tanggal'] ?>" enctype="multipart/form-data">
                                        <input type="file" id="dok1_img1" name="dok1_img1" required />
                                        <input class="hidden" type="text" name="kec" value="<?= $kec_ ?>">
                                        <input class="hidden" type="text" name="desa" value="<?= $desa_ ?>">
                                        <input class="hidden" type="text" name="bulan" value="<?= $bulan_ ?>">
                                        <input class="hidden" type="text" name="tahun" value="<?= $tahun_ ?>">
                                        <input type="submit" value="Upload Foto 1" />
                                    </form>
                                    <?php endif;?>
                                </td>
                                <td style="width:20% !important">
                                    <?php if($ds['foto2']!=""): ?>
                                        <center>
                                            <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $ds['foto2'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/foto/<?= $ds['foto2'] ?>" style="width:100px !important;height:100px !important">
                                            </a>
                                            <a href="<?=base_url()?>musdes/download_foto/<?=$ds['foto2']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= $bulan_ ?>/<?= $tahun_ ?>">
                                            <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                    Download
                                            </button>
                                            </a>
                                            <br><br>
                                        </center>
                                        <form method="post" action="<?=base_url().'musdes/foto2/'.$ds['id']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= date('Y-m-d') ?>/<?= $ds['nmdesa'] ?>/<?= $ds['tanggal'] ?>" enctype="multipart/form-data">
                                            <input type="file" id="dok1_img2" name="dok1_img2" required />
                                            <input class="hidden" type="text" name="kec" value="<?= $kec_ ?>">
                                            <input class="hidden" type="text" name="desa" value="<?= $desa_ ?>">
                                            <input class="hidden" type="text" name="bulan" value="<?= $bulan_ ?>">
                                            <input class="hidden" type="text" name="tahun" value="<?= $tahun_ ?>">
                                            <input type="submit" value="Update Foto 2" style="background:#a8a9e1;color:black"/>
                                        </form>
                                    <?php else:?>
                                    <form method="post" action="<?=base_url().'musdes/foto2/'.$ds['id']?>/<?= $kec_ ?>/<?= $desa_ ?>/<?= date('Y-m-d') ?>/<?= $ds['nmdesa'] ?>/<?= $ds['tanggal'] ?>" enctype="multipart/form-data">
                                        <input type="file" id="dok1_img2" name="dok1_img2" required />
                                        <input class="hidden" type="text" name="kec" value="<?= $kec_ ?>">
                                        <input class="hidden" type="text" name="desa" value="<?= $desa_ ?>">
                                        <input class="hidden" type="text" name="bulan" value="<?= $bulan_ ?>">
                                        <input class="hidden" type="text" name="tahun" value="<?= $tahun_ ?>">
                                        <input type="submit" value="Upload Foto 2" />
                                    </form>
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php $no++; ?>
                        <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="head_foot">No.</th>
                                        <th class="head_foot">Nama Kelurahan / Desa</th>
                                        <th class="head_foot">Tanggal Pelaksanaan</th>
                                        <th class="head_foot">Berita Acara</th>
                                        <th colspan="2" class="head_foot">Dokumentasi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- /.row -->	
	</div>
	<!-- /.main-content -->
</div>
<script src="<?= base_url() ?>assets/scripts/jquery.min.js"></script>
	<!-- Datepicker -->
	<script src="<?= base_url() ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?= base_url(); ?>assets/script/bootstrap-datepicker.min.js"></script>
<script>
jQuery('.mydatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>