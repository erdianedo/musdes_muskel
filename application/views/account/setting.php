<style type="text/css">
	.navigation .menu li.account a{
		background-color: #63d5db !important;
		color: #fff;
	}
</style>

<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-md-12 col-xs-12">
				<div class="box-content card">
					<h4 class="box-title"><i class="fa fa-user ico"></i>Edit Akun</h4>
					<div class="card-content">
						<?php if($this->session->flashdata('flash_password_ok')) : ?>
							<div class="row mt-3">
							    <div class="col-md-8">
							        <div class="alert alert-success" role="alert">
							          Password Berhasil Dirubah
							          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							            <span aria-hidden="true">&times;</span>
							          </button>
							        </div>
							    </div>
							</div>
						<?php elseif($this->session->flashdata('flash_password_salah')) : ?>
							<div class="row mt-3">
							    <div class="col-md-8">
							        <div class="alert alert-danger" role="alert">
							          <?= $this->session->flashdata('flash_password_salah'); ?>
							          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							            <span aria-hidden="true">&times;</span>
							          </button>
							        </div>
							    </div>
							</div>
						<?php endif;?>
						<div class="row">
							<form method="post" action="">
							<div class="col-md-6">
								<div class="row"><br>
									<div class="col-xs-5"><label>Username</label></div>
									<!-- /.col-xs-5 -->
									<div class="col-xs-7">
										<input type="text" class="form-control" name="username" value="<?= $sesion_data['id']; ?>" disabled>
									</div>
									<!-- /.col-xs-7 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.col-md-6 -->
							<div class="col-md-6">
								<div class="row"><br>
									<div class="col-xs-5"><label>Password Lama</label><span style="color: red">*</span></div>
									<!-- /.col-xs-5 -->
									<div class="col-xs-7">
										<input type="Password" class="form-control" name="psw_lama" required>
									</div>
									<!-- /.col-xs-7 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.col-md-6 -->

							<div class="col-md-6">
								<div class="row"><br>
									<div class="col-xs-5"><label>Password Baru</label><span style="color: red">*</span></div>
									<!-- /.col-xs-5 -->
									<div class="col-xs-7">
										<input type="Password" class="form-control" name="psw_baru" required>
									</div>
									<!-- /.col-xs-7 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.col-md-6 -->
							<div class="col-md-6">
								<div class="row"><br>
									<div class="col-xs-5"><label>Konfirmasi Password</label><span style="color: red">*</span></div>
									<!-- /.col-xs-5 -->
									<div class="col-xs-7">
										<input type="Password" class="form-control" name="psw_konfirmasi" required>
									</div>
									<!-- /.col-xs-7 -->
								</div>
								<!-- /.row -->
							</div>
							<div class="col-md-12"><br>
							    <div class="card">
							        <div class="card-body">
							            <center>
							            <div class="card-body">
							                <button type="submit" class="btn btn-primary" name="tambah">Edit</button>
							            </div>
							            </center>
							        </div>
							    </div>
							</div>
							</form>
							<!-- /.col-md-6 -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>

