<style type="text/css">
	.navigation .menu li.ktp a{
		background-color: #63d5db !important;
		color: #fff;
	}
</style>

<div id="wrapper">
	<div class="main-content">
    <?php if($this->session->flashdata('flash_gambar_gagal')) : ?>
    <div class="row mt-3">
        <div class="col-md-8">
            <div class="alert alert-danger" role="alert">
                <?= $this->session->flashdata('flash_gambar_gagal'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php elseif($this->session->flashdata('flash_gambar')) : ?>
    <div class="row mt-3">
        <div class="col-md-8">
            <div class="alert alert-success" role="alert">
                <?= $this->session->flashdata('flash_gambar'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php endif;?>
		<div class="row small-spacing">
			<div class="col-md-12 col-xs-12">
				<div class="box-content card">
					<h4 class="box-title"><i class="fa fa-user ico"></i>Upload KTP & Buku Rekening</h4>
					<div class="card-content">
                        <h2 style='color:red'>
                            <center>
                            PERTAHATIAN! DIMOHON UNTUK MENGUPLOAD REKENING BANK JATENG!
                            <br>
                            BAGI YANG BELUM MEMILIKI, DIHARAP UNTUK DAPAT SEGERA MEMBUAT REKENING BANK JATENG
                            </center>
                        </h2>
						<div class="row">
                            <div class="table-responsive">
                                <table id="zero_configs" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>KTP</th>
                                            <th>Buku Rekening</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                        <?php if($the_user['ktp']!=""): ?><!-- KTP -->
                                            <center>
                                                <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $the_user['ktp'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                    <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $the_user['ktp'] ?>" style="width:100px !important;height:100px !important">
                                                </a>
                                                <a href="<?=base_url()?>account/download_foto/<?=$the_user['ktp']?>/ktp">
                                                <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                        Download
                                                </button>
                                                </a>
                                                <a href="<?=base_url()?>account/hapus/<?=$the_user['ktp']?>/ktp">
                                                <button class='btn btn-danger' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                        Hapus
                                                </button>
                                                </a>
                                                <br><br>
                                            </center>
                                            <form method="post" action="<?=base_url().'account/upload_ktp/'.$the_user['id']?>" enctype="multipart/form-data">
                                                <input type="file" id="dok1_img1" name="dok1_img1" required />
                                                <input type="submit" value="Upload Ulang KTP" style="background:#a8a9e1;color:black"/>
                                            </form>
                                            <?php else:?>
                                            <form method="post" action="<?=base_url().'account/upload_ktp/'.$the_user['id']?>" enctype="multipart/form-data">
                                                <input type="file" id="dok1_img1" name="dok1_img1" required />
                                                <input type="submit" value="Upload KTP" />
                                            </form>
                                        <?php endif;?>
                                            </td>
                                            <td>
                                        <?php if($the_user['buku_rekening']!=""): ?><!-- BUKU REKENING -->
                                            <center>
                                                <a href="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $the_user['buku_rekening'] ?>" class="item-gallery lightview" data-lightview-group="group">
                                                    <img src="<?= base_url() ?>assets/images/dokumentasi_musdes/berita_acara/<?= $the_user['buku_rekening'] ?>" style="width:100px !important;height:100px !important">
                                                </a>
                                                <a href="<?=base_url()?>account/download_foto/<?=$the_user['buku_rekening']?>/rekening">
                                                <button class='btn btn-info' style='font-size: 11px;line-height: 10px;padding: :5px 10px'>
                                                        Download
                                                </button>
                                                </a>
                                                <a href="<?=base_url()?>account/hapus/<?=$the_user['buku_rekening']?>/rekening">
                                                <button class='btn btn-danger' style='font-size: 11px;line-height: 10px;padding: :5px 10px;'>
                                                        Hapus
                                                </button>
                                                </a>
                                                <br><br>
                                            </center>
                                            <form method="post" action="<?=base_url().'account/upload_rekening/'.$the_user['id']?>" enctype="multipart/form-data">
                                                <input type="file" id="dok1_img2" name="dok1_img2" required />
                                                <input type="submit" value="Upload Ulang Buku Rekening" style="background:#a8a9e1;color:black"/>
                                            </form>
                                            <?php else:?>
                                            <form method="post" action="<?=base_url().'account/upload_rekening/'.$the_user['id']?>" enctype="multipart/form-data">
                                                <input type="file" id="dok1_img2" name="dok1_img2" required />
                                                <input type="submit" value="Upload Buku Rekening" />
                                            </form>
                                        <?php endif;?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.card-content -->
				</div>
			</div>
		</div>
	</div>
</div>

