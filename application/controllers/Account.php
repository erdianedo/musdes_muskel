<?php

    class Account extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model('Admin_model');
            $this->load->model('Account_model');
	        $this->load->model('Kec_model');
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
        }

        public function setting(){
            if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('psw_lama', 'Password Lama', 'required');
                $this->form_validation->set_rules('psw_baru', 'Password Baru', 'required');
                $this->form_validation->set_rules('psw_konfirmasi', 'Konfirmasi Password', 'required');

                if($this->form_validation->run() == FALSE){
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Kec_model->getKotaByKec($data['sesion_data']['id']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('account/setting',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $this->Account_model->editAccount($the_id,$data['sesion_data']['password']);
                    
                }
            }
            else{
                redirect("home");
            }
        }

        public function ktp(){
            if($this->Admin_model->logged_id()){
                $data['sesion'] = $this->Admin_model->logged_id();
                $the_id = $data['sesion'];
                $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                $data['kec'] = $this->Kec_model->getKotaByKec($data['sesion_data']['id']);
                $data['the_user'] = $this->Account_model->get_user($the_id);
                $this->load->view('templates/header',$data);
                $this->load->view('templates/sidebar',$data);
                $this->load->view('account/ktp',$data);
                $this->load->view('templates/footer');
            }
            else{
                redirect("home");
            }
        }

        public function store($id) {

            $filename=preg_replace('/[^A-Za-z0-9\.]/', '', $_FILES['profile_image']['name']);
            $nama_gambar = $id.$filename;
            $config['upload_path'] = './assets/images/users/';
            $config['allowed_types'] = 'jpg|jpeg|png|png';
            // $config['allowed_types'] = 'gif|jpg|png|jpeg|mp4|avi|flv|webm|wmv|ogg|3gp';
            $config['file_name'] = $nama_gambar;
            $config['max_size'] = 2000;//2Mb max
            $this->load->library('upload', $config);

            $data['sesion'] = $this->Admin_model->logged_id();
            $data['user'] = $this->Admin_model->getUserByID($id);
            $the_id = $data['sesion'];
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);


            if (!$this->upload->do_upload('profile_image')) {
                $error = array('error' => $this->upload->display_errors());
                
                $tipe =  explode("/", $_FILES['profile_image']['type']);
                $ukuran = $_FILES['profile_image']['size'];
                
                if($tipe[0] != "image"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                }

                if($ukuran > 2000000){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Ukuran File Terlalu Besar');
                }

                redirect('profile/account/'.$id);
            } else {
                
                if($data['user']['gambar'] != ""){
                    $file = "./assets/images/users/".$data['user']['gambar'];
                    if (is_readable($file) && unlink($file)) {
                    }
                }

                $data = array('image_metadata' => $this->upload->data());
                $image_metadatas = array();
                $image_metadatas = $this->upload->data();
                $nama_file = preg_replace('/[^A-Za-z0-9\.]/', '', $image_metadatas['file_name']);
            
            $data = [
                "gambar" => $nama_gambar
            ];
            $this->db->where('id', $id);
            $this->db->update('user',$data);
                $this->session->set_flashdata('flash_gambar','Gambar Profil Berhasil Diunggah');
            redirect('profile/account/'.$id);   
            }
        }

        public function upload_ktp($id){
            $this->db->like('kode',$id);
            $query = $this->db->get('kode_wil_kemendagri');
            $data['users'] = $query->row_array();
            $nmkab=explode(" ",$data['users']['nmkab']);
            $nmkec=$data['users']['nmkec'];

            $nama_gambar = "KTP_".$nmkab[0]."_".$nmkab[1]."_".$nmkec;
            $config['upload_path'] = './assets/images/dokumentasi_musdes/berita_acara';
            $config['allowed_types'] = 'jpg|jpeg';
            $config['file_name'] = $nama_gambar;
            $config['max_size'] = 520000;//520kb max
            $this->load->library('upload', $config);


            $this->db->where('id',$id);
            $query = $this->db->get('user');
            $data['cek_user'] = $query->row_array();
            if (!$this->upload->do_upload('dok1_img1')) {
                $error = array('error' => $this->upload->display_errors());
                echo $config['upload_path']."<br>";
                echo $config['allowed_types']."<br>";
                echo $config['file_name']."<br>";
                $tipe =  explode("/", $_FILES['dok1_img1']['type']);
                $ukuran = $_FILES['dok1_img1']['size'];
                if($tipe[0] != "image"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('account/ktp');
                }

                if($tipe[1] != "jpg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('account/ktp');
                }
                elseif($tipe[1] != "jpeg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('account/ktp');
                }

                if($ukuran > 520000){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Ukuran File Terlalu Besar');
                    redirect('account/ktp');
                }
            } 
            else {
                $tipe =  explode("/", $_FILES['dok1_img1']['type']);
                if($data['cek_user']['ktp'] != ""){
                    $file = "./assets/images/dokumentasi_musdes/berita_acara/".$data['cek_user']['ktp'];
                    if (is_readable($file) && unlink($file)) {
                    }
                }

                $data = array('image_metadata' => $this->upload->data());
                $image_metadatas = array();
                $image_metadatas = $this->upload->data();
                $nama_file = preg_replace('/[^A-Za-z0-9\.]/', '', $image_metadatas['file_name']);
            
            $data = [
                "ktp" => $image_metadatas['file_name']
            ];
            $this->db->where('id', $id);
            $this->db->update('user',$data);
            $this->session->set_flashdata('flash_gambar','Foto KTP Berhasil Diunggah');
            redirect('account/ktp'); 
            }
        }


        public function upload_rekening($id){
            $this->db->like('kode',$id);
            $query = $this->db->get('kode_wil_kemendagri');
            $data['users'] = $query->row_array();
            $nmkab=explode(" ",$data['users']['nmkab']);
            $nmkec=$data['users']['nmkec'];

            $nama_gambar = "REKENING_".$nmkab[0]."_".$nmkab[1]."_".$nmkec;
            $config['upload_path'] = './assets/images/dokumentasi_musdes/berita_acara';
            $config['allowed_types'] = 'jpg|jpeg';
            $config['file_name'] = $nama_gambar;
            $config['max_size'] = 520000;//520kb max
            $this->load->library('upload', $config);

            $this->db->where('id',$id);
            $query = $this->db->get('user');
            $data['cek_user'] = $query->row_array();
            if (!$this->upload->do_upload('dok1_img2')) {
                $error = array('error' => $this->upload->display_errors());
                $tipe =  explode("/", $_FILES['dok1_img2']['type']);
                $ukuran = $_FILES['dok1_img2']['size'];
                if($tipe[0] != "image"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('account/ktp');
                }

                if($tipe[1] != "jpg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('account/ktp');
                }
                elseif($tipe[1] != "jpeg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('account/ktp');
                }

                if($ukuran > 520000){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Ukuran File Terlalu Besar');
                    redirect('account/ktp');
                }
            } 
            else {
                $tipe =  explode("/", $_FILES['dok1_img2']['type']);
                if($data['cek_user']['buku_rekening'] != ""){
                    $file = "./assets/images/dokumentasi_musdes/berita_acara/".$data['cek_user']['buku_rekening'];
                    if (is_readable($file) && unlink($file)) {
                    }
                }

                $data = array('image_metadata' => $this->upload->data());
                $image_metadatas = array();
                $image_metadatas = $this->upload->data();
                $nama_file = preg_replace('/[^A-Za-z0-9\.]/', '', $image_metadatas['file_name']);
            
            $data = [
                "buku_rekening" => $image_metadatas['file_name']
            ];
            $this->db->where('id', $id);
            $this->db->update('user',$data);
            $this->session->set_flashdata('flash_gambar','Foto Buku Rekening Berhasil Diunggah');
            redirect('account/ktp'); 
            }
        }

        public function download_foto($nama,$jenis){
			if($this->Admin_model->logged_id()){
                $files = $nama;
                if($jenis=='ktp'){
                    $filePath = "assets/images/dokumentasi_musdes/berita_acara/".$files;
                }
                elseif($jenis=='rekening'){
                    $filePath = "assets/images/dokumentasi_musdes/berita_acara/".$files;
                }
			    
				if (!empty($files) && file_exists($filePath)) {
				    header("Cache-Control: public");
				    header("Content-Description: File Transfer");
				    header("Content-Disposition: attachment; filename=$files");
				    header("Content-Type: application/zip");
				    header("Content-Transfer-Encoding: binary");

				    readfile($filePath);
                }
                $id = explode(".",$sesion_data['id']);
                redirect('account/ktp'); 
			}else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function hapus($nama,$jenis){
			if($this->Admin_model->logged_id()){
                $data['sesion'] = $this->Admin_model->logged_id();
                $the_id = $data['sesion'];
                $files = $nama;
                if($jenis=='ktp'){
                    $filePath = "assets/images/dokumentasi_musdes/berita_acara/".$files;
                    if (is_readable($filePath) && unlink($filePath)) {
                    }
                    $data = [
                        "ktp" => ""
                    ];
                    $this->db->where('id', $the_id);
                    $this->db->update('user',$data);
                }
                elseif($jenis=='rekening'){
                    $filePath = "assets/images/dokumentasi_musdes/berita_acara/".$files;
                    if (is_readable($filePath) && unlink($filePath)) {
                    }
                    $data = [
                        "buku_rekening" => ""
                    ];
                    $this->db->where('id', $the_id);
                    $this->db->update('user',$data);
                }
				
                redirect('account/ktp'); 
			}else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }
        public function tet(){
            $url="https://mantra.jatengprov.go.id/dinsos/dinsos_test/test_all";
            $accesskey="8juyss8p28zltg0xt005vnm3ueqfj5ia5rlbhqmyewxqcccscal1bza4x9rrffbq"; //Kunci akses diperoleh dari permohonan akses requester
            $pardata=array();

            $pardata["q_jumlahbaris"]=urlencode("...");
            $pardata["q_awalbaris"]=urlencode("...");
            $par="?".http_build_query($pardata);

            $options=array(
                'http'=>array(
                    'ignore_errors'=>true,
                    'method'=>"GET",
                    'header'=>implode("\r\n",[
                        "Content-Type:application/x-www-form-urlencoded",
                        "Accept:application/json",
                        "Accept-Charset:UTF-8",			
                        "User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0",
                        "AccessKey:$accesskey"
                    ])
                ),
                'ssl'=>array(
                    'verify_peer'=>false
                )
                
            );
            $context=stream_context_create($options);
            $content=file_get_contents($url.$par,false,$context);
            var_dump($content);
        }

        public function test(){
            
            $client = new Client([
                'base_uri' => 'http://172.16.160.43:8080/dukcapil/get_json/33/dinassosial_33/call_nik',
                // default timeout 5 detik
                'timeout'  => 5,
            ]);
            
            $response = $client->request('POST', '/api/login', [
                'json' => [
                    'nik' => '3374012904950002',
                    'user_id' => '1445202005189budi_darmawan',
                    'password' => 'pmkspsks2018'
                ]
            ]);
            $body = $response->getBody();
            $body_array = json_decode($body);
            print_r($body_array);
        }

    }

?>
