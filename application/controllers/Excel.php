
<?php

class Excel extends CI_Controller{

    public function __construct(){
        parent::__construct();
        //load model admin
        $this->load->model('Admin_model');
        $this->load->model('Kec_model');
        $this->load->model('Musdes_model');
        $this->load->model('Ruta_model');
    }

    // KOTA
    public function download_musdes($kec_,$desa_,$bulan,$tahun){
        if($this->Admin_model->logged_id()){ $data['sesion'] = $this->Admin_model->logged_id();
            $ke = explode("%20",$kec_);
            $de = explode("%20",$desa_);
            $kec = ""; $desa = "";
            $kec2 = ""; $desa2 = "";
            for($x=0;$x<count($ke);$x++){
                if($x == 0){
                    $kec = $ke[$x];
                    $kec2 = $ke[$x];
                }
                else{
                    $kec = $kec." ".$ke[$x];
                    $kec2 = $kec2."".$ke[$x];
                }
            }
            for($x=0;$x<count($de);$x++){
                if($x == 0){
                    $desa = $de[$x];
                    $desa2 = $de[$x];
                }
                else{
                    $desa = $desa." ".$de[$x];
                    $desa2 = $desa2."".$de[$x];
                }
            }
            
            $the_id = $data['sesion'];
            $nama = $this->session->userdata('user_name');
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $data['rekap'] = $this->Musdes_model->laporan_rekap($data['sesion_data']['nama'],$kec,$desa,$bulan,$tahun);
            $data['kota'] = $data['sesion_data']['nama'];
            $data['kec'] = $kec2;
            $data['kel'] = $desa2;
            $data['bulan'] = $this->Kec_model->bulan_ini_saja2($bulan);
            $data['tahun'] = $tahun;
            $this->load->view('excel/musdes_excel',$data);
        }else{
            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("home");
        }
    }

    public function download_ruta($kota,$kec_,$desa_,$bulan,$tahun){
        if($this->Admin_model->logged_id()){ $data['sesion'] = $this->Admin_model->logged_id();
            $ke = explode("%20",$kec_);
            $de = explode("%20",$desa_);
            $kec = ""; $desa = "";
            $kec2 = ""; $desa2 = "";
            for($x=0;$x<count($ke);$x++){
                if($x == 0){
                    $kec = $ke[$x];
                    $kec2 = $ke[$x];
                }
                else{
                    $kec = $kec." ".$ke[$x];
                    $kec2 = $kec2."".$ke[$x];
                }
            }
            for($x=0;$x<count($de);$x++){
                if($x == 0){
                    $desa = $de[$x];
                    $desa2 = $de[$x];
                }
                else{
                    $desa = $desa." ".$de[$x];
                    $desa2 = $desa2."".$de[$x];
                }
            }
            $the_id = $data['sesion'];
            $nama = $this->session->userdata('user_name');
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $data['rekap'] = $this->Ruta_model->rekap_kota_ruta($data['sesion_data']['nama'],$kec,$desa,$bulan,$tahun);
            $data['kota'] = $data['sesion_data']['nama'];
            $data['kec'] = $kec2;
            $data['kel'] = $desa2;
            $data['bulan'] = $this->Kec_model->bulan_ini_saja2($bulan);
            $data['tahun'] = $tahun;
            $this->load->view('excel/ruta_excel',$data);
        }else{
            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("home");
        }
    }

    public function download_art($kota,$kec_,$desa_,$bulan,$tahun){
        if($this->Admin_model->logged_id()){ $data['sesion'] = $this->Admin_model->logged_id();
            $ke = explode("%20",$kec_);
            $de = explode("%20",$desa_);
            $kec = ""; $desa = "";
            $kec2 = ""; $desa2 = "";
            for($x=0;$x<count($ke);$x++){
                if($x == 0){
                    $kec = $ke[$x];
                    $kec2 = $ke[$x];
                }
                else{
                    $kec = $kec." ".$ke[$x];
                    $kec2 = $kec2."".$ke[$x];
                }
            }
            for($x=0;$x<count($de);$x++){
                if($x == 0){
                    $desa = $de[$x];
                    $desa2 = $de[$x];
                }
                else{
                    $desa = $desa." ".$de[$x];
                    $desa2 = $desa2."".$de[$x];
                }
            }
            $the_id = $data['sesion'];
            $nama = $this->session->userdata('user_name');
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $data['rekap'] = $this->Ruta_model->rekap_kota_art($data['sesion_data']['nama'],$kec,$desa,$bulan,$tahun);
            $data['kota'] = $data['sesion_data']['nama'];
            $data['kec'] = $kec2;
            $data['kel'] = $desa2;
            $data['bulan'] = $this->Kec_model->bulan_ini_saja2($bulan);
            $data['tahun'] = $tahun;
            $this->load->view('excel/art_excel',$data);
        }else{
            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("home");
        }
    }
    // END KOTA

    // KECAMATAN
    public function download_musdes_kec($kota,$kec_,$bulan,$tahun){
        if($this->Admin_model->logged_id()){ $data['sesion'] = $this->Admin_model->logged_id();
            $ke = explode("%20",$kec_);
            $kec = ""; $kec2 = "";
            for($x=0;$x<count($ke);$x++){
                if($x == 0){
                    $kec = $ke[$x];
                    $kec2 = $ke[$x];
                }
                else{
                    $kec = $kec." ".$ke[$x];
                    $kec2 = $kec2."".$ke[$x];
                }
            }
            $the_id = $data['sesion'];
            $nama = $this->session->userdata('user_name');
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $ko = explode("%20",$kota);
            $data['rekap'] = $this->Musdes_model->laporan_kec_kel($ko[0]." ".$ko[1],$kec,$bulan,$tahun);
            $data['kota'] = $kota;
            $data['kec'] = $kec2;
            $data['bulan'] = $this->Kec_model->bulan_ini_saja2($bulan);
            $data['tahun'] = $tahun;
            $this->load->view('excel/musdes_excel_kec',$data);
        }else{
            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("home");
        }
    }
    public function download_ruta_kec($kota,$kec_,$bulan,$tahun){
        if($this->Admin_model->logged_id()){ $data['sesion'] = $this->Admin_model->logged_id();
            $ke = explode("%20",$kec_);
            $kec = ""; $kec2 = "";
            for($x=0;$x<count($ke);$x++){
                if($x == 0){
                    $kec = $ke[$x];
                    $kec2 = $ke[$x];
                }
                else{
                    $kec = $kec." ".$ke[$x];
                    $kec2 = $kec2."".$ke[$x];
                }
            }
            $the_id = $data['sesion'];
            $nama = $this->session->userdata('user_name');
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $ko = explode("%20",$kota);
            $data['rekap'] = $this->Ruta_model->laporan_kec_ruta($ko[0]." ".$ko[1],$kec,$bulan,$tahun);
            $data['kota'] = $kota;
            $data['kec'] = $kec2;
            $data['bulan'] = $this->Kec_model->bulan_ini_saja2($bulan);
            $data['tahun'] = $tahun;
            $this->load->view('excel/ruta_excel_kec',$data);
        }else{
            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("home");
        }
    }
    public function download_art_kec($kota,$kec_,$bulan,$tahun){
        if($this->Admin_model->logged_id()){ $data['sesion'] = $this->Admin_model->logged_id();
            $ke = explode("%20",$kec_);
            $kec = ""; $kec2 = "";
            for($x=0;$x<count($ke);$x++){
                if($x == 0){
                    $kec = $ke[$x];
                    $kec2 = $ke[$x];
                }
                else{
                    $kec = $kec." ".$ke[$x];
                    $kec2 = $kec2."".$ke[$x];
                }
            }
            $the_id = $data['sesion'];
            $nama = $this->session->userdata('user_name');
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $ko = explode("%20",$kota);
            $data['rekap'] = $this->Ruta_model->laporan_kec_art($ko[0]." ".$ko[1],$kec,$bulan,$tahun);
            $data['kota'] = $kota;
            $data['kec'] = $kec2;
            $data['bulan'] = $this->Kec_model->bulan_ini_saja2($bulan);
            $data['tahun'] = $tahun;
            $this->load->view('excel/art_excel_kec',$data);
        }else{
            //jika session belum terdaftar, maka redirect ke halaman login
            redirect("home");
        }
    }
    // END KECAMATAN


}
?>