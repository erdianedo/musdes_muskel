<?php

	class Dashboard extends CI_Controller{

		public function __construct(){
	        parent::__construct();
	        //load model admin
	        $this->load->model('Admin_model');
	        $this->load->model('Kec_model');
	        $this->load->model('Musdes_model');
	    }

		public function index(){

			if($this->Admin_model->logged_id()){
                $data['sesion'] = $this->Admin_model->logged_id();
                $the_id = $data['sesion'];
				$nama = $this->session->userdata('user_name');
				$data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
				$data['kec'] = $this->Kec_model->getKotaByKec($data['sesion_data']['id']);
				$data['kel'] = $this->Kec_model->getKelByKec($data['sesion_data']['id']);
				$bln = date('m');
				switch($bln){
					case '01':
						$bulan_r = "Januari";
					break;
					case '02':
						$bulan_r = "Februari";
					break;
					case '03':
						$bulan_r = "Maret";
					break;
					case '04':
						$bulan_r = "April";
					break;
					case '05':
						$bulan_r = "Mei";
					break;
					case '06':
						$bulan_r = "Juni";
					break;
					case '07':
						$bulan_r = "Juli";
					break;
					case '08':
						$bulan_r = "Agustus";
					break;
					case '09':
						$bulan_r = "September";
					break;
					case '10':
						$bulan_r = "Oktober";
					break;
					case '11':
						$bulan_r = "November";
					break;
					case '12':
						$bulan_r = "Desember";
					break;
					default:
						$bulan_r = "Tidak di ketahui";     
					break;
				}

				$kode = explode(".",$data['sesion_data']['id']);

				if($kode[1]=="00" && $kode[2]=="00"){//user provinsi
					$data['rekap_prov'] = $this->Musdes_model->laporan_dinsos($bln,date('Y'));
					$data['bulan'] = $bulan_r;
					$data['bulan2'] = $bln;
					$data['tahun'] = date('Y');
				}	
				elseif($kode[1]!="00" && $kode[2]=="00"){//user kota
					$data['rekap_kota'] = $this->Musdes_model->laporan_kec_bykota($data['sesion_data']['nama'],$bln,date('Y'));
					$data['rekap_kota2'] = $this->Musdes_model->laporan_kec_bykota2($data['sesion_data']['nama'],$bln,date('Y'));
					$data['bulan'] = $bulan_r;
					$data['bulan2'] = $bln;
					$data['tahun'] = date('Y');
				}
				else{//user kecamatan
					$data['rekap_kec'] = $this->Musdes_model->laporan_kec_kel($data['kec']['nmkab'],$data['sesion_data']['nama'],$bln,date('Y'));
					$data['bulan'] = $bulan_r;
					$data['bulan2'] = $bln;
					$data['tahun'] = date('Y');
				}
				

				$this->load->view('templates/header',$data);
	            $this->load->view('templates/sidebar',$data);
				$this->load->view('dashboard/index',$data);
				$this->load->view('templates/footer');
	        }else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
		}

		public function logout(){
	        $this->session->sess_destroy();
	        redirect('home');
	    }
	}

?>
