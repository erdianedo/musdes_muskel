<?php

	class Home extends CI_Controller{
		public function __construct(){
            parent::__construct();
            $this->load->model('Admin_model');
        }

		public function index(){
			if($this->Admin_model->logged_id()){
	            //jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
	            redirect("dashboard");
	        }
	        else{
	        	$this->load->view('home/index');
	        }
		}
	}

?>
