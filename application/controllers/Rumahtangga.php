<?php

	class Rumahtangga extends CI_Controller{

		public function __construct(){
	        parent::__construct();
	        //load model admin
	        $this->load->model('Admin_model');
	        $this->load->model('Kec_model');
	        $this->load->model('Musdes_model');
	        $this->load->model('Ruta_model');
	    }

        public function ruta(){
			if($this->Admin_model->logged_id()){
                $data['sesion'] = $this->Admin_model->logged_id();
                $the_id = $data['sesion'];
                $nama = $this->session->userdata('user_name');
                $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

                $this->form_validation->set_rules('kel[]', 'Tanggal Pelaksanaan', 'required');

                if($this->form_validation->run() == FALSE){ 
                    
                    $data['desa'] = $this->Kec_model->getKelByKec($data['sesion_data']['id']);
                    $data['bulan_ini'] = $this->Kec_model->bulan_ini();
                    $data['tahun_ini'] = date("Y");
                    $data['ruta'] = $this->Ruta_model->get_ruta($data['kec']['nmkab'],$data['sesion_data']['nama'],$data['bulan_ini']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/ruta',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $status = $this->input->post('status', true);
                    $bln = $this->input->post('bulan', true);
                    if($status=="baru"){
                        $this->Ruta_model->input_ruta($data['kec']['nmkab'],$bln);
                    }
                    elseif($status=="update"){
                        $this->Ruta_model->update_ruta();
                    }
                }
	        }else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function art(){
			if($this->Admin_model->logged_id()){
                $data['sesion'] = $this->Admin_model->logged_id();
                $the_id = $data['sesion'];
                $nama = $this->session->userdata('user_name');
                $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

                $this->form_validation->set_rules('kel[]', 'Tanggal Pelaksanaan', 'required');

                if($this->form_validation->run() == FALSE){ 
                   
                    $data['desa'] = $this->Kec_model->getKelByKec($data['sesion_data']['id']);
                    $data['bulan_ini'] = $this->Kec_model->bulan_ini();
                    $data['art'] = $this->Ruta_model->get_art($data['kec']['nmkab'],$data['sesion_data']['nama'],$data['bulan_ini']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/art',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $status = $this->input->post('status', true);
                    if($status=="baru"){
                        $this->Ruta_model->input_art($data['kec']['nmkab']);
                    }
                    elseif($status=="update"){
                        $this->Ruta_model->update_art();
                    }
                }
	        }else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function tgl_ruta(){
			if($this->Admin_model->logged_id()){

                $data['sesion'] = $this->Admin_model->logged_id();
                $the_id = $data['sesion'];
                $nama = $this->session->userdata('user_name');
                $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

                $this->form_validation->set_rules('bulan_', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun_', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    
                    $data['desa'] = $this->Kec_model->getKelByKec($data['sesion_data']['id']);
                    $data['bulan_ini'] = $this->Kec_model->bulan_ini();
                    $data['tahun_ini'] = date("Y");
                    $data['ruta'] = $this->Ruta_model->get_ruta($data['kec']['nmkab'],$data['sesion_data']['nama'],$data['bulan_ini']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/ruta',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['desa'] = $this->Kec_model->getKelByKec($data['sesion_data']['id']);
                    $data['bulan_ini'] = $this->Kec_model->bulan_ini_saja2($this->input->post('bulan_', true));
                    $data['tahun_ini'] = $this->input->post('tahun_', true);
                    $data['thn_bln'] = $this->input->post('tahun_', true)."-".$this->input->post('bulan_', true);
                    $data['ruta'] = $this->Ruta_model->get_ruta2($data['kec']['nmkab'],$data['sesion_data']['nama'],$data['thn_bln']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/ruta',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function tgl_art(){
			if($this->Admin_model->logged_id()){

                $data['sesion'] = $this->Admin_model->logged_id();
                $the_id = $data['sesion'];
                $nama = $this->session->userdata('user_name');
                $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

                $this->form_validation->set_rules('bulan_', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun_', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    
                    $data['desa'] = $this->Kec_model->getKelByKec($data['sesion_data']['id']);
                    $data['bulan_ini'] = $this->Kec_model->bulan_ini();
                    $data['tahun_ini'] = date("Y");
                    $data['art'] = $this->Ruta_model->get_art($data['kec']['nmkab'],$data['sesion_data']['nama'],$data['bulan_ini']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/art',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['desa'] = $this->Kec_model->getKelByKec($data['sesion_data']['id']);
                    $data['bulan_ini'] = $this->Kec_model->bulan_ini_saja2($this->input->post('bulan_', true));
                    $data['tahun_ini'] = $this->input->post('tahun_', true);
                    $data['thn_bln'] = $this->input->post('tahun_', true)."-".$this->input->post('bulan_', true);
                    $data['art'] = $this->Ruta_model->get_art2($data['kec']['nmkab'],$data['sesion_data']['nama'],$data['thn_bln']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/art',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function laporan_ruta_kec(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->laporan_kec_ruta($data['kec']['nmkab'],$data['sesion_data']['nama'],date('m'),date('Y'));
                    $data['tahun_'] = date('Y');
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_ruta',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['lap'] = $this->Ruta_model->laporan_kec_ruta($data['kec']['nmkab'],$data['sesion_data']['nama'],$bulan,$tahun);
                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_ruta',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function lap_ruta_kec($kec,$bulan,$tahun){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->laporan_kec_ruta($data['kec']['nmkab'],$kec,$bulan,$tahun);
                    $data['tahun_'] = $tahun;
                    $data['kec_'] = "";
                    $data['desa_'] = "";
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_ruta',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['lap'] = $this->Ruta_model->laporan_kec_ruta($data['kec']['nmkab'],$data['sesion_data']['nama'],$bulan,$tahun);
                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_ruta',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function lap_art_kec($kec,$bulan,$tahun){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->laporan_kec_art($data['kec']['nmkab'],$kec,$bulan,$tahun);
                    $data['tahun_'] = $tahun;
                    $data['kec_'] = "";
                    $data['desa_'] = "";
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_art',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['lap'] = $this->Ruta_model->laporan_kec_art($data['kec']['nmkab'],$data['sesion_data']['nama'],$bulan,$tahun);

                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_art',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function update_data_ruta(){
			if($this->Admin_model->logged_id()){
                $this->Ruta_model->update_data_ruta();
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function update_data_art(){
			if($this->Admin_model->logged_id()){
                $this->Ruta_model->update_data_art();
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function laporan_art_kec(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->laporan_kec_art($data['kec']['nmkab'],$data['sesion_data']['nama'],date('m'),date('Y'));
                    $data['bulan_'] = "";
                    $data['tahun_'] = date('Y');
                    $data['kec_'] = "";
                    $data['desa_'] = "";
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_art',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['lap'] = $this->Ruta_model->laporan_kec_art($data['kec']['nmkab'],$data['sesion_data']['nama'],$bulan,$tahun);

                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/data_art',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function rekap_ruta(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->laporan_kec_ruta($data['kec']['nmkab'],$data['sesion_data']['nama'],date('m'),date('Y'));
                    $data['kota'] = $data['kec']['nmkab'];
                    $data['kec_2'] = $data['sesion_data']['nama'];
                    $data['bulan'] = date('m');
                    $data['bulan_'] = "";
                    $data['tahun_'] = date('Y');
                    $data['kec_'] = "";
                    $data['desa_'] = "";
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_ruta',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['lap'] = $this->Ruta_model->laporan_kec_ruta($data['kec']['nmkab'],$data['sesion_data']['nama'],$bulan,$tahun);
                    $data['kota'] = $data['kec']['nmkab'];
                    $data['kec_2'] = $data['sesion_data']['nama'];
                    $data['bulan'] = $bulan;
                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_ruta',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function rekap_art(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->laporan_kec_art($data['kec']['nmkab'],$data['sesion_data']['nama'],date('m'),date('Y'));
                    $data['kota'] = $data['kec']['nmkab'];
                    $data['kec_2'] = $data['sesion_data']['nama'];
                    $data['bulan'] = date('m');
                    $data['bulan_'] = "";
                    $data['tahun_'] = date('Y');
                    $data['kec_'] = "";
                    $data['desa_'] = "";
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_art',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    $data['kota'] = $data['kec']['nmkab'];
                    $data['kec_2'] = $data['sesion_data']['nama'];
                    $data['bulan'] = $bulan;
                    $data['lap'] = $this->Ruta_model->laporan_kec_art($data['kec']['nmkab'],$data['sesion_data']['nama'],$bulan,$tahun);
                   
                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_art',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function rekap_ruta_kota(){
            if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('kec', 'Kecamatan', 'required');
                $this->form_validation->set_rules('kel', 'Kelurahan', 'required');
                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec_'] = $this->Musdes_model->get_kec_by_kota($data['sesion_data']['nama']);
                    $data['desa_'] = $this->Musdes_model->getKelByDesaByKec($data['kec_'][0]['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->rekap_kota_ruta($data['sesion_data']['nama'],"semua","semua",date('m'),date('Y'));
                    $data['bulan'] = date('m');
                    $data['tahun'] = date('Y');
                    $data['kec'] = "semua";
                    $data['kel'] = "semua";
                    $data['kota'] = $data['sesion_data']['nama'];
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_ruta_kota',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec_'] = $this->Musdes_model->get_kec_by_kota($data['sesion_data']['nama']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec_'][0]['nmkab'],$data['sesion_data']['nama']);

                    if($this->input->post('kec', true)=="semua"){
                        $kec = $this->input->post('kec', true);
                    }
                    else{
                        $kecs = explode("_",$this->input->post('kec', true));
                        $kec = $kecs[1];
                    }
                    if($this->input->post('kel', true)=="semua"){
                        $kel = $this->input->post('kel', true);
                    }
                    else{
                        $kels = explode("_",$this->input->post('kel', true));
                        $kel = $kels[1];
                    }
                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    $data['lap'] = $this->Ruta_model->rekap_kota_ruta($data['sesion_data']['nama'],$kec,$kel,$bulan,$tahun);

                    $data['bulan'] = $bulan;
                    $data['tahun'] = $tahun;
                    $data['kec'] = $kec;
                    $data['kel'] = $kel;
                    $data['kota'] = $data['sesion_data']['nama'];
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_ruta_kota',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function rekap_art_kota(){
            if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('kec', 'Kecamatan', 'required');
                $this->form_validation->set_rules('kel', 'Kelurahan', 'required');
                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec_'] = $this->Musdes_model->get_kec_by_kota($data['sesion_data']['nama']);
                    $data['desa_'] = $this->Musdes_model->getKelByDesaByKec($data['kec_'][0]['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Ruta_model->rekap_kota_art($data['sesion_data']['nama'],"semua","semua",date('m'),date('Y'));
                    $data['bulan'] = date('m');
                    $data['tahun'] = date('Y');
                    $data['kec'] = "semua";
                    $data['kel'] = "semua";
                    $data['kota'] = $data['sesion_data']['nama'];
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_art_kota',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec_'] = $this->Musdes_model->get_kec_by_kota($data['sesion_data']['nama']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec_'][0]['nmkab'],$data['sesion_data']['nama']);

                    if($this->input->post('kec', true)=="semua"){
                        $kec = $this->input->post('kec', true);
                    }
                    else{
                        $kecs = explode("_",$this->input->post('kec', true));
                        $kec = $kecs[1];
                    }
                    if($this->input->post('kel', true)=="semua"){
                        $kel = $this->input->post('kel', true);
                    }
                    else{
                        $kels = explode("_",$this->input->post('kel', true));
                        $kel = $kels[1];
                    }
                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    $data['lap'] = $this->Ruta_model->rekap_kota_art($data['sesion_data']['nama'],$kec,$kel,$bulan,$tahun);

                    $data['bulan'] = $bulan;
                    $data['tahun'] = $tahun;
                    $data['kec'] = $kec;
                    $data['kel'] = $kel;
                    $data['kota'] = $data['sesion_data']['nama'];
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('rumah_tangga/rekap_art_kota',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }


	}

?>
