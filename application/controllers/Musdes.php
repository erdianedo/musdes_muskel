<?php

	class Musdes extends CI_Controller{

		public function __construct(){
	        parent::__construct();
	        //load model admin
	        $this->load->model('Admin_model');
	        $this->load->model('Kec_model');
	        $this->load->model('Musdes_model');
            $this->db3 = $this->load->database('db_tiga', TRUE);
            
	    }

        public function isi_id_pmks(){
            
            // $db2 = $this->load->database('db_dua',TRUE);
            // $query = $db2->select('nama')->get('usulan_pmks_2021');
            // $aa = $query->result_array();

            // var_dump(count($aa));

            
            $db2 = $this->load->database('db_dua',TRUE);
            $query = $db2->get('usulan_pmks_2021');
            $aa = $query->result_array();
            // var_dump($aa[0]['id']);
            // var_dump($query->num_rows());
            exit();
            $id = 0;
            // for($x=0;$x<10;$x++){
                $id = $x+1;
                $data = [
                    "desa_ada" => "aaaa"
                ];
                $db2->where('id',$aa[5]['id']);
                $db2->update('usulan_pmks_2021',$data);
            // }

        } 

        public function show_kecamatan($id_kota){
            echo $this->Musdes_model->show_kecamatan($id_kota);
        } 

        public function show_kelurahan($id_kec){
            echo $this->Musdes_model->show_kelurahan($id_kec);
        }

        public function show_kelurahan_kota($id_kec){
            $id = explode("_",$id_kec);
            echo $this->Musdes_model->show_kelurahan($id[0]);
        }

		public function laporan(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('tanggal_pelaksanaan', 'Tanggal Pelaksanaan', 'required');
                $this->form_validation->set_rules('desa', 'Nama Kelurahan / Desa', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/laporan',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $this->Musdes_model->input_laporan();
                }
	        }else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }
        
        public function laporan_kec(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('desa', 'Nama Kelurahan / Desa', 'required');
                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Musdes_model->laporan_kec($data['sesion_data']['id'],"semua",date('m'),date('Y'));
                    $data['bulan_'] = date('m');
                    $data['tahun_'] = date('Y');
                    $data['kec_'] = $data['sesion_data']['nama'];
                    $data['desa_'] = "semua";
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2(date('m'));
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/data',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $desa = explode("_",$this->input->post('desa', true));
                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['lap'] = $this->Musdes_model->laporan_kec($desa[1],$desa[0],$bulan,$tahun);

                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['kec_'] = $desa[1];
                    $data['desa_'] = $desa[0];
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan); 
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/data',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function edit_tgl_musdes($id,$kec,$desa,$bulan,$tahun){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('id', 'id', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Musdes_model->laporan_kec($kec,$desa,$bulan,$tahun);
                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['kec_'] = $kec;
                    $data['desa_'] = $desa;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);  
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/data',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $tgl_ = $this->input->post('tanggal_baru', true);
                    $nmdesa = $this->input->post('nmdesa', true);
                    $this->Musdes_model->edit_musdes($id,$kec,$desa,$bulan,$tahun,$nmdesa,$tgl_);
                }
	        }else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function laporan_kecamatan($kec,$desa,$bulan,$tahun){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('desa', 'Nama Kelurahan / Desa', 'required');
                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);
                    $data['lap'] = $this->Musdes_model->laporan_kec($kec,$desa,$bulan,$tahun);
                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['kec_'] = $kec;
                    $data['desa_'] = $desa;
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);  
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/data',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $data['desa'] = $this->Musdes_model->getKelByDesaByKec($data['kec']['nmkab'],$data['sesion_data']['nama']);

                    $desa = explode("_",$this->input->post('desa', true));
                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['lap'] = $this->Musdes_model->laporan_kec($desa[1],$desa[0],$bulan,$tahun);

                    $data['bulan_'] = $bulan;
                    $data['tahun_'] = $tahun;
                    $data['kec_'] = $desa[1];
                    $data['desa_'] = $desa[0];
                    $data['bulan_2'] = $this->Kec_model->bulan_ini_saja2($bulan);                       

                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/data',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }
        
        public function rekap(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);
                    $bln = date('m');
                    switch($bln){
                        case '01':
                            $bulan_r = "Januari";
                        break;
                        case '02':
                            $bulan_r = "Februari";
                        break;
                        case '03':
                            $bulan_r = "Maret";
                        break;
                        case '04':
                            $bulan_r = "April";
                        break;
                        case '05':
                            $bulan_r = "Mei";
                        break;
                        case '06':
                            $bulan_r = "Juni";
                        break;
                        case '07':
                            $bulan_r = "Juli";
                        break;
                        case '08':
                            $bulan_r = "Agustus";
                        break;
                        case '09':
                            $bulan_r = "September";
                        break;
                        case '10':
                            $bulan_r = "Oktober";
                        break;
                        case '11':
                            $bulan_r = "November";
                        break;
                        case '12':
                            $bulan_r = "Desember";
                        break;
                        default:
                            $bulan_r = "Tidak di ketahui";     
                        break;
                    }
                    // var_dump($bulan_r);
                    $data['rekap'] = $this->Musdes_model->laporan_kec_kel($data['kec']['nmkab'],$data['sesion_data']['nama'],$bln,date('Y'));
                    $data['bulan2'] = $this->Kec_model->bulan_ini_saja2($bln);
                    $data['kota'] = $data['kec']['nmkab'];
                    $data['kec_'] = $data['sesion_data']['nama'];
                    $data['kel'] = "semua";
                    $data['bulan'] = $bln;
                    $data['tahun'] = date('Y');
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/rekap',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    
                    $data['rekap'] = $this->Musdes_model->laporan_kec_kel($data['kec']['nmkab'],$data['sesion_data']['nama'],$bulan,$tahun);
                    $data['kota'] = $data['kec']['nmkab'];
                    $data['kec_'] = $data['sesion_data']['nama'];
                    $data['kel'] = "semua";
                    $data['bulan'] = $bulan;
                    $data['tahun'] = $tahun;
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/rekap',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        function json(){
            $this->load->library('datatables');
            $this->datatables->select('*');
            $this->datatables->from('karyawan');
            return print_r($this->datatables->generate());
        }

        public function rekap_prov(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('kota', 'Kota', 'required');
                $this->form_validation->set_rules('kec', 'Kec', 'required');
                $this->form_validation->set_rules('kel', 'Kel', 'required');
                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kota_'] = $this->Kec_model->get_AllKota();
                    $bln = date('m');
                    switch($bln){
                        case '01':
                            $bulan_r = "Januari";
                        break;
                        case '02':
                            $bulan_r = "Februari";
                        break;
                        case '03':
                            $bulan_r = "Maret";
                        break;
                        case '04':
                            $bulan_r = "April";
                        break;
                        case '05':
                            $bulan_r = "Mei";
                        break;
                        case '06':
                            $bulan_r = "Juni";
                        break;
                        case '07':
                            $bulan_r = "Juli";
                        break;
                        case '08':
                            $bulan_r = "Agustus";
                        break;
                        case '09':
                            $bulan_r = "September";
                        break;
                        case '10':
                            $bulan_r = "Oktober";
                        break;
                        case '11':
                            $bulan_r = "November";
                        break;
                        case '12':
                            $bulan_r = "Desember";
                        break;
                        default:
                            $bulan_r = "Tidak di ketahui";     
                        break;
                    }
                    // var_dump($bulan_r);
                    $data['kota'] = "";
                    $data['rekap'] = $this->Musdes_model->laporan_kec_kel("","",$bln,date('Y'));
                    $data['bulan'] = $bln;
                    $data['tahun'] = date('Y');
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/rekap_prov',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kota_'] = $this->Kec_model->get_AllKota();

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    $kota = $this->input->post('kota', true);
                    $kec = $this->input->post('kec', true);
                    $kel = $this->input->post('kel', true);
                    
                    if($kota!="semua"){
                        $this->db->like('kode',$kota);
                        $query = $this->db->get('kode_wil_kemendagri');
                        $data['nm_kota'] = $query->row_array();
                        $data['kota'] = $data['nm_kota']['nmkab'];
                    }
                    else{
                        $data['kota'] = $kota;
                    }
                    $kecamatan = explode("_",$kec);
                    $kelurahan = explode("_",$kel);
                    if($kecamatan[0]=="semua"){
                        $kec = $kecamatan[0];
                    }
                    else{
                        $kec = $kecamatan[1];
                    }
                    if($kelurahan[0]=="semua"){
                        $kel = $kelurahan[0];
                    }
                    else{
                        $kel = $kelurahan[1];
                    }
                    $kota = $data['kota'];
                    $data['kec'] = $kec;
                    $data['kel'] = $kel;
                    $data['bulan'] = $bulan;
                    $data['tahun'] = $tahun;
                    $data['rekap'] = $this->Musdes_model->laporan_rekap($kota,$kec,$kel,$bulan,$tahun);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/rekap_prov',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function rekap_kota(){
			if($this->Admin_model->logged_id()){

                $this->form_validation->set_rules('kota', 'Kota', 'required');
                $this->form_validation->set_rules('kec', 'Kec', 'required');
                $this->form_validation->set_rules('kel', 'Kel', 'required');
                $this->form_validation->set_rules('bulan', 'Bulan', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');

                if($this->form_validation->run() == FALSE){ 
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec_'] = $this->Kec_model->get_AllKec($data['sesion_data']['nama']);
                    $bln = date('m');
                    switch($bln){
                        case '01':
                            $bulan_r = "Januari";
                        break;
                        case '02':
                            $bulan_r = "Februari";
                        break;
                        case '03':
                            $bulan_r = "Maret";
                        break;
                        case '04':
                            $bulan_r = "April";
                        break;
                        case '05':
                            $bulan_r = "Mei";
                        break;
                        case '06':
                            $bulan_r = "Juni";
                        break;
                        case '07':
                            $bulan_r = "Juli";
                        break;
                        case '08':
                            $bulan_r = "Agustus";
                        break;
                        case '09':
                            $bulan_r = "September";
                        break;
                        case '10':
                            $bulan_r = "Oktober";
                        break;
                        case '11':
                            $bulan_r = "November";
                        break;
                        case '12':
                            $bulan_r = "Desember";
                        break;
                        default:
                            $bulan_r = "Tidak di ketahui";     
                        break;
                    }
                    // var_dump($bulan_r);
                    $data['kota'] = "";
                    $data['rekap'] = $this->Musdes_model->laporan_rekap($data['sesion_data']['nama'],"semua","semua",$bln,date('Y'));
                    $data['kota'] = $data['sesion_data']['nama'];
                    $data['kec'] = "semua";
                    $data['kel'] = "semua";
                    $data['bulan'] = $bln;
                    $data['tahun'] = date('Y');
                    $data['tahun'] = date('Y');
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/rekap_kota',$data);
                    $this->load->view('templates/footer');
                }
                else{
                    $data['sesion'] = $this->Admin_model->logged_id();
                    $the_id = $data['sesion'];
                    $nama = $this->session->userdata('user_name');
                    $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
                    $data['kec_'] = $this->Kec_model->get_AllKec($data['sesion_data']['nama']);

                    $bulan = $this->input->post('bulan', true);
                    $tahun = $this->input->post('tahun', true);
                    $kota = $this->input->post('kota', true);
                    $kec = $this->input->post('kec', true);
                    $kel = $this->input->post('kel', true);
                    
                    $kecamatan = explode("_",$kec);
                    $kelurahan = explode("_",$kel);
                    if($kecamatan[0]=="semua"){
                        $kec = $kecamatan[0];
                    }
                    else{
                        $kec = $kecamatan[1];
                    }
                    if($kelurahan[0]=="semua"){
                        $kel = $kelurahan[0];
                    }
                    else{
                        $kel = $kelurahan[1];
                    }
                    $data['kota'] = $kota;
                    $data['kec'] = $kec;
                    $data['kel'] = $kel;
                    $data['bulan'] = $bulan;
                    $data['tahun'] = $tahun;
                    $data['rekap'] = $this->Musdes_model->laporan_rekap($kota,$kec,$kel,$bulan,$tahun);
                    $this->load->view('templates/header',$data);
                    $this->load->view('templates/sidebar',$data);
                    $this->load->view('musdes/rekap_kota',$data);
                    $this->load->view('templates/footer');
                }
            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function rekap_dataprov($kota,$kec,$kel,$bulan,$tahun){
            $this->load->library('datatables');
            $this->datatables->select('*');
            $this->datatables->from('laporan');
            return print_r($this->datatables->generate());
        }

        public function get_data_musdes($kota,$kec,$kel,$bulan,$tahun){
            $list = $this->Musdes_model->get_datatables($kota,$kec,$kel,$bulan,$tahun);
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $field) {
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $field->nmkab;
                $row[] = $field->nmkec;
                $row[] = $field->nmdesa;
                $row[] = $field->kode;
                $row[] = $field->kode;
                
    
                $data[] = $row;
            }
    
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Musdes_model->count_all(),
                "recordsFiltered" => $this->Musdes_model->count_filtered(),
                "data" => $data,
            );
            //output dalam format JSON
            echo json_encode($output);
        }

        function data_barang($kota){
            $kota = explode("%20",$kota);
            $data=$this->Musdes_model->barang_list($kota[0]." ".$kota[1]);
            echo json_encode($data);
        }

        public function foto1($id,$kec,$desa,$tgl,$nmdesa,$tanggal){
            $data['sesion'] = $this->Admin_model->logged_id();
            $the_id = $data['sesion'];
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

            $this->db->where('id',$the_id);
            $query = $this->db->get('user');
            $data['kec'] = $query->result_array();
            $nama_kec=$data['kec'][0]['nama'];


            $bulan = $this->input->post('bulan', true);
            $tahun = $this->input->post('tahun', true);
            $kec = $this->input->post('kec', true);
            $kel = $this->input->post('desa', true);
            
            $huruf=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
            $a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);
            $e = rand(0,9);

            $ids = $huruf[$a].$e.$huruf[$b].$c.$d;

            $filename=preg_replace('/[^A-Za-z0-9\.]/', '', $_FILES['dok1_img1']['name']);
            date_default_timezone_set('Asia/Jakarta');
            $nama_gambar = $ids."_dok_".$nama_kec."_".$nmdesa."_".$tanggal."_".date('H_i_s');
            $config['upload_path'] = './assets/images/dokumentasi_musdes/foto';
            $config['allowed_types'] = 'jpg|jpeg';
            $config['file_name'] = $nama_gambar;
            $config['max_size'] = 5200;//5200Mb max
            $this->load->library('upload', $config);

            $this->db->where('id',$id);
            $query = $this->db->get('laporan');
            $data['laporan'] = $query->row_array();

            if (!$this->upload->do_upload('dok1_img1')) {
                $error = array('error' => $this->upload->display_errors());
                $tipe =  explode("/", $_FILES['dok1_img1']['type']);
                $ukuran = $_FILES['dok1_img1']['size'];

                if($tipe[0] == "image" && $ukuran > 5200 || $tipe[1] == "jpg" && $ukuran > 5200 || $tipe[1] == "jpeg" && $ukuran > 5200){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Ukuran File Terlalu Besar');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }

                if($tipe[0] != "image"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }

                if($tipe[1] != "jpg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }
                elseif($tipe[1] != "jpeg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }
            } 
            else {
                $tipe =  explode("/", $_FILES['dok1_img1']['type']);
                if($data['laporan']['foto1'] != ""){
                    $file = "./assets/images/dokumentasi_musdes/foto/".$data['laporan']['foto1'];
                    if (is_readable($file) && unlink($file)) {
                    }
                }
                $data = array('image_metadata' => $this->upload->data());
                $image_metadatas = array();
                $image_metadatas = $this->upload->data();
                $nama_file = preg_replace('/[^A-Za-z0-9\.]/', '', $image_metadatas['file_name']);
            
            $data = [
                "foto1" => $image_metadatas['file_name']
            ];
            $this->db->where('id', $id);
            $this->db->update('laporan',$data);
            $this->session->set_flashdata('flash_gambar','Foto Dokumentasi Berhasil Diunggah');
            redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);   
            }
        }

        public function foto2($id,$kec,$desa,$tgl,$nmdesa,$tanggal){
            $data['sesion'] = $this->Admin_model->logged_id();
            $the_id = $data['sesion'];
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

            $this->db->where('id',$the_id);
            $query = $this->db->get('user');
            $data['kec'] = $query->result_array();
            $nama_kec=$data['kec'][0]['nama'];

            $bulan = $this->input->post('bulan', true);
            $tahun = $this->input->post('tahun', true);
            $kec = $this->input->post('kec', true);
            $kel = $this->input->post('desa', true);

            $huruf=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
            $a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);
            $e = rand(0,9);

            $ids = $huruf[$a].$e.$huruf[$b].$c.$d;
            $filename=preg_replace('/[^A-Za-z0-9\.]/', '', $_FILES['dok1_img2']['name']);
            date_default_timezone_set('Asia/Jakarta');
            $nama_gambar = $ids."_dok_".$nama_kec."_".$nmdesa."_".$tanggal."_".date('H_i_s');
            $config['upload_path'] = './assets/images/dokumentasi_musdes/foto';
            $config['allowed_types'] = 'jpg|jpeg';
            $config['file_name'] = $nama_gambar;
            $config['max_size'] = 5200;//5,2Mb max
            $this->load->library('upload', $config);

            $this->db->where('id',$id);
            $query = $this->db->get('laporan');
            $data['laporan'] = $query->row_array();

            if (!$this->upload->do_upload('dok1_img2')) {
                $error = array('error' => $this->upload->display_errors());
                $tipe =  explode("/", $_FILES['dok1_img2']['type']);
                $ukuran = $_FILES['dok1_img2']['size'];

                if($tipe[0] == "image" && $ukuran > 5200 || $tipe[1] == "jpg" && $ukuran > 5200 || $tipe[1] == "jpeg" && $ukuran > 5200){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Ukuran File Terlalu Besar');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }

                if($tipe[0] != "image"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }

                if($tipe[1] != "jpg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }
                elseif($tipe[1] != "jpeg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Gambar Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }
            } 
            else {
                $tipe =  explode("/", $_FILES['dok1_img1']['type']);
                if($data['laporan']['foto2'] != ""){
                    $file = "./assets/images/dokumentasi_musdes/foto/".$data['laporan']['foto2'];
                    if (is_readable($file) && unlink($file)) {
                    }
                }

                $data = array('image_metadata' => $this->upload->data());
                $image_metadatas = array();
                $image_metadatas = $this->upload->data();
                $nama_file = preg_replace('/[^A-Za-z0-9\.]/', '', $image_metadatas['file_name']);
            
            $data = [
                "foto2" => $image_metadatas['file_name']
            ];
            $this->db->where('id', $id);
            $this->db->update('laporan',$data);
            $this->session->set_flashdata('flash_gambar','Foto Dokumentasi Berhasil Diunggah');
            redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun); 
            }
        }

        public function berita($id,$kec,$desa,$tgl,$nmdesa,$tanggal){
            $data['sesion'] = $this->Admin_model->logged_id();
            $the_id = $data['sesion'];
            $data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
            $data['kec'] = $this->Musdes_model->get_musdes($data['sesion_data']['id']);

            $this->db->where('id',$the_id);
            $query = $this->db->get('user');
            $data['kec'] = $query->result_array();
            $nama_kec=$data['kec'][0]['nama'];
            
            
            $bulan = $this->input->post('bulan', true);
            $tahun = $this->input->post('tahun', true);
            $kec = $this->input->post('kec', true);
            $kel = $this->input->post('desa', true);

            $huruf=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
            $a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);
            $e = rand(0,9);
            $ids = $huruf[$a].$e.$huruf[$b].$c.$d;
            
            $filename=$_FILES['berita_img']['name'];
            date_default_timezone_set('Asia/Jakarta');
            $nama_gambar = $ids."_berita_acara_".$nama_kec."_".$nmdesa."_".$tanggal."_".date('H_i_s');
            $config['upload_path'] = './assets/images/dokumentasi_musdes/berita_acara';
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['file_name'] = $nama_gambar;
            $config['max_size'] = 5200;//5Mb max
            $this->load->library('upload', $config);
            $this->db->where('id',$id);
            $query = $this->db->get('laporan');
            $data['laporan'] = $query->row_array();

            

            if (!$this->upload->do_upload('berita_img')) {
                $error = array('error' => $this->upload->display_errors());
                $tipe =  explode("/", $_FILES['berita_img']['type']);
                $ukuran = $_FILES['berita_img']['size'];

                if($tipe[0] == "image" && $ukuran > 5200 || $tipe[1] == "jpg" && $ukuran > 5200 || $tipe[1] == "jpeg" && $ukuran > 5200 || $tipe[1] == "pdf" && $ukuran > 5200){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Berita Acara Dikarenakan Ukuran File Terlalu Besar');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }

                if($tipe[0] != "image"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Berita Acara Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }

                if($tipe[1] != "jpg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Berita Acara Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }
                elseif($tipe[1] != "jpeg"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Berita Acara Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }
                elseif($tipe[1] != "pdf"){
                    $this->session->set_flashdata('flash_gambar_gagal','<b>Gagal</b> Mengunggah Berita Acara Dikarenakan Tipe File Yang Anda Unggah Tidak Sesuai');
                    redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun);
                }
            } 
            else {
                $tipe =  explode("/", $_FILES['berita_img']['type']);
                if($data['laporan']['berita_acara'] != ""){
                    $file = "./assets/images/dokumentasi_musdes/berita_acara/".$data['laporan']['berita_acara'];
                    if (is_readable($file) && unlink($file)) {
                    }
                }

                $data = array('image_metadata' => $this->upload->data());
                $image_metadatas = array();
                $image_metadatas = $this->upload->data();
                // $nama_file = preg_replace('/[^A-Za-z0-9\.]/', '', $image_metadatas['file_name']);
            
            $data = [
                "berita_acara" => $image_metadatas['file_name']
            ];
            
            $this->db->where('id', $id);
            $this->db->update('laporan',$data);
            $this->session->set_flashdata('flash_gambar','Berita Acara Berhasil Diunggah');
            redirect('musdes/laporan_kecamatan/'.$the_id.'/'.$kel.'/'.$bulan.'/'.$tahun); 
            }
        }

        public function download_foto($nama,$kec,$desa,$bulan,$tahun){
			if($this->Admin_model->logged_id()){
			    $files = $nama;
			    $filePath = "assets/images/dokumentasi_musdes/foto/".$files;
				if (!empty($files) && file_exists($filePath)) {
				    header("Cache-Control: public");
				    header("Content-Description: File Transfer");
				    header("Content-Disposition: attachment; filename=$files");
				    header("Content-Type: application/zip");
				    header("Content-Transfer-Encoding: binary");

				    readfile($filePath);
                }
                $id = explode(".",$sesion_data['id']);
                if($id[1]=="00" && $id[2]=="00"){//prov
                    redirect('musdes/laporan_kecamatan/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
                }
                elseif($id[1]!="00" && $id[2]=="00"){//kota
                    redirect('musdes/rekap_kota/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
                }
                elseif($id[1]!="00" && $id[2]!="00"){//kec
                    redirect('musdes/laporan_kecamatan/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
                }
			}else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }
        
        public function download_berita($nama,$kec,$desa,$bulan,$tahun){
			if($this->Admin_model->logged_id()){
			    $files = $nama;
			    $filePath = "assets/images/dokumentasi_musdes/berita_acara/".$files;
				if (!empty($files) && file_exists($filePath)) {
				    header("Cache-Control: public");
				    header("Content-Description: File Transfer");
				    header("Content-Disposition: attachment; filename=$files");
				    header("Content-Type: application/zip");
				    header("Content-Transfer-Encoding: binary");

				    readfile($filePath);
				}
                $id = explode(".",$sesion_data['id']);
                if($id[1]=="00" && $id[2]=="00"){//prov
                    redirect('musdes/laporan_kecamatan/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
                }
                elseif($id[1]!="00" && $id[2]=="00"){//kota
                    redirect('musdes/rekap_kota/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
                }
                elseif($id[1]!="00" && $id[2]!="00"){//kec
                    redirect('musdes/laporan_kecamatan/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
                }
			}else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
		}

        public function hapus_musdes($id,$foto1,$foto2,$berita,$kec,$desa,$bulan,$tahun){
            if($foto1 != "kosong"){
                $file = "./assets/images/dokumentasi_musdes/foto/".$foto1;
                if (is_readable($file) && unlink($file)) {
                }
            }
            if($foto2 != "kosong"){
                $file = "./assets/images/dokumentasi_musdes/foto/".$foto2;
                if (is_readable($file) && unlink($file)) {
                }
            }
            if($berita != "kosong"){
                $file = "./assets/images/dokumentasi_musdes/berita_acara/".$berita;
                if (is_readable($file) && unlink($file)) {
                }
            }
            $this->db->where('id',$id);
            $this->db->delete('laporan');
            $this->session->set_flashdata('hapus_musdes','Data Musdes berhasil Dihapus');
            redirect('musdes/laporan_kecamatan/'.$kec.'/'.$desa.'/'.$bulan.'/'.$tahun);
        }

        public function generate_krt($kodkab){
            if($this->Admin_model->logged_id()){
                // $this->db3->where('idbdt is NOT NULL',null,false);
                // $this->db3->where('kddesa is NOT NULL',null,false);
                $this->db3->where('kdkab',$kodkab);
                $this->db3->group_by('nmkab,nmkec,nmdesa','ASC');
                $query = $this->db3->get('dtjateng_krt');
                $data['krt'] = $query->result_array();

                $jumlah = 0;

                foreach($data['krt'] as $q){
                    $this->db3->where('kdkab',$q['kdkab']);
                    $this->db3->where('kdkec',$q['kdkec']);
                    $this->db3->where('kddesa',$q['kddesa']);
                    $query = $this->db3->get('dtjateng_krt');
                    $data['krt2'] = $query->result_array();

                    $counter = 1;
                    foreach($data['krt2'] as $k){
                        
                        if($counter<10){
                            $urutan = '000'.$counter;
                        }elseif($counter <100 && $counter>=10){
                            $urutan = '00'.$counter;
                        }elseif($counter<1000 && $counter>=100){
                            $urutan = '0'.$counter;
                        }elseif($counter < 10000 && $counter>=1000){
                            $urutan = $counter;
                        }
                        $idjtg = '33'.$k['kdkab'].$k['kdkec'].$k['kddesa'].$urutan;
                        // echo $idjtg.'<br>';
                        // break;
                        // echo $urutan.'<br>';
                        
                        $data = [
                            'idjtg' => '33'.$k['kdkab'].$k['kdkec'].$k['kddesa'].$urutan,
                            'idjtgart' => '33'.$k['kdkab'].$k['kdkec'].$k['kddesa'].$urutan.'01',
                        ];
                        $this->db3->where('no',$k['no']);
                        $this->db3->update('dtjateng_krt',$data);
                        $counter++;
                        $jumlah++;
                    }
                }
                echo "oke kode ".$kodkab." sudah selesai total = ".$jumlah;

            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function generate_krt2($kodkab,$kodkec){
            if($this->Admin_model->logged_id()){
                // $this->db3->where('idbdt is NOT NULL',null,false);
                // $this->db3->where('kddesa is NOT NULL',null,false);
                $this->db3->where('kdkab',$kodkab);
                $this->db3->where('kdkec',$kodkec);
                $this->db3->group_by('nmkab,nmkec,nmdesa','ASC');
                $query = $this->db3->get('dtjateng_krt');
                $data['krt'] = $query->result_array();

                $jumlah = 0;

                foreach($data['krt'] as $q){
                    $this->db3->where('kdkab',$q['kdkab']);
                    $this->db3->where('kdkec',$q['kdkec']);
                    $this->db3->where('kddesa',$q['kddesa']);
                    $query = $this->db3->get('dtjateng_krt');
                    $data['krt2'] = $query->result_array();

                    $counter = 1;
                    foreach($data['krt2'] as $k){
                        
                        if($counter<10){
                            $urutan = '000'.$counter;
                        }elseif($counter <100 && $counter>=10){
                            $urutan = '00'.$counter;
                        }elseif($counter<1000 && $counter>=100){
                            $urutan = '0'.$counter;
                        }elseif($counter < 10000 && $counter>=1000){
                            $urutan = $counter;
                        }
                        $idjtg = '33'.$k['kdkab'].$k['kdkec'].$k['kddesa'].$urutan;
                        // echo $idjtg.'<br>';
                        // break;
                        // echo $urutan.'<br>';
                        
                        $data = [
                            'idjtg' => '33'.$k['kdkab'].$k['kdkec'].$k['kddesa'].$urutan,
                            'idjtgart' => '33'.$k['kdkab'].$k['kdkec'].$k['kddesa'].$urutan.'01',
                        ];
                        $this->db3->where('no',$k['no']);
                        $this->db3->update('dtjateng_krt',$data);
                        $counter++;
                        $jumlah++;
                    }
                }
                echo "oke kode ".$kodkab." sudah selesai total = ".$jumlah;

            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }

        public function generate_art($kodkab){
            if($this->Admin_model->logged_id()){
               
                $this->db3->where('idjtg is NOT NULL',null,false);
                $this->db3->where('kdkab',$kodkab);
                $this->db3->group_by('idjtg','ASC');
                $query = $this->db3->get('dtjateng_art');
                $data['art'] = $query->result_array();

                foreach($data['art'] as $q){
                    $this->db3->where('idjtg',$q['idjtg']);
                    $query = $this->db3->get('dtjateng_art');
                    $data['art2'] = $query->result_array();

                    $counter = 2;
                    foreach($data['art2'] as $ar){
                        
                        if($ar['Hub_KRT'] == '1'){
                            $data = [
                                'idjtgart' => $ar['idjtg'].'01',
                            ];
                            $this->db3->where('no',$ar['no']);
                            $this->db3->update('dtjateng_art',$data);
                        }
                        else{
                            if($counter<10){
                                $urutan = '0'.$counter;
                            }elseif($counter >=10){
                                $urutan = $counter;
                            }
                            $data = [
                                'idjtgart' => $ar['idjtg'].$urutan,
                            ];
                            $this->db3->where('no',$ar['no']);
                            $this->db3->update('dtjateng_art',$data);
                            $counter++;
                        }
                    }
                }
                echo "oke kode ".$kodkab." sudah selesai";

            }
            else{
	            //jika session belum terdaftar, maka redirect ke halaman login
	            redirect("home");
	        }
        }














	}

?>
